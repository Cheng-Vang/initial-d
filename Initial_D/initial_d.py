# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries/Imports---------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import re
import rev
import drive

from datetime import datetime

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------Functions-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def initial_d():
    """
    Initialize search arguments based on file
    """
    
    dlc1 = rev.select()    # Prompts and sets arguments from the user's chosen file
    
    return(dlc1)


def lap(dlc1):
    """
    Perform on the fly mechanisms before firing up the driver session
    """
    
    start = datetime.now()     # Mark start time for log

    print('There is/are ' + str(len(dlc1['dataset']['holder'])) + ' driver session(s) for this set')
    
    for data in dlc1['dataset']['holder']:

        dlc1 = rev.check_address(dlc1,data)       # Run address test

        if dlc1['address_flag'] == False:
            
            dlc1 = rev.check_entity(dlc1,data)    # Run entity test

            if dlc1['entity'] == True:            # Override tests and jump to county test given an entity
                pass
            
            else:
                
                dlc1 = rev.check_abbreviations(dlc1,data)    # Run abbreviations test
                
                ampersand = re.search(' & ', dlc1['dataset']['holder'][data])

                if bool(ampersand) == True:
                    
                    dlc1 = rev.check_multi(dlc1,data)    # Run multi test
                    
                else:
                    
                    dlc1 = rev.check_single(dlc1,data)   # Run single test

            if dlc1['skip'] == True:     # Bypass current index and proceed onto the next if any preceding tests failed
                print('Skipping session ' + str(int(data) + 1) + ' due to unaccounted holder case')
        
            else:
                
                dlc1 = rev.check_county(dlc1,data)    # Run county test

                if dlc1['county_flag'] == 0:    # Bypass current index and proceed onto the next if county test fails
                    print('Skipping session ' + str(int(data) + 1) + ' due to inactive county case')
                
                else:
                    
                    dlc1['data'] = data                   # Set pointer for current iteration
                    dlc2 = rev.gen_dlc2(dlc1,data)        # Generate drive specific arguments
                    dlc = {**dlc1,**dlc2}                 # Merge dlc's
                    dlc['dataset']['county'][dlc['data']] = dlc['county']    # Manual merge county
                        
                    print('Session '+ str(int(data) + 1) + ' out of ' + str(len(dlc1['dataset']['holder'])) + ' driver session(s)')

                    driver = drive.ignite(dlc)            # Fire up driver

                    drive.drive(dlc, driver)              # Drive
                    
        else:   # Mark address error in dataframe and move onto the next index
            
            dlc1 = rev.mark_address_error(dlc1,data)
            print('Skipping session ' + str(int(data) + 1) + ' due to unaccounted address case')

    end = datetime.now()       # Mark end time for log

    log(start, end, dlc1)

    
def log(start, end, dlc1):
    """
    Log evolution
    """
    
    print('---------------- Evolution Complete  -----------------')

    parent_dir = os.path.abspath('..')
    
    log_path =  parent_dir + '\\logs\\'+ dlc1['log_date'] + '.txt'
    log = open(log_path, 'w')

    print('Started at ' + str(start))
    log.write('Started at ' + str(start))
    
    print('Ended at ' + str(end))
    log.write('\nEnded at ' + str(end))

    delta = end - start
    seconds = int(round(delta.total_seconds()))
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    
    print('Evolution duration: {:d} hour(s) and {:02d} minute(s) and {:02d} second(s)'.format(hours, minutes, seconds))
    log.write('\nEvolution duration {:d} hour(s) and {:02d} minute(s) and {:02d} second(s)'.format(hours, minutes, seconds))
    log.close()

    
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    
    dlc1 = initial_d()
    lap(dlc1)
