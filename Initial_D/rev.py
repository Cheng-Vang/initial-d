# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import re
import json
import pandas
import tkinter
import datetime
import tkinter.messagebox
import dateutil.relativedelta

from tkinter import Tk as Tkinter
from tkinter.filedialog import askopenfilename

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------Functions-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def select():
    """
    Initialize search arguments based on specified datum
    """

    file_check = False

    # Prompt for dataset
    while file_check == False:

        Tkinter().withdraw()
        tkinter.messagebox.showinfo("Dataset Selection", "Choose a dataset")

        path = askopenfilename()

        if path.lower().endswith('.xlsx'):  # Ensure file is a dataset
            datacheck = True

            if datacheck == True:
                try:
                    dataframe = pandas.read_excel(path, 'Sheet1')
                    dataframe.loc[0, 'Site Address']  # Minor check for dataset's validity
                except:
                    print('Invalid selection. Please try again.')
                else:
                    file_check = True
        else:
            print('Please select a dataset')

    # Fetch date
    get_date = re.search('[0-9]+-[0-9]+-[0-9]+', path)
    date = get_date.group(0)
    log_date = str(date)

    # Detect indexes with commercial assets
    mark_list = []

    for row in dataframe.itertuples():
        if getattr(row, '_13') == 'Commercial' and getattr(row, 'Check') == 1:
            mark_list.append(getattr(row, 'Index'))
        else:
            pass

    # Prep dlc
    hash_gen = 0
    dataset = {}
    dataset['holder'] = {}
    dataset['address'] = {}
    dataset['county'] = {}
    dataset['principal'] = {}
    dataset['first_name'] = {}
    dataset['first_mid'] = {}
    dataset['second_name'] = {}
    dataset['second_mid'] = {}
    dataset['last_name'] = {}

    # Form a search set based on detected indexes
    for marked in mark_list:
        pointer = mark_list[hash_gen]

        holder = dataframe.loc[pointer, 'Private Lender']
        dataset['holder'][str(hash_gen)] = str(holder)

        address = dataframe.loc[pointer, 'Mailing Address']
        dataset['address'][str(hash_gen)] = str(address)

        county = dataframe.loc[pointer, 'County']
        dataset['county'][str(hash_gen)] = str(county)

        principal = dataframe.loc[pointer, 'Loan Amount']
        dataset['principal'][str(hash_gen)] = str(principal)

        hash_gen += 1

    # Format date
    replace_date_dashes = date.replace('-', '/')
    convert_date_to_datetime_object = datetime.datetime.strptime(replace_date_dashes, '%m/%d/%Y')
    convert_starting_datetime_to_string = datetime.datetime.strftime(convert_date_to_datetime_object, '%m/%d/%Y')
    dataset['to_date'] = convert_starting_datetime_to_string

    backtrack_half = convert_date_to_datetime_object - dateutil.relativedelta.relativedelta(months=6)
    convert_ending_datetime_to_string = datetime.datetime.strftime(backtrack_half, '%m/%d/%Y')
    dataset['from_date'] = convert_ending_datetime_to_string

    # Inject dataframe runtime indexes
    dataframe['Driver Crashed'] = ''
    dataframe['Crash Cause'] = ''
    dataframe['Address Error'] = ''
    dataframe['Holder Not Found'] = ''
    dataframe['Doc Not Found'] = ''
    dataframe['Owner'] = ''
    dataframe['Matures'] = ''

    # Pack vars
    dlc1 = {}
    dlc1['dataset'] = dataset
    dlc1['from_date'] = dataset['from_date']
    dlc1['to_date'] = dataset['to_date']
    dlc1['dataframe'] = dataframe
    dlc1['path'] = path
    dlc1['sole_holder_flag'] = False
    dlc1['skip'] = False
    dlc1['address_flag'] = False
    dlc1['entity'] = False
    dlc1['county_flag'] = 0
    dlc1['log_date'] = log_date

    return (dlc1)


def check_address(dlc1, data):
    """
    Validate whether address will be accounted for as a search argument and if so then generate OCR terms
    """

    # Prep for address validation
    address = dlc1['dataset']['address'][data]
    up_address = address.upper()
    po_mark = re.match('PO', up_address)

    # Check if address is a PO Box and if it is then set OCR terms
    try:
        po = po_mark.group(0)
    except:
        po_flag = False
    else:
        po_flag = True

        # Refer to config JSON for specs and designations
        with open('../misc/config.json') as config:
            config = json.load(config)

        ocr = config['ocr']

        address_possibilities = ocr

    # If address is not a PO Box then check if it is a current valid address and if so then set OCR terms
    if po_flag == False:

        filter_address = re.match('[0-9]+', address)

        try:
            address_digits = filter_address.group(0)

        except AttributeError:
            address_possibilities = []
            address_possibilities.append('Error')
        else:
            address_possibilities = []
            address_possibilities.append(address_digits)

    else:
        pass

    # Ensure address is accounted for
    if 'Error' in address_possibilities:
        dlc1['address_flag'] = True
        return (dlc1)
    else:
        dlc1['address_flag'] = False
        return (dlc1)


def check_entity(dlc1, data):
    """
    Determine holder entity status and set data accordingly
    """

    holder = dlc1['dataset']['holder'][data]
    holder_split = holder.split()
    holder_counter = 0

    for term in holder_split:

        check_num = re.search('[0-9]+', holder_split[holder_counter])

        # Proceed with initialization if there are no digits present
        if check_num == None:

            holder_term = holder_split[holder_counter]

            # Refer to config JSON for specs and designations
            with open('../misc/config.json') as config:
                config = json.load(config)

            entity_markers = config['entity']

            # Initialize if referenced and instantly stop iteration
            if holder_term in entity_markers:
                dlc1['entity'] = True

                dlc1['dataset']['first_name'][data] = holder
                dlc1['dataset']['first_mid'][data] = ''
                dlc1['dataset']['second_name'][data] = ''
                dlc1['dataset']['second_mid'][data] = ''
                dlc1['dataset']['last_name'][data] = ''
                dlc1['entity'] = True
                dlc1['sole_holder_flag'] = True
                break

            # Initialize presets and proceed with next iteration
            else:
                dlc1['entity'] = False

                dlc1['dataset']['first_name'][data] = ''
                dlc1['dataset']['first_mid'][data] = ''
                dlc1['dataset']['second_name'][data] = ''
                dlc1['dataset']['second_mid'][data] = ''
                dlc1['dataset']['last_name'][data] = ''
                dlc1['entity'] = False
                dlc1['sole_holder_flag'] = False


        # Initialize if digits are present and instantly stop iteration
        else:
            dlc1['entity'] = True
            
            dlc1['dataset']['first_name'][data] = holder
            dlc1['dataset']['first_mid'][data] = ''
            dlc1['dataset']['second_name'][data] = ''
            dlc1['dataset']['second_mid'][data] = ''
            dlc1['dataset']['last_name'][data] = ''
            dlc1['entity'] = True
            dlc1['sole_holder_flag'] = True
            break

        holder_counter += 1

    return (dlc1)


def check_abbreviations(dlc1, data):
    """
    Remove select abbreviations from holder
    """

    holder = dlc1['dataset']['holder'][data]
    holder_split = holder.split()

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    abbreviations = config['abbreviations']

    # Remove any abbreviations
    term_counter = 0

    for term in holder_split:

        if term in abbreviations:
            holder_split.pop(term_counter)
        else:
            pass

        term_counter += 1

    holder = ' '.join(holder_split)

    dlc1['dataset']['holder'][data] = holder

    return (dlc1)


def check_multi(dlc1, data):
    """
    Determine holders status and set data acordingly
    """

    holder = dlc1['dataset']['holder'][data]

    first_holder, second_holder = re.split('\s*?\s*& ', holder)

    # ID first and last names
    dlc1['dataset']['first_name'][data] = str(re.search('^\w+', first_holder).group(0))
    dlc1['dataset']['second_name'][data] = str(re.search('^\w+', second_holder).group(0))
    dlc1['dataset']['last_name'][data] = str(re.search('\w+$', second_holder).group(0))

    # Determine mid initial(s)
    first_holder_spaces = first_holder.count(' ')
    second_holder_spaces = second_holder.count(' ')

    if first_holder_spaces == 0:
        dlc1['dataset']['first_mid'][data] = ''
        dlc1['skip'] = False
        run_second = True

    elif first_holder_spaces == 1:
        dlc1['dataset']['first_mid'][data] = str(re.search('\s+\w', first_holder).group(0).strip(' '))
        dlc1['skip'] = False
        run_second = True

    # Unaccounted case
    else:
        dlc1['dataset']['first_name'][data] = ''
        dlc1['dataset']['first_mid'][data] = ''
        dlc1['dataset']['second_name'][data] = ''
        dlc1['dataset']['second_mid'][data] = ''
        dlc1['dataset']['last_name'][data] = ''
        dlc1['skip'] = True
        dlc1 = mark_holder_not_found(dlc1, data)
        run_second = False

    # Proceed only if first holder's status is valid
    if run_second == True:

        if second_holder_spaces == 0 or second_holder_spaces == 1:
            dlc1['dataset']['second_mid'][data] = ''
            dlc1['skip'] = False

        elif second_holder_spaces == 2:
            dlc1['dataset']['second_mid'][data] = str(re.search('\s+\w', second_holder).group(0).strip(' '))
            dlc1['skip'] = False

        # Unaccounted case
        else:
            dlc1['dataset']['first_name'][data] = ''
            dlc1['dataset']['first_mid'][data] = ''
            dlc1['dataset']['second_name'][data] = ''
            dlc1['dataset']['second_mid'][data] = ''
            dlc1['dataset']['last_name'][data] = ''
            dlc1['skip'] = True
            dlc1 = mark_holder_not_found(dlc1, data)
    else:
        pass

    return (dlc1)


def check_single(dlc1, data):
    """
    Determine holder status and set data acordingly
    """

    holder = dlc1['dataset']['holder'][data].split()

    if len(holder) == 3:

        dlc1['dataset']['first_name'][data] = holder[0]
        dlc1['dataset']['first_mid'][data] = holder[1]
        dlc1['dataset']['second_name'][data] = ''
        dlc1['dataset']['second_mid'][data] = ''
        dlc1['dataset']['last_name'][data] = holder[2]
        dlc1['sole_holder_flag'] = True
        dlc1['skip'] = False

    elif len(holder) == 2:

        dlc1['dataset']['first_name'][data] = holder[0]
        dlc1['dataset']['first_mid'][data] = ''
        dlc1['dataset']['second_name'][data] = ''
        dlc1['dataset']['second_mid'][data] = ''
        dlc1['dataset']['last_name'][data] = holder[1]
        dlc1['sole_holder_flag'] = True
        dlc1['skip'] = False

    # Unaccounted case
    else:
        dlc1['dataset']['first_name'][data] = ''
        dlc1['dataset']['first_mid'][data] = ''
        dlc1['dataset']['second_name'][data] = ''
        dlc1['dataset']['second_mid'][data] = ''
        dlc1['dataset']['last_name'][data] = ''
        dlc1['sole_holder_flag'] = True
        dlc1['skip'] = True
        dlc1 = mark_holder_not_found(dlc1, data)

    return (dlc1)


def check_county(dlc1, data):
    """
    Determine asset's county and set county flag accordingly
    """
    
    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    if dlc1['dataset']['county'][data] in config['inoperable']:
        dlc1['county_flag'] = 0
    elif dlc1['dataset']['county'][data] in config['Firefox']:
        dlc1['county_flag'] = 1
    else:
        dlc1['county_flag'] = 2

    return (dlc1)


def save_dataframe(dlc1):
    """
    Save dataframe
    """
    
    dlc1['dataframe'].to_excel(dlc1['path'])


def mark_address_error(dlc1, data):
    """
    Edit dataset to reflect unaccounted address cases and reload dataframe
    """

    for row in dlc1['dataframe'].itertuples():
        if getattr(row, '_3') == dlc1['dataset']['address'][data] and getattr(row, 'County') == \
                dlc1['dataset']['county'][data]:
            dlc1['dataframe'].loc[getattr(row, 'Index'), 'Address Error'] = 1
            break
        else:
            pass

    save_dataframe(dlc1)

    return (dlc1)


def mark_holder_not_found(dlc1, data):
    """
    Edit dataset to reflect unaccounted holder cases and reload dataframe
    """

    for row in dlc1['dataframe'].itertuples():
        if getattr(row, '_3') == dlc1['dataset']['address'][data] and getattr(row, 'County') == \
                dlc1['dataset']['county'][data]:
            dlc1['dataframe'].loc[getattr(row, 'Index'), 'Holder Not Found'] = 1
            break
        else:
            pass

    save_dataframe(dlc1)

    return (dlc1)


def gen_dlc2(dlc1, data):
    """
    Initialize specific drive arguments based on county
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc1['dataset']['county'][data]

    # Appropriately assign arguments based on county
    if county in config['mfc']:

        website = 'https://www.myfloridacounty.com/ori/index.do'
        name_field_1 = ''
        name_field_2 = ''
        from_date_field = ''
        to_date_field = ''
        search_button = '/html/body/div[1]/div[2]/div[3]/form/input'
        mid_initial_flag = False
        comma_flag = False

    elif county in config['landmark267']:

        website = config['landmark267_website'][county]
        name_field_1 = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[2]/td[2]/input'
        
        name_field_2 = ''
        from_date_field = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[6]/td[2]/input'
        to_date_field = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[7]/td[2]/input'
        search_button = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[9]/td[2]/a[1]'

        if county == 'Charlotte' or county == 'Clay' or county == 'Indian River':
            mid_initial_flag = False
            comma_flag = False
        else:
            mid_initial_flag = True
            comma_flag = False

        if county == 'Indian River':
            county = 'IndianRiver'
        else:
            pass

        if county == 'Saint Johns':
            county = 'SaintJohns'
        else:
            pass

    elif county in config['landmark278']:

        website = config['landmark278_website'][county]
        name_field_1 = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[2]/td[2]/input'
        name_field_2 = ''
        from_date_field = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[7]/td[2]/input'
        to_date_field = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[8]/td[2]/input'
        search_button = '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[10]/td[2]/a[1]'
        mid_initial_flag = True
        comma_flag = False

    elif county == 'Alachua' or county == 'Sarasota':

        if county == 'Alachua':
            website = 'http://isol.alachuaclerk.org/RealEstate/SearchEntry.aspx?e=newSession'
            search_button = '/html[1]/body[1]/div[3]/form[1]/div[3]/div[3]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]'
        elif county == 'Sarasota':
            website = 'https://clerkpublicrecords.scgov.net/'
            search_button = '/html[1]/body[1]/div[3]/form[1]/div[3]/div[3]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[1]/table[1]/tbody[1]/tr[1]/td[1]/span[1]/u[1]'

        name_field_1 = '//*[@id="cphNoMargin_f_txtParty"]'
        name_field_2 = ''
        from_date_field = '//*[@id="x:2002578730.0:mkr:3"]'
        to_date_field = '//*[@id="x:625521537.0:mkr:3"]'
        mid_initial_flag = True
        comma_flag = False

    elif county == 'Brevard':

        website = 'https://vaclmweb1.brevardclerk.us/AcclaimWeb/search/Disclaimer?st=/AcclaimWeb/search/SearchTypeName'
        name_field_1 = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/input[1]'
        name_field_2 = ''
        from_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[4]/div[1]/div[1]/input[1]'
        to_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[4]/div[5]/div[1]/div[1]/input[1]'
        search_button = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[2]/input[2]'
        mid_initial_flag = True
        comma_flag = True

    # OnCore245
    elif county == 'Broward' or county == 'Pinellas':

        if county == 'Broward':
            website = 'https://officialrecords.broward.org/AcclaimWeb/search/Disclaimer?st=/AcclaimWeb/search/SearchTypeName'
            mid_initial_flag = True
            comma_flag = True
        else:
            website = 'https://officialrecords.mypinellasclerk.org/search/Disclaimer?st=/search/SearchTypeName'
            mid_initial_flag = True
            comma_flag = False

        name_field_1 = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[2]/input[1]'
        name_field_2 = ''
        from_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[4]/div[4]/div[1]/div[1]/input[1]'
        to_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[5]/div[5]/div[1]/div[1]/input[1]'
        search_button = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[2]/input[2]'

    elif county == 'Collier':

        website = 'https://www.collierclerk.com/records-search/official-land-records-search'
        name_field_1 = '//*[@id="search_lastname"]'
        name_field_2 = '//*[@id="search_firstname"]'
        from_date_field = '//*[@id="search_startdate"]'
        to_date_field = '//*[@id="search_enddate"]'
        search_button = '//*[@id="searchbutton"]'
        mid_initial_flag = False
        comma_flag = False

    # OnCore234
    elif county == 'Duval' or county == 'Highlands' or county == 'Saint Lucie':

        if county == 'Duval':
            website = 'https://oncore.duvalclerk.com/search/SearchTypeName'
            mid_initial_flag = False
            comma_flag = False
        elif county == 'Highlands':
            website = 'http://acclaim.hcclerk.org/search/Disclaimer?st=/search/SearchTypeName'
            mid_initial_flag = True
            comma_flag = True
        elif county == 'Saint Lucie':
            website = 'https://acclaimweb.stlucieclerk.com/search/Disclaimer?st=/search/SearchTypeName'
            mid_initial_flag = False
            comma_flag = True
            county = 'SaintLucie'

        name_field_1 = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[2]/input[1]'
        name_field_2 = ''
        from_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[4]/div[1]/div[1]/input[1]'
        to_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[4]/div[4]/div[1]/div[1]/input[1]'
        search_button = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[2]/input[2]'

    # OnCore202127
    elif county == 'Hillsborough' or county == 'Nassau':

        if county == 'Hillsborough':
            website = 'https://pubrec3.hillsclerk.com/oncore/search.aspx'
        elif county == 'Nassau':
            website = 'http://www.nassauclerk.com/publicrecords/oncoreweb/'

        name_field_1 = '/html/body/form/div[3]/div[2]/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[4]/td[2]/input'
        name_field_2 = ''
        from_date_field = '/html/body/form/div[3]/div[2]/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[20]/td[2]/input'
        to_date_field = '/html/body/form/div[3]/div[2]/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[21]/td[2]/input'
        search_button = '/html/body/form/div[3]/div[2]/table/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr[27]/td[2]/input'

        if county == 'Nassau':
            mid_initial_flag = False
            comma_flag = False
        else:
            mid_initial_flag = True
            comma_flag = False

    elif county == 'Lake':

        website = 'https://officialrecords.lakecountyclerk.org/search/Disclaimer?st=/search/SearchTypeName'
        name_field_1 = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[2]/input[1]'
        name_field_2 = ''
        from_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[4]/div[1]/div[1]/input[1]'
        to_date_field = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[4]/div[1]/div[1]/input[1]'
        search_button = '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[2]/input[2]'
        mid_initial_flag = False
        comma_flag = True

    elif county == 'Marion':

        website = 'http://216.255.240.38/SearchNG_Application/'
        name_field_1 = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/font[2]/input'
        name_field_2 = ''
        from_date_field = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/table/tbody/tr/td[1]/font[3]/input'
        to_date_field = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/table/tbody/tr/td[1]/font[5]/input'
        search_button = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/input[1]'
        mid_initial_flag = True
        comma_flag = False

    elif county == 'Miami-dade':

        website = 'https://www2.miami-dadeclerk.com/PremierServices/login.aspx'
        name_field_1 = '//*[@id="pfirst_party"]'
        name_field_2 = ''
        from_date_field = "//input[@id='prec_date_from']"
        to_date_field = "//input[@id='prec_date_to']"
        search_button = "//input[@id='btnNameSearch']"
        mid_initial_flag = True
        comma_flag = False
        county = 'MiamiDade'

    # NewVision244
    elif county == 'Okeechobee' or county == 'Polk':

        if county == 'Okeechobee':
            website = 'http://67.77.91.155/BrowserViewOR/'
        elif county == 'Polk':
            website = 'http://www.polkcountyclerk.net/official-records-online/#2'

        name_field_1 = '/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/' + \
                       'div[1]/input[1]'
        name_field_2 = ''
        from_date_field = '/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[4]/' + \
                          'div[2]/p[1]/input[1]'
        to_date_field = '/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[4]/' + \
                        'div[3]/p[1]/input[1]'
        search_button = '/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/' + \
                        'div[3]/button[2]'
        mid_initial_flag = True
        comma_flag = False

    # NewVision235
    elif county == 'Osceola' or county == 'Palm Beach':

        if county == 'Osceola':
            website = 'http://198.140.240.30/or_web1/disclaim.asp'
        elif county == 'Palm Beach':
            website = 'http://oris.co.palm-beach.fl.us/or_web1/or_sch_1.asp'
            county = 'PalmBeach'

        name_field_1 = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/font[2]/input'
        name_field_2 = ''
        from_date_field = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/table/' + \
                          'tbody/tr/td[1]/font[3]/input'
        to_date_field = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/table/' + \
                        'tbody/tr/td[1]/font[5]/input'
        search_button = '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/input[1]'
        mid_initial_flag = True
        comma_flag = False

    elif county == 'Seminole':
        website = 'https://recording.seminoleclerk.org/DuProcessWebInquiry'
        name_field_1 = '//*[@id="criteria_full_name"]'
        name_field_2 = ''
        from_date_field = '//*[@id="criteria_file_date_start"]'
        to_date_field = '//*[@id="criteria_file_date_end"]'
        search_button = '/html/body/div[1]/div[2]/div[1]/div[2]/button[2]'
        mid_initial_flag = True
        comma_flag = True

    elif county == 'Volusia':

        website = 'https://app02.clerk.org/or_m/'
        name_field_1 = '//*[@id="name"]'
        name_field_2 = ''
        from_date_field = '//*[@id="fromDateTxt"]'
        to_date_field = '//*[@id="toDateTxt"]'
        search_button = '//*[@id="search"]'
        mid_initial_flag = True
        comma_flag = False

    # Pack vars
    dlc2 = {}
    dlc2['website'] = website
    dlc2['name_field_1'] = name_field_1
    dlc2['name_field_2'] = name_field_2
    dlc2['from_date_field'] = from_date_field
    dlc2['to_date_field'] = to_date_field
    dlc2['search_button'] = search_button
    dlc2['mid_initial_flag'] = mid_initial_flag
    dlc2['comma_flag'] = comma_flag
    dlc2['county'] = county

    return (dlc2)


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':  
    pass
