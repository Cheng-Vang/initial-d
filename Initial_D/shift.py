# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import re
import time
import json
import win32gui
import win32clipboard

from pywinauto.keyboard import SendKeys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Functions---------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((MyFloridaCounty)))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def mfc_entity(driver):
    """
    MyFloridaCounty entity activation
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[3]/p[1]/input[2]').click()
    time.sleep(1)

    return (driver)


def mfc_dates(dlc, driver):
    """
    Input date for MyFloridaCounty
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    # Set months
    from_date_split = dlc['from_date'].split('/')
    to_date_split = dlc['to_date'].split('/')

    from_date_month = str(from_date_split[0])
    to_date_month = str(to_date_split[0])

    set_from_month = config['mfc_get_month'][from_date_month]
    set_to_month = config['mfc_get_month'][to_date_month]

    # Fetch days
    from_date_day = str(from_date_split[1])
    to_date_day = str(to_date_split[1])

    mule_from_date_day = re.match('0', from_date_day)
    mule_to_date_day = re.match('0', to_date_day)

    # Set days based on it being singular or double digits
    if bool(mule_from_date_day) == True:
        final_from_date_day = from_date_day.strip('0')
        
    else:
        final_from_date_day = from_date_day

    if bool(mule_to_date_day) == True:
        final_to_date_day = to_date_day.strip('0')
        
    else:
        final_to_date_day = to_date_day

    print('Setting from date to ' + dlc['from_date'])
    select_from_month = Select(
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[12]/p[1]/select[1]'))
    select_from_month.select_by_visible_text(set_from_month)

    select_from_day = Select(
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[12]/p[1]/select[2]'))
    select_from_day.select_by_visible_text(final_from_date_day)

    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(from_date_split[2])
    win32clipboard.CloseClipboard()
        
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[12]/p[1]/input').clear()
    ActionChains(driver).key_down(Keys.CONTROL).send_keys('v').key_up(Keys.CONTROL).perform()
    
    print('Setting to date to ' + dlc['to_date'])
    select_to_month = Select(
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[12]/p[2]/select[1]'))
    select_to_month.select_by_visible_text(set_to_month)

    select_to_day = Select(
        driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[12]/p[2]/select[2]'))
    select_to_day.select_by_visible_text(final_to_date_day)

    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(to_date_split[2])
    win32clipboard.CloseClipboard()
    
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[12]/p[2]/input').clear()
    ActionChains(driver).key_down(Keys.CONTROL).send_keys('v').key_up(Keys.CONTROL).perform()

    return (driver)


def mfc_search_results_count(driver):
    """
    Retrieve MyFloridaCounty counties' search results
    """
    
    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,'/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/p[2]/span[1]')))
        
    except TimeoutException:

        # First check for the no records element before flagging as a crash session
        try:
            driver.find_element_by_xpath('/html/body/div[1]/div[3]/div[2]/h2')
            
        except NoSuchElementException:
            return (NoSuchElementException)

        else:
            search_results = 0

    else:
        
        raw_search_results = str(driver.find_element_by_xpath('/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/p[2]/span[1]').text)

        if raw_search_results == 'One item found.':
            search_results = 1
            
        else:
            
            raw_search_results_split = raw_search_results.split(',')   
            search_results = int(re.match('^[0-9]+', raw_search_results_split[0]).group(0))

    return (search_results, driver)


def mfc_pagination(comb, driver):
    """
    Pagination for MyFloridaCounty
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 25 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/p[2]/span[2]/a[7]').click()

            comb['print_search_instance'] = 1

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/table/tbody/tr[' +
                                                                              str(comb[
                                                                                      'print_search_instance']) + ']/td[5]')))
            
            return (comb, driver)
        
        else:
            return (comb, driver)


def mfc_doc_type(comb, driver):
    """
    MyFloridaCounties' doctype and owner
    """

    view = driver.find_element_by_xpath(
        '/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/table/tbody/tr[' + str(
            comb['print_search_instance']) + ']/td[5]')

    driver.execute_script("arguments[0].scrollIntoView();", view)
    time.sleep(1)

    xpath_doc_type = '/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/table/tbody/tr[' + str(
        comb['print_search_instance']) + ']/td[5]'

    xpath_owner = '/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/table/tbody/tr[' + str(
        comb['print_search_instance']) + ']/td[2]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)
    
    return (xpath_doc_type, owner, driver)


def mfc_doc_type_match():
    """
    MyFloridaCounty matching documents
    """

    return ('MORTGAGE', 'AGREEMENT AND/OR CONTRACT FOR DEED')


def mfc_download(comb, driver):
    """
    Initate MyFloridaCounty download
    """

    driver.find_element_by_xpath(
        '/html/body/div[1]/div[3]/table/tbody/tr[2]/td[2]/form/div/table/tbody/tr[' + str(
            comb['print_search_instance']) + ']/td[10]/a').click()

    return (driver)


def mfc_revert(driver):
    """
    Restore MyFloridaCounty search after download
    """

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()

    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Alachua)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Alachua_search_results_count(driver):
    """
    Retrieve the number of Alachua county's search results
    """
    
    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH, '//*[@id="cphNoMargin_cphNoMargin_SearchCriteriaTop_TotalRows"]')))

    search_results = int(
        driver.find_element_by_xpath('//*[@id="cphNoMargin_cphNoMargin_SearchCriteriaTop_TotalRows"]').text)

    return (search_results, driver)


def sort_Alachua(driver):
    """
    Sort Alachua search results by ascending order
    """

    sort = Select(
        driver.find_element_by_xpath('//*[@id="cphNoMargin_cphNoMargin_OptionsBar1_ddlSortColumns"]'))
    sort.select_by_visible_text('Date Filed, Ascending')

    return (driver)


def pagination_Alachua(comb, driver):
    """
    Pagination for Alachua county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 25 == 0:

            # Paginate element
            driver.find_element_by_xpath(
                '//*[@id="OptionsBar2_imgNext"]').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 0

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '//*[@id="x:1533160306.14:adr:0:tag::chlGCnt:0:exp:False:iep:False:ppd:False"]/td[11]')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Alachua(comb, driver):
    """
    Alachua xpath doc type and owner
    """

    xpath_doc_type = '//*[@id="x:1533160306.14:adr:' + str(
        comb['doc_counter']) + ':tag::chlGCnt:0:exp:False:iep:False:ppd:False"]/td[11]'

    owner_counter = comb['doc_counter'] + 2

    xpath_owner = '/html[1]/body[1]/div[3]/form[1]/div[3]/div[3]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/table[1]/' + \
                  'tbody[1]/tr[2]/td[1]/table[1]/tbody[2]/tr[1]/td[1]/div[2]/table[1]/tbody[1]/' + \
                  'tr[' + str(owner_counter) + ']/td[12]/div[1]/span[2]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Alachua_match():
    """
    Alachua doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Alachua(comb, driver):
    """
    Alachua click image
    """
    
    click = driver.find_element_by_xpath('//*[@id="x:1533160306.14:adr:' + str(
        comb['doc_counter']) + ':tag::chlGCnt:0:exp:False:iep:False:ppd:False"]/td[2]/div')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    ActionChains(driver).move_to_element(click).perform()
    click.click()
    
    return (driver)


def predownload_Alachua(driver):
    """
    Switch driver to appropriate frame to download from Alachua county
    """

    time.sleep(1)
    view_image = driver.window_handles[1]
    driver.switch_to.window(view_image)
    time.sleep(1)

    driver.switch_to.frame(0)
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="btnProcessNow__5"]')))
    time.sleep(1)
    
    driver.find_element_by_xpath('//*[@id="btnProcessNow__5"]').click()
    WebDriverWait(driver, 15).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'ifrImageWindow')))
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/fieldset/div/a')))    
    driver.find_element_by_xpath('/html/body/fieldset/div/a').click()
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="download"]')))

    return (driver)


def download_Alachua(driver):
    """
    Initiate Alachua download
    """

    driver.find_element_by_xpath('//*[@id="download"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)

        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Alachua(driver):
    """
    Restore Alachua search after download
    """

    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Baker))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Baker_search_results_count(driver):
    """
    Retrieve the number of Baker county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b').text)
    
    search_results = convert_Baker_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Baker_raw_search_results(raw_search_results):
    """
    Convert Baker raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Baker(driver):
    """
    Sort Baker search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Baker(driver):
    """
    Pagination for Baker county
    """

    # Show all results so pagination is not required
    select = Select(
        driver.find_element_by_xpath(
            "//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Baker(comb, driver):
    """
    Baker xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' \
                     + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' \
                  + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Baker_match():
    """
    Baker doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Baker(comb, driver):
    """
    Baker click image
    """

    click = driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' + str(
            comb['print_search_instance']) + ']/td[9]')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    click.click()

    WebDriverWait(driver, 15).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Baker(driver):
    """
    Initiate Baker download
    """

    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_Baker(driver):
    """
    Restore Baker search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((Bay))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Bay_search_results_count(driver):
    """
    Retrieve the number of Bay county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b').text)

    search_results = convert_Bay_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Bay_raw_search_results(raw_search_results):
    """
    Convert Bay raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Bay(driver):
    """
    Sort Bay search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Bay(driver):
    """
    Pagination for Bay county
    """

    select = Select(driver.find_element_by_xpath("//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all,driver)


def xpath_doc_type_and_owner_Bay(comb, driver):
    """
    Bay xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' \
                     + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' \
                  + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Bay_match():
    """
    Bay doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Bay(comb, driver):
    """
    Bay click image
    """

    click = driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' + str(
            comb['print_search_instance']) + ']/td[9]')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    click.click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Bay(driver):
    """
    Initiate Bay download
    """

    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_Bay(driver):
    """
    Restore Bay search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Brevard)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def checkbox_Brevard(driver):
    """
    Ensure whether or not names checkbox pops up and switch to mark it if it does
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//form[@action='/AcclaimWeb/Search/SearchTypePreName']//input[@id='Checkbox1']")))
        
    except TimeoutException:
        
        check_box_exist = False
        return (check_box_exist, driver)
    
    else:
        check_box_exist = True
        return (check_box_exist, driver)


def Brevard_search_results_count(check_box_exist, driver):
    """
    Retrieve the number of Brevard county's search results
    """

    if check_box_exist == False:
        
        search_results = 0
        return (search_results, driver)
    
    else:
        
        driver.find_element_by_xpath("//form[@action='/AcclaimWeb/Search/SearchTypePreName']//input[@id='Checkbox1']").click()
        time.sleep(1)

        driver.find_element_by_xpath("//form[@action='/AcclaimWeb/Search/SearchTypePreName']//input[@value='Done']").click()

        # Wait for first xpath_doc_type to appear before proceeding to get search results otherwise it won't process
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,'/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))

        raw_search_results = str(driver.find_element_by_xpath('/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[2]/div[3]').text)

        search_results = convert_Brevard_raw_search_results(raw_search_results)

        return (search_results, driver)


def convert_Brevard_raw_search_results(raw_search_results):
    """
    Convert Brevard raw search results
    """

    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return(search_results)


def pagination_Brevard(comb, driver):
    """
    Pagination for Brevard county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 11 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                '/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))
            
            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Brevard(comb, driver):
    """
    Brevard xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[7]'
    
    xpath_owner = '/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[5]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Brevard_match():
    """
    Brevard doc type match
    """

    return ('MORTGAGE', 'AGREEMENT/CONTRACT FOR DEED')


def click_image_Brevard(comb, driver):
    """
    Brevard click image
    """

    click = driver.find_element_by_xpath(
        '/html/body/div[2]/div[5]/div/div/strong/div/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[7]')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    click.click()

    return (driver)


def predownload_Brevard(driver):
    """
    Switch driver to appropriate frame to download from Brevard county
    """
    
    driver.switch_to.window(driver.window_handles[-1])
    driver.switch_to.default_content()
    
    WebDriverWait(driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'imgFrame1')))
    WebDriverWait(driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'ImageInPdf')))
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH,
        '//*[@id="secondaryToolbarToggle"]')))
    
    driver.find_element_by_xpath('//*[@id="secondaryToolbarToggle"]').click()
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
        '//*[@id="secondaryDownload"]')))

    return (driver)


def download_Brevard(driver):
    """
     Initiate Brevard download
    """

    driver.find_element_by_xpath('//*[@id="secondaryDownload"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Brevard(driver):
    """
    Restore Brevard search after download
    """

    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()
    time.sleep(1)

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Broward)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Broward_search_results_count(driver):
    """
    Retrieve the number of Broward county's search results
    """

    try:
        WebDriverWait(driver, 6).until(EC.visibility_of_element_located((By.XPATH,
                                                                         '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[13]')))
    except TimeoutException:
        search_results = 0
        
    else:
        
        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]').text)
        
        search_results = convert_Broward_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Broward_raw_search_results(raw_search_results):
    """
    Convert Broward raw search results
    """
    
    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)


def pagination_Broward(comb, driver):
    """
    Pagination for Broward county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 12 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[13]')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Broward(comb, driver):
    """
    Broward xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + \
                     str(comb['doc_counter']) + ']/td[15]'

    xpath_owner = '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + \
                  str(comb['doc_counter']) + ']/td[5]'
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Broward_match():
    """
    Broward doc type match
    """

    return ('Mortgage/ Modifications & Assumptions', 'MULE')


def click_image_Broward(comb, driver):
    """
    Broward click image
    """
        
    click = driver.find_element_by_xpath(
            '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + \
            str(comb['doc_counter']) + ']/td[15]')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    ActionChains(driver).move_to_element(click).perform()
    click.click()
    
    return (driver)


def predownload_Broward(driver):
    """
    Switch driver to appropriate frame to download from Brevard county
    """
    
    driver.switch_to.window(driver.window_handles[-1])
    driver.switch_to.default_content()
    
    WebDriverWait(driver, 20).until(EC.invisibility_of_element_located((By.XPATH,'/html/body/div[2]/table/tbody/tr/td[2]/div[1]/img')))
    WebDriverWait(driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'imgFrame1')))
    WebDriverWait(driver, 20).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'ImageInPdf')))
    WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '#viewer')))
    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH,
        '//*[@id="secondaryToolbarToggle"]')))
    
    driver.find_element_by_xpath('//*[@id="secondaryToolbarToggle"]').click()
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
        '//*[@id="secondaryDownload"]')))

    return (driver)


def download_Broward(driver):
    """
     Initiate Broward download
    """
    
    driver.find_element_by_xpath('//*[@id="secondaryDownload"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Broward(driver):
    """
    Restore Broward search after download
    """

    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()
    time.sleep(1)

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((Charlotte)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Charlotte_search_results_count(driver):
    """
    Retrieve the number of Charlotte county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b').text)
    
    search_results = convert_Charlotte_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Charlotte_raw_search_results(raw_search_results):
    """
    Convert Charlotte raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Charlotte(driver):
    """
    Sort Charlotte search results by ascending order
    """

    driver.find_element_by_xpath("//*[@id='resultsTable']/thead/tr[2]/th[8]").click()

    return (driver)


def pagination_Charlotte(driver):
    """
    Pagination for Charlotte county
    """

    # Show all results so pagination is not required
    select = Select(driver.find_element_by_xpath("//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Charlotte(comb, driver):
    """
    Charlotte xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/' + \
                     'tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/' + \
                  'tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Charlotte_match():
    """
    Charlotte doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Charlotte(comb, driver):
    """
    Charlotte click image
    """

    driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/'
                                 'td/div/div[8]/table/tbody/tr[' + str(
        comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Charlotte(driver):
    """
    Charlotte initiate download
    """

    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_Charlotte(driver):
    """
    Restore Charlotte search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Citrus)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Citrus_search_results_count(driver):
    """
    Retrieve the number of Citrus county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]'
             '/tr[1]/td[1]/div[1]/div[2]/b[1]')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]'
        '/td[1]/div[1]/div[2]/b[1]').text)

    search_results = convert_Citrus_raw_search_results(raw_search_results)
    
    return (search_results, driver)


def convert_Citrus_raw_search_results(raw_search_results):
    """
    Convert Citrus raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Citrus(driver):
    """
    Sort Citrus search results by ascending order
    """

    driver.find_element_by_xpath('/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3'
                                 ']/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[8]/table[1]/thead[1]/tr[2]/th[8]').click()

    return (driver)


def pagination_Citrus(driver):
    """
    Pagination for Citrus county
    """

    select = Select(driver.find_element_by_xpath('/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/'
                                                 'div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/div[4]/label[1]/b[1]/select[1]'))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Citrus(comb, driver):
    """
    Citrus xpath doc type and owner
    """

    xpath_doc_type = str(
        '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/' +
        'tbody[1]/tr[1]/td[1]/div[1]/div[8]/table[1]/tbody[1]/tr[' + str(comb['print_search_instance']) + ']/td[9]')

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[8]/table/' \
                  + 'tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Citrus_match():
    """
    Citrus doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Citrus(comb, driver):
    """
    Citrus click image
    """

    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/'
        'tbody[1]/tr[1]/td[1]/div[1]/div[8]/table[1]/tbody[1]/tr[' + str(
            comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//img[@id='documentImageInner']")))

    return (driver)


def download_Citrus(driver):
    """
    Citrus initiate download
    """

    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/a[1]').click()
    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[2]/a[1]').click()

    return (driver)


def revert_Citrus(driver):
    """
    Restore Citrus search after download
    """

    driver.find_element_by_xpath('/html[1]/body[1]/div[7]/div[1]/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((Clay))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Clay_search_results_count(driver):
    """
    Retrieve the number of Clay county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b').text)

    search_results = convert_Clay_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Clay_raw_search_results(raw_search_results):
    """
    Convert Clay raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Clay(driver):
    """
    Sort Clay search results by ascending order
    """

    driver.find_element_by_xpath("//*[@id='resultsTable']/thead/tr[2]/th[8]").click()

    return (driver)


def pagination_Clay(driver):
    """
    Pagination for Clay county
    """

    # Show all results so pagination is not required
    select = Select(driver.find_element_by_xpath("//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Clay(comb, driver):
    """
    Clay xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/' \
                     + 'tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/' \
                  + 'tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Clay_match():
    """
    Clay doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Clay(comb, driver):
    """
    Clay click image
    """

    driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/'
                                 'td/div/div[8]/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Clay(driver):
    """
    Initiate Clay download
    """

    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_Clay(driver):
    """
    Restore Clay search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Collier)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Collier_search_results_count(driver):
    """
    Retrieve the number of Collier county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH, '/html/body/div/div[4]/div[1]/table/tbody/tr[1]/td/div[1]')))
    

    tsearch_results = driver.find_element_by_xpath(
        '/html/body/div/div[4]/div[1]/table/tbody/tr[1]/td/div[1]').text
    
    tsearch_resultssplit = tsearch_results.split()
    
    raw_search_results = tsearch_resultssplit[-1]

    search_results = convert_Collier_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Collier_raw_search_results(raw_search_results):
    """
    Convert Collier raw search results
    """

    if raw_search_results == 'criteria.':
        search_results = 0
        
    else:
        search_results = int(raw_search_results)

    return (search_results)


def pagination_Collier(comb, driver):
    """
    Pagination for Collier county
    """

    if comb['search_instance'] == 0:
        return (comb,driver)
    
    else:
        
        if comb['search_instance'] % 50 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div/div[4]/div[1]/table/tbody/tr[1]/td/div[2]/a[1]/img top').click()

            # Next page number
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 3
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                                  '/html/body/div/div[4]/div[1]/table/tbody/tr[3]/td[4]')))

            return (comb,driver)
        
        else:
            return (comb,driver)


def xpath_doc_type_and_owner_Collier(comb, driver):
    """
    Collier xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div/div[4]/div[1]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[4]'

    xpath_owner = '/html/body/div/div[4]/div[1]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[2]'

    mule = driver.find_element_by_xpath(xpath_owner).get_attribute('innerHTML')

    mule_2 = re.findall('F:.*?<br>', mule)

    mule_counter = 0

    owner = ''

    for elements in mule_2:
        
        mule_3 = mule_2[mule_counter]
        mule_3 = mule_3.replace('F:', '')
        mule_3 = mule_3.replace('<b>', '')
        mule_3 = mule_3.replace('</b>', '')
        mule_3 = mule_3.replace('<br>', '')
        mule_3 = mule_3.replace('</br>', '')
        mule_3 = mule_3.replace('MBR', '')

        owner = owner + mule_3 + ' '

        mule_counter += 1

    return (xpath_doc_type, owner, driver)


def doc_type_Collier_match():
    """
    Collier doc type match
    """

    return ('MTGE', 'MULE')


def download_Collier(comb, driver):
    """
    Initiate Collier download
    """

    driver.find_element_by_xpath(
        '/html/body/div/div[4]/div[1]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[1]/a/img').click()

    return (driver)


def revert_Collier(driver):
    """
    Restore Collier search after download
    """

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.frame('blockrandom-240')

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((Duval)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def checkbox_Duval(driver):
    """
    Ensure whether or not names checkbox pops up and switch to mark it if it does
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, '/html/body/div[7]/div[2]/div/form/div[2]/div/ul/li/div/span[2]/input[2]')))
        
    except TimeoutException:
        
        check_box_exist = False
        return (check_box_exist, driver)
    
    else:
        check_box_exist = True
        return (check_box_exist, driver)


def Duval_search_results_count(check_box_exist, driver):
    """
    Retrieve the number of Duval county's search results
    """

    if check_box_exist == False:
        
        search_results = 0
        return (search_results, driver)
    
    else:

        # Select all names variations and delay for 1 sec before beginning search otherwise it won't process
        driver.find_element_by_xpath(
            '/html/body/div[7]/div[2]/div/form/div[2]/div/ul/li/div/span[2]/input[2]').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[7]/div[2]/div/form/div[3]/input').click()

        # Wait for first xpath_doc_type to appear before proceeding to get search results otherwise it won't process
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))

        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]/div[3]').text)
        
        search_results = convert_Duval_raw_search_results(raw_search_results)

        return (search_results, driver)


def convert_Duval_raw_search_results(raw_search_results):
    """
    Convert Duval raw search results
    """

    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)


def pagination_Duval(comb, driver):
    """
    Pagination for Duval county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 11 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))
            
            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Duval(comb, driver):
    """
    Duval xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[7]'

    xpath_owner = '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[4]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Duval_match():
    """
    Duval doc type match
    """

    return ('MORTGAGE', 'MULE')


def download_Duval(comb, driver):
    """
    Initiate Duval download
    """

    driver.find_element_by_xpath(
        '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[7]').click()
        
    return (driver)


def revert_Duval(driver):
    """
    Restore Duval search after download
    """

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Escambia))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Escambia_search_results_count(driver):
    """
    Retrieve the number of Escambia county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH,
                                          '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/'
                                          'tr[1]/td[1]/div[1]/div[1]/b[1]')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]'
        '/td[1]/div[1]/div[1]/b[1]').text)
    
    search_results = convert_Escambia_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Escambia_raw_search_results(raw_search_results):
    """
    Convert Escambia raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Escambia(driver):
    """
     Sort Escambia search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/'
        'thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Escambia(driver):
    """
    Pagination for Escambia county
    """

    select = Select(
        driver.find_element_by_xpath(
            "//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Escambia(comb, driver):
    """
    Escambia xpath doc type and owner
    """

    xpath_doc_type = str(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]'
        + '/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]')

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]' \
                  + '/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Escambia_match():
    """
    Escambia doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Escambia(comb, driver):
    """
    Escambia click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/tbody/tr['
        + str(comb['print_search_instance']) + ']/td[9]').click()

    return (driver)


def download_Escambia(driver):
    """
    Initiate Escambia download
    """

    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/a[1]').click()
    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[2]/a[1]').click()

    return (driver)


def revert_Escambia(driver):
    """
    Restore Escambia search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Hernando))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

def Hernando_search_results_count(driver):
    """
    Retrieve the number of Hernando county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH,
                                          '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/'
                                          'tr[1]/td[1]/div[1]/div[1]/b[1]')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]'
        '/td[1]/div[1]/div[1]/b[1]').text)

    search_results = convert_Hernando_raw_search_results(raw_search_results)
    
    return (search_results, driver)


def convert_Hernando_raw_search_results(raw_search_results):
    """
    Convert Hernando raw search reuslts
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Hernando(driver):
    """
    Sort Hernando search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/'
        'thead/tr[2]/th[8]').click()

    return (driver)

def pagination_Hernando(driver):
    """
    Pagination for Hernando county
    """
    
    select = Select(
        driver.find_element_by_xpath(
            "//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True    

    return (view_all, driver)

def xpath_doc_type_and_owner_Hernando(comb, driver):
    """
    Hernando xpath doc type and owner
    """

    xpath_doc_type = str(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]'
        + '/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]')

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]' \
                  + '/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)

def doc_type_Hernando_match():
    """
    Hernando doc type match
    """

    return ('MORTGAGE', 'MULE')

def click_image_Hernando(comb, driver):
    """
    Hernando click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/tbody/tr['
        + str(comb['print_search_instance']) + ']/td[9]').click()

    return (driver)

def download_Hernando(driver):
    """
    Initiate Hernando download
    """

    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/a[1]').click()
    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[2]/a[1]').click()

    return (driver)

def revert_Hernando(driver):
    """
    Restore Hernando search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)

# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Highlands))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Highlands_search_results_count(driver):
    """
    Retrieve the number of Highlands county's search results
    """

    driver = Highlands_loading_image(driver)

    try:
        WebDriverWait(driver, 3).until(EC.visibility_of_element_located(
            (By.XPATH,'/html/body/div[2]/div[5]/div[1]/div[1]/div[1]/span')))

    except TimeoutException:
        
        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html[1]/body[1]/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[2]/div[3]').text)
        
        search_results = convert_Highlands_raw_search_results(raw_search_results)

    else:
        search_results = 0

    return (search_results, driver)


def Highlands_loading_image(driver):
    """
    Allow loading image to appear and disappear for Highlands county
    """

    WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '/html/body/div[4]/imgwack')))
    WebDriverWait(driver, 15).until_not(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[4]/imgwack')))

    return (driver)


def convert_Highlands_raw_search_results(raw_search_results):
    """
    Convert Highlands raw search results
    """
    
    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)


def pagination_Highlands(comb, driver):
    """
    Pagination for Highlands county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 15 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))
            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Highlands(comb, driver):
    """
    Highlands xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[7]'

    xpath_owner = '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[5]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Highlands_match():
    """
    Highlands doc type match
    """

    return ('M', 'MULE')


def click_image_Highlands(comb, driver):
    """
    Highlands click image
    """

    driver.find_element_by_xpath(
        '//*[@id="RsltsGrid"]/div[4]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[1]').click()

    return (driver)


def predownload_Highlands(driver):
    """
    Switch driver to appropriate frame to download from Highlands county
    """

    time.sleep(1)
    view_image = driver.window_handles[-1]
    driver.switch_to.window(view_image)
    driver.switch_to.frame(0)
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="SaveDoc"]')))

    return (driver)


def download_Highlands(driver):
    """
    Initiate Highlands download
    """

    driver.find_element_by_xpath('//*[@id="SaveDoc"]').click()
    driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div[1]/form/div[1]/div[2]/button[4]').click()

    return (driver)


def revert_Highlands(driver):
    """
    Restore Highlands search after download
    """

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()
    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((Hillsborough)))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Hillsborough_search_results_count(driver):
    """
    Retrieve the number of Hillsborough county's search results
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html[1]/body[1]/form[1]/span[1]/table[1]/tbody[1]/tr[1]/td[2]/font[1]/span[2]')))
    except TimeoutException:
        search_results = 0
        
    else:
        
        raw_search_results = str(
            driver.find_element_by_xpath('/html[1]/body[1]/form[1]/span[1]/table[1]/tbody[1]/tr[1]/td[2]/font[1]/'
                                         'span[2]').text)
        
        search_results = convert_Hillsborough_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Hillsborough_raw_search_results(raw_search_results):
    """
    Convert Hillsborough raw search results
    """
    
    search_results = int(raw_search_results)

    return (search_results)


def sort_Hillsborough(driver):
    """
    Sort Hillsborough search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html[1]/body[1]/form[1]/span[1]/table[1]/tbody[1]/tr[6]/td[1]/table[1]/tbody[1]/tr[2]/td[6]/a[1]').click()
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
        (By.XPATH, '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[3]/td[7]')))

    return (driver)


def pagination_Hillsborough(comb, driver):
    """
    Pagination for Hillsborough county
    """

    
    # Pagination to load more ...
    if comb['pagination_counter'] == comb['pagination_load_more']:

        # ...
        driver.find_element_by_xpath(
            '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[1]/td/a[10]').click()

        # "[row]" element
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[3]/td[1]/a')))

        # Add 10 to comb['pagination_load_more'] so it can load more results correctly
        comb['pagination_load_more'] = comb['pagination_load_more'] + 10

        # Reset comb['pagination_counter'] to 2 so it can continue pagination correctly
        comb['pagination_counter'] = 2

        comb['doc_counter'] = 3

    else:
        pass
    

    # Pagination
    if comb['search_instance'] == 0:
        pass
    
    else:
        
        if comb['search_instance'] % 30 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[1]/td/a[' + str(
                    comb['pagination_counter']) + ']').click()

            comb['pagination_counter'] = comb['pagination_counter'] + 1
            comb['doc_counter'] = 3
            comb['print_search_instance'] = 1
            
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[3]/td[1]/a')))
        else:
            pass

    return (comb, driver)


def xpath_doc_type_and_owner_Hillsborough(comb, driver):
    """
    Hillsborough xpath doc type and owner
    """

    xpath_doc_type = '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[7]'

    xpath_owner = '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[5]'


    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Hillsborough_match():
    """
    Hillsborough doc type match
    """

    return ('MORTGAGE', 'AGREEMENT')


def click_image_Hillsborough(comb, driver):
    """
    Hillsborough click driver
    """

    driver.find_element_by_xpath(
        '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[' + str(
            comb['doc_counter']) + ']/td[1]/a').click()

    return (driver)


def predownload_Hillsborough(driver):
    """
    Switch driver to appropriate frame to download from Hillsborough county
    """
    
    WebDriverWait(driver, 15).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, 'doc')))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'download')))
    
    return (driver)


def download_Hillsborough(driver):
    """
    Initiate Hillsborough download
    """

    driver.find_element_by_id('download').click()
    
    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Hillsborough(driver):
    """
    Restore Hillsborough search after download
    """

    driver.switch_to.default_content()
    driver.switch_to.frame('top')
    driver.find_element_by_xpath('//*[@id="PageHeader1_hlSearch"]').click()
    time.sleep(1)

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((Indian River)))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def IndianRiver_search_results_count(driver):
    """
    Retrieve the number of Indian River county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b').text)
    
    search_results = convert_IndianRiver_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_IndianRiver_raw_search_results(raw_search_results):

    if raw_search_results == 'Returned 0 records':
        search_results = 0
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_IndianRiver(driver):
    """
    Sort Indian River search results by ascending order
    """

    driver.find_element_by_xpath("//*[@id='resultsTable']/thead/tr[2]/th[8]").click()

    return (driver)


def pagination_IndianRiver(driver):
    """
    Pagination for Indian River county
    """

    # Show all results so pagination is not required
    select = Select(driver.find_element_by_xpath("//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_IndianRiver(comb, driver):
    """
    Indian River xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' \
                     + 'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]'
    
    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' \
                  + 'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_IndianRiver_match():
    """
    Indian River doc type match
    """
    
    return ('MORTGAGE', 'MULE')


def click_image_IndianRiver(comb, driver):
    """
    Indian River click image
    """

    click = driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/'
                                 'td/div/div[8]/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]')
    
    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    click.click()
    
    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_IndianRiver(driver):
    """
    Initiate Indian River download
    """

    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_IndianRiver(driver):
    """
    Restore Indian_River search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((Lake)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def checkbox_Lake(driver):
    """
    Ensure whether or not names checkbox pops up and switch to mark it if it does
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, '/html/body/div[7]/div[2]/div/form/div[2]/div/ul/li/div/span[2]/input[2]')))
        
    except TimeoutException:
        
        check_box_exist = False
        return (check_box_exist, driver)
    
    else:
        check_box_exist = True
        return (check_box_exist, driver)


def Lake_search_results_count(check_box_exist, driver):
    """
    Retrieve the number of Lake county's search results
    """

    if check_box_exist == False:
        
        search_results = 0
        return (search_results, driver)
    
    else:

        # Select all names variations and delay for 1 sec before beginning search otherwise it won't process
        driver.find_element_by_xpath(
            '/html/body/div[7]/div[2]/div/form/div[2]/div/ul/li/div/span[2]/input[2]').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[7]/div[2]/div/form/div[3]/input').click()

        # Wait for "Loading" image to disappear otherwise search won't process
        WebDriverWait(driver, 10).until(EC.invisibility_of_element_located((By.XPATH, '/html/body/div[4]/img')))

        # Wait for first xpath_doc_type to appear before proceeding to get search results otherwise it won't process
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[3]')))

        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[3]').text)
        
        search_results = convert_Lake_raw_search_results(raw_search_results)

        # Toggle search result window so second lender can be fired up if needed
        driver.find_element_by_xpath('//*[@id="toggleForm"]').click()

        return (search_results, driver)


def convert_Lake_raw_search_results(raw_search_results):
    """
    Ensure Lake raw search results
    """
    
    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)
    

def sort_Lake(driver):
    """
    Sort Lake search results by ascending order
    """

    driver.find_element_by_xpath('/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[3]/'
                                 'div/table/tbody/tr/th[6]/a').click()

    return (driver)


def pagination_Lake(comb, driver):
    """
    Pagination for Lake county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 50 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))
            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Lake(comb, driver):
    """
    Lake xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[7]'

    xpath_owner = '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' \
                  + str(comb['doc_counter']) + ']/td[5]'
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Lake_match():
    """
    Lake doc type match
    """

    return ('MTG', 'MULE')


def click_image_Lake(comb, driver):
    """
    Lake click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
            comb['doc_counter']) + ']/td[7]').click()

    return (driver)


def predownload_Lake(driver):
    """
    Switch driver to appropriate frame to download from Lake county
    """

    driver.switch_to.window(driver.window_handles[1])
    WebDriverWait(driver, 15).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'imgFrame1')))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="SaveDoc"]')))

    return (driver)


def download_Lake(driver):
    """
    Initiate Lake download
    """

    driver.find_element_by_xpath('//*[@id="SaveDoc"]').click()
    driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div[1]/form/div[1]/div[2]/button[3]').click()

    return (driver)


def revert_Lake(driver):
    """
    Restore Lake search after download
    """

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()
    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()

    return (driver)


# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((Lee)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Lee_search_results_count(driver):
    """
    Retrieve the number of Lee county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH,
                                          '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/'
                                          'tr[1]/td[1]/div[1]/div[1]/b[1]')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html[1]/body[1]/div[6]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]'
        '/td[1]/div[1]/div[1]/b[1]').text)
    
    search_results = convert_Lee_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Lee_raw_search_results(raw_search_results):
    """
    Convert Lee raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
        
    else:
        
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Lee(driver):
    """
    Sort Lee search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/'
        'thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Lee(driver):
    """
    Pagination for Lee county
    """
    select = Select(
        driver.find_element_by_xpath(
            "//*[@id='displaySelect']/select"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Lee(comb, driver):
    """
    Lee xpath doc type and owner
    """

    xpath_doc_type = str(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]'
        + '/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]')

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]' \
                  + '/table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Lee_match():
    """
    Lee doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Lee(comb, driver):
    """
    Lee click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/tbody/tr['
        + str(comb['print_search_instance']) + ']/td[9]').click()

    return (driver)


def predownload_Lee(driver):
    """
    Switch driver to appropriate frame to download from Lee county
    """

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/a[1]')))
    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/a[1]').click()

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[2]/a[1]')))
    driver.find_element_by_xpath(
        '/html[1]/body[1]/div[7]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/ul[1]/li[2]/a[1]').click()

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
        '/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]')))   

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    time.sleep(1)

    driver.switch_to.window(driver.window_handles[-1])
    
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="download"]')))
    
    return (driver)


def download_Lee(driver):
    """
    Initiate Lee download
    """

    driver.find_element_by_xpath('//*[@id="download"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Lee(driver):
    """
    Restore Lee search after download
    """

    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()
    time.sleep(1)

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Marion))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Marion_search_results_count(driver):
    """
     Retrieve the number of Marion county's search results
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, '/html/body/form/table[3]/tbody/tr[3]/td[2]/font')))

    except NoSuchElementException:
        search_results = 0

    else:
        
        raw_search_results = driver.find_element_by_xpath('/html/body/form/table[3]/tbody/tr[3]/td[2]/font').text

        search_results = convert_Marion_raw_search_results(raw_search_results)
        
    return (search_results, driver)


def convert_Marion_raw_search_results(raw_search_results):
    """
    Convert Marion raw search results
    """

    extract = re.search('[0-9]+ Records', raw_search_results)
    records = extract.group(0)
    recordssplit = records.split()
    search_results = int(recordssplit[0])

    return (search_results)
    

def sort_Marion(driver):
    """
    Sort Marion search results by ascending order
    """

    driver.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[1]/td[3]/a/strong/font').click()
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/form/table[4]/tbody/tr[1]/td[3]/a/strong/font').click()

    return (driver)


def pagination_Marion(comb, driver):
    """
    Pagination for Marion county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 20 == 0:

            # Paginate element
            driver.find_element_by_xpath(
                '/html/body/form/strong/font/a').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 2

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/form/table[4]/tbody/tr[2]/td[1]/font/a')))


            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_Marion(comb):
    """
    Marion xpath doc type
    """

    xpath_doc_type = '/html/body/form/table[4]/tbody/tr[' + str(comb['doc_counter']) + ']/td[4]/a/font'

    return (xpath_doc_type)


def doc_type_Marion_match():
    """
    Marion doc type match
    """

    return ('MTG  ', 'MULE')


def click_image_Marion(comb, driver):
    """
    Marion click image
    """
    
    driver.find_element_by_xpath(
        '/html/body/form/table[4]/tbody/tr[' + str(
            comb['doc_counter']) + ']/td[1]/font/a').click()

    return (driver)


def predownload_Marion(driver):
    """
    Switch driver to appropriate frame to download from Marion county
    """
    
    WebDriverWait(driver, 10).until(
        EC.frame_to_be_available_and_switch_to_it(driver.find_element_by_xpath('/html/frameset/frame[2]')))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
                                                                '/html/body/table[3]/tbody/tr[1]/td[1]/form')))

    driver.find_element_by_xpath('/html/body/table[3]/tbody/tr[1]/td[1]/form').click()

    time.sleep(1)
    
    driver.switch_to.alert.accept()
    driver.switch_to.default_content()

    WebDriverWait(driver, 10).until(
        EC.frame_to_be_available_and_switch_to_it(driver.find_element_by_xpath('/html/frameset/frame[2]')))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="download"]')))

    return (driver)


def download_Marion(driver):
    """
    Initiate Marion Download
    """

    driver.find_element_by_xpath('//*[@id="download"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Marion(driver):
    """
    Restore Marion search after download
    """

    driver.switch_to.default_content()
    driver.execute_script('window.history.go(-1)')
    time.sleep(1)
    driver.switch_to.frame(driver.find_element_by_xpath('/html/frameset/frame[1]'))

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td[2]/table[3]/tbody/tr[10]/td[2]')
    owner = str(raw_owner.text)

    driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td[2]/map[1]/area').click()

    time.sleep(1)
    driver.switch_to.default_content()

    return (owner, driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Martin)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Martin_search_results_count(driver):
    """
    Retrieve the number of Martin county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b')))
    
    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[1]/b').text)
    
    search_results = convert_Martin_raw_search_results(raw_search_results)

    return (search_results,driver)


def convert_Martin_raw_search_results(raw_search_results):
    """
    Convert Martin raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)
    

def sort_Martin(driver):
    """
    Sort Martin search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Martin(driver):
    """
    Pagination for Martin county
    """

    # Show all results so pagination is not required
    select = Select(
        driver.find_element_by_xpath(
            "//select[@name='resultsTable_length']"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Martin(comb, driver):
    """
    Martin xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/tbody/tr[' + str(
        comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' \
                  + 'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Martin_match():
    """
    Martin doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Martin(comb, driver):
    """
    Martin click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/tbody/tr['
        + str(comb['print_search_instance']) + ']/td[9]'
    ).click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Martin(driver):
    """
    Initiate Martin download
    """
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_Martin(driver):
    """
    Restore Martin search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((MiamiDade))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

def MiamiDade_search_results_count(driver):
    """
    Retrieve the number of MiamiDade county's search results
    """

    try:
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located(
                (By.XPATH, '//*[@id="lblResults"]')))
    
    except TimeoutException:
        
        # First check for the no records element before flagging as a crash session
        try:
            driver.find_element_by_xpath('/html/body/div/form/div[4]/div[6]/div[3]/div/div[2]/div/div/div/div/strong')
            
        except NoSuchElementException:
            return(NoSuchElementException)

        else:
            search_results = 0
        
    else:

        raw_search_results = str(driver.find_element_by_xpath('//*[@id="lblResults"]').text)
        
        search_results = convert_MiamiDade_raw_search_results(raw_search_results)

    return (search_results,driver)


def convert_MiamiDade_raw_search_results(raw_search_results):
    """
    Convert MiamiDade raw search results
    """

    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)


def pagination_MiamiDade(comb, driver):
    """
    Pagination for MiamiDade county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        if comb['search_instance'] % 50 == 0:

            # Paginate element
            driver.find_element_by_xpath('/html[1]/body[1]/div[1]/form[1]/div[4]/div[6]/div[3]/div[1]/div[3]/' +
                                         'div[1]/div[1]/div[1]/ul[1]/li[' + str(comb['pagination_counter']) + ']/a[1]')

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 2

            # Increment pagination counter
            comb['pagination_counter'] = comb['pagination_counter'] + 1

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div/form/div[4]/div[6]/div[3]/div/table/tbody/tr[1]/td[2]/a')))

            return (comb, driver)
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_MiamiDade(comb, driver):
    """
    MiamiDade xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div/form/div[4]/div[6]/div[3]/div/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[2]'
    
    xpath_owner = '/html/body/div/form/div[4]/div[6]/div[3]/div/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[9]/div[3]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_MiamiDade_match():
    """
    MiamiDade doc type match
    """

    return ('MOR', 'MULE')


def click_image_MiamiDade(comb, driver):
    """
    MiamiDade click image
    """

    click = driver.find_element_by_xpath(
        '/html/body/div/form/div[4]/div[6]/div[3]/div/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[2]')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    click.click()

    return (driver)


def predownload_MiamiDade(driver):
    """
    Switch driver to appropriate frame to download from MiamiDade county
    """

    time.sleep(1)
    window_1 = driver.window_handles[1]
    driver.switch_to.window(window_1)
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '// *[@ id="lnkDownload"]')))
    driver.find_element_by_xpath('//*[@id="lnkDownload"]').click()

    return (driver)


def download_MiamiDade(driver):
    """
    Initiate MiamiDade download
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//*[@id='lnkDownloadPDF']")))
    driver.find_element_by_xpath("//*[@id='lnkDownloadPDF']").click()

    return (driver)


def revert_MiamiDade(driver):
    """
    Restore MiamiDade search after download
    """

    window2 = driver.window_handles[2]
    driver.switch_to.window(window2)
    driver.close()
    
    window1 = driver.window_handles[1]
    driver.switch_to.window(window1)
    driver.close()
    
    window = driver.window_handles[0]
    driver.switch_to.window(window)

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((Nassau))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Nassau_search_results_count(driver):
    """
    Retrieve the number of Nassau county's search results
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '//*[@id="lblRecordCount"]')))
    except TimeoutException:
        search_results = 0
        
    else:
        
        raw_search_results = str(driver.find_element_by_xpath('//*[@id="lblRecordCount"]').text)

        search_results = convert_Nassau_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Nassau_raw_search_results(raw_search_results):
    """
    Convert Nasau raw search results
    """

    search_results = int(raw_search_results)

    return (search_results)


def sort_Nassau(driver):
    """
    Sort Nassau search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[2]/td[7]/a').click()
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
        (By.XPATH, '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[3]/td[8]')))

    return (driver)


def pagination_Nassau(comb, driver):
    """
    Pagination for Nassau county
    """

    # Pagination to load more ...
    if comb['pagination_counter'] == comb['pagination_load_more']:

        # ...
        driver.find_element_by_xpath(
            '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[1]/td/a[10]').click()

        # "[row]" element
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[3]/td[1]/a')))

        # Add 10 to comb['pagination_load_more'] so it can load more results correctly
        comb['pagination_load_more'] = comb['pagination_load_more'] + 10

        # Reset comb['pagination_counter'] to 2 so it can continue pagination correctly
        comb['pagination_counter'] = 2

        comb['doc_counter'] = 3

    else:
        pass

    # Pagination
    if comb['search_instance'] == 0:
        pass
    else:
        if comb['search_instance'] % 15 == 0:
            driver.find_element_by_xpath(
                '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[1]/td/b/a[' + str(
                    comb['pagination_counter']) + ']').click()

            comb['pagination_counter'] = comb['pagination_counter'] + 1
            comb['doc_counter'] = 3
            comb['print_search_instance'] = 1
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[3]/td[8]')))
        else:
            pass

    return (comb, driver)


def xpath_doc_type_and_owner_Nassau(comb, driver):
    """
    Nassau xpath doc type and owner
    """

    xpath_doc_type = '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[8]'

    xpath_owner = '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)
    
    return (xpath_doc_type, owner, driver)


def doc_type_Nassau_match():
    """
    Nassau doc type match
    """
    return ('MORTGAGE', 'MULE')


def click_image_Nassau(comb, driver):
    """
    Nassau click image
    """

    click = driver.find_element_by_xpath(
        '/html/body/form/span/table/tbody/tr[6]/td/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[1]/a')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    click.click()

    return (driver)


def predownload_Nassau(driver):
    """
    Switch driver to appropriate frame to download from Nassau county
    """

    driver.switch_to.frame('doc')
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/form/table/tbody/tr[1]/'
                                                                          'td/input[1]')))
    driver.find_element_by_xpath('/html/body/form/table/tbody/tr[1]/td/input[1]').click()

    return (driver)


def download_Nassau(driver):
    """
    Initiate Nassau download
    """

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/div[3]/div/div[1]'
                                                                          '/div[2]/button[4]')))
    driver.find_element_by_xpath(
        '/html/body/div[1]/div[2]/div[3]/div/div[1]/div[2]/button[4]').click()
    
    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')
    
    return (driver)


def revert_Nassau(driver):
    """
    Restore Nassau search after download
    """

    driver.switch_to.default_content()
    driver.execute_script("history.back();")
    time.sleep(1)
    driver.execute_script("history.back();")

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((Okeechobee))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Okeechobee_search_results_count(driver):
    """
    Retrieve the number of Okeechobee county's search results
    """

    try:
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located(
                (By.XPATH, '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[4]/div/div[2]')))

    except TimeoutException:
        search_results = 0

    else:
        
        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[4]/div/div[2]').text)
        
        search_results = convert_Okeechobee_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Okeechobee_raw_search_results(raw_search_results):
    """
    Convert Okeechobee raw search results
    """

    total = re.search('[0-9]+ total', raw_search_results)
    totalnum = total.group(0)
    getsearch_results = re.search('[0-9]+', totalnum)
    search_results = int(getsearch_results.group(0))

    return (search_results)


def sort_Okeechobee(driver):
    """
    Sort Okeechobee search results by ascending order
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/' +
                                 'div/div[1]/div/div[1]/div[2]/div/div[5]/div/span[4]').click()

    return (driver)


def pagination_Okeechobee(comb, driver):
    """
    Pagination for Okeechobee county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        if comb['search_instance'] % 25 == 0:

            # Paginate element
            driver.find_element_by_xpath(
                '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[4]/div/div[1]/button[' + str(
                    comb['pagination_counter']) +
                ']').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 1

            # Increment pagination counter
            comb['pagination_counter'] = comb['pagination_counter'] + 1

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/' +
                                                                              'div/div[4]/div[2]/div/div/div[1]/div[1]/button')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Okeechobee(comb, driver):
    """
    Okeechobee xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/' + \
                     'div/div[4]/div[2]/div/div/div[' + str(comb['doc_counter']) + ']/div[6]'

    xpath_owner = '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/' + \
                  'div/div[4]/div[2]/div/div/div[' + str(comb['doc_counter']) + ']/div[4]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Okeechobee_match():
    """
    Okeechobee doc type match
    """

    return ('MTG', 'MULE')


def click_image_Okeechobee(comb, driver):
    """
    Okeechobee click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/div/div[4]/' +
        'div[2]/div/div/div[' + str(comb['doc_counter']) + ']/div[1]/button').click()
    
    return (driver)


def predownload_Okeechobee(driver):
    """
    Switch driver to appropriate frame to download from Okeechobee county
    """

    WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/button[7]')))
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/button[7]').click()
    WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/span[2]/a')))

    return (driver)


def download_Okeechobee(driver):
    """
    Initiate Okeechobee download
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/span[2]/a').click()

    return (driver)


def revert_Okeechobee(driver):
    """
    Restore Okeechobee search after download
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[2]/ul/li[2]/a').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Osceola)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Osceola_search_results_count(driver):
    """
    Retrieve the number of Osceola county's search results
    """

    try:
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/table[3]/tbody/tr[3]/td[2]/font')))

    except TimeoutException:
        search_results = 0

    else:
        raw_search_results = str(
            driver.find_element_by_xpath('/html/body/form/table[3]/tbody/tr[3]/td[2]/font').text)
        
        search_results = convert_Osceola_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Osceola_raw_search_results(raw_search_results):
    """
    Convert Osceola raw search results
    """

    total = re.search('[0-9]+ Records', raw_search_results)
    totalnum = total.group(0)
    getsearch_results = re.search('[0-9]+', totalnum)
    search_results = int(getsearch_results.group(0))

    return (search_results)


def pagination_Osceola(comb, driver):
    """
    Pagination for Osceola county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 20 == 0:

            # Paginate element
            driver.find_element_by_xpath('/html/body/form/strong/font/a').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 2

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/form/table[4]/tbody/tr[2]/td[4]/a/font')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_Osceola(comb):
    """
    Osceola xpath doc type
    """

    xpath_doc_type = '/html/body/form/table[4]/tbody/tr[' + str(comb['doc_counter']) + ']/td[4]/a/font'

    return (xpath_doc_type)


def doc_type_Osceola_match():
    """
    Osceola doc type match
    """

    return ('MTG  ', 'MULE')


def click_image_Osceola(comb, driver):
    """
    Osceola click image
    """

    driver.find_element_by_xpath(
        '/html/body/form/table[4]/tbody/tr[(' + str(comb['doc_counter']) + ')]/td[1]/font/a').click()

    return (driver)


def predownload_Osceola(driver):
    """
    Switch driver to appropriate frame to download from Osceola county
    """

    WebDriverWait(driver, 10).until(
        EC.frame_to_be_available_and_switch_to_it(driver.find_element_by_name('FrmDes')))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
                                                                '/html/body/table[2]/tbody/tr/td[2]/map[4]/area')))
    driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td[2]/map[4]/area').click()

    driver.switch_to.default_content()

    driver.switch_to.frame(driver.find_element_by_name('FrmImg'))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
                                                                '/html/body/table[3]/tbody/tr[1]/td[1]/form/input[14]')))

    return (driver)


def download_Osceola(driver):
    """
    Initiate Osceola download
    """

    driver.find_element_by_xpath('/html/body/table[3]/tbody/tr[1]/td[1]/form/input[14]').click()
    driver.switch_to.alert.accept()
    driver.switch_to.default_content()

    return (driver)


def revert_Osceola(driver):
    """
    Restore Osceola search after download
    """

    #driver.switch_to.default_content()
    driver.switch_to.frame(driver.find_element_by_name('FrmDes'))

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td[2]/table[4]/tbody/tr[11]/td[2]/dl/dt')
    owner = str(raw_owner.text)

    driver.execute_script("window.history.go(-2)")
    time.sleep(1) 
    driver.switch_to.default_content()
    
    return (owner, driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Palm Beach)))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def PalmBeach_search_results_count(driver):
    """
    Retrieve the number of Palm Beach county's search results
    """

    try:
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/table[3]/tbody/tr[4]/td[2]/font')))
        
    except TimeoutException:
        search_results = 0

    else:
        
        raw_search_results = str(
            driver.find_element_by_xpath('/html/body/form/table[3]/tbody/tr[4]/td[2]/font').text)
        
        search_results = convert_PalmBeach_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_PalmBeach_raw_search_results(raw_search_results):
    """
    Convert Palm Beach raw search results
    """
    
    total = re.search('[0-9]+ Records', raw_search_results)
    totalnum = total.group(0)
    getsearch_results = re.search('[0-9]+', totalnum)
    search_results = int(getsearch_results.group(0))

    return (search_results)

    
def pagination_PalmBeach(comb, driver):
    """
    Pagination for Palm Beach county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 20 == 0:

            # Paginate element
            driver.find_element_by_xpath('/html/body/form/strong/font/a').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 2

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/form/table[4]/tbody/tr[2]/td[5]/a/font')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_PalmBeach(comb, driver):
    """
    Palm Beach xpath doc type and owner
    """

    xpath_doc_type = '/html/body/form/table[4]/tbody/tr[' + str(comb['doc_counter']) + ']/td[5]/a/font'

    xpath_owner = '/html/body/form/table[4]/tbody/tr[' + str(comb['doc_counter']) + ']/td[3]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_PalmBeach_match():
    """
    Palm Beach doc type match
    """

    return ('MTG  ', 'MULE')


def click_image_PalmBeach(comb, driver):
    """
    Palm Beach click image
    """

    driver.find_element_by_xpath(
        '/html/body/form/table[4]/tbody/tr[(' + str(comb['doc_counter']) + ')]/td[1]/font/a').click()

    return (driver)


def predownload_PalmBeach(driver):
    """
    Switch driver to appropriate frame to download from PalmBeach county
    """

    WebDriverWait(driver, 10).until(
        EC.frame_to_be_available_and_switch_to_it(driver.find_element_by_name('FrmDes')))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
                                                                '/html/body/table[2]/tbody/tr/td[2]/map[4]/area')))

    driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td[2]/map[4]/area').click()

    driver.switch_to.default_content()

    driver.switch_to.frame(driver.find_element_by_name('FrmImg'))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
                                                                '/html/body/table[2]/tbody/tr[1]/td[1]/form')))

    return (driver)


def download_PalmBeach(driver):
    """
    Initiate Palm Beach download
    """

    driver.find_element_by_xpath('/html/body/table[2]/tbody/tr[1]/td[1]/form').click()
    driver.switch_to.alert.accept()

    driver.switch_to.default_content()

    WebDriverWait(driver, 10).until(
        EC.frame_to_be_available_and_switch_to_it(driver.find_element_by_xpath('/html/frameset/frame[2]')))

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="download"]')))
    driver.find_element_by_xpath('//*[@id="download"]').click()
    
    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_PalmBeach(driver):
    """
    Restore Palm Beach search after download
    """
    driver.switch_to.default_content()
    driver.switch_to.frame(driver.find_element_by_name('FrmDes'))
    driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td[2]/map[1]/area').click()
    driver.switch_to.default_content()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Pinellas)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def checkbox_Pinellas(driver):
    """
    Ensure whether or not names checkbox pops up and switch to mark it if it does
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, '/html/body/div[7]/div[2]/div/form/div[3]/input')))
        
    except TimeoutException:
        
        check_box_exist = False
        return (check_box_exist, driver)
    
    else:
        
        check_box_exist = True
        return (check_box_exist, driver)


def Pinellas_search_results_count(check_box_exist, driver):
    """
    Retrieve the number of Pinellas county's search results
    """

    if check_box_exist == False:
        
        search_results = 0
        return (search_results, driver)
    
    else:

        # Select all names variations and delay for 1 sec before beginning search otherwise it won't process
        driver.find_element_by_xpath(
            '/html/body/div[7]/div[2]/div/form/div[3]/input').click()

        # Wait for first xpath_doc_type to appear before proceeding to get search results otherwise it won't process
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[8]')))

        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]/div[3]').text)
        
        search_results = convert_Pinellas_raw_search_results(raw_search_results)

        return (search_results, driver)

def convert_Pinellas_raw_search_results(raw_search_results):
    """
    Convert Pinellas raw search results
    """

    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)
    

def sort_Pinellas(driver):
    """
    Pinellas county already by ascending order
    """

    driver.find_element_by_xpath('/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/' +
                                 'div[3]/div/table/tbody/tr/th[7]/a').click()

    return (driver)


def pagination_Pinellas(comb, driver):
    """
    Pagination for Pinellas county
    """

    # Pagination to load more ...
    if comb['pagination_counter'] == comb['pagination_load_more']:

        # ...
        driver.find_element_by_xpath(
            '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]/div[2]/div[1]/a[10]').click()

        # xpath_doc_type
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[8]')))

        # Add 10 to comb['pagination_load_more'] so it can load more results correctly
        comb['pagination_load_more'] = comb['pagination_load_more'] + 10
        comb['doc_counter'] = 1

    else:
        pass

    # Pagination
    if comb['search_instance'] == 0:
        pass
    
    else:
        
        if comb['search_instance'] % 11 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            comb['pagination_counter'] = comb['pagination_counter'] + 1

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[8]')))
        else:
            pass

    return (comb, driver)


def xpath_doc_type_and_owner_Pinellas(comb, driver):
    """
    Pinellas xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + \
                     str(comb['doc_counter']) + ']/td[8]'

    xpath_owner = '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + \
                  str(comb['doc_counter']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Pinellas_match():
    """
    Pinellas doc type match
    """

    return ('MORTGAGE', 'AGREEMENT AND OR CONTRACT FOR DEED')


def click_image_Pinellas(comb, driver):
    """
    Pinellas click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[2]/div[5]/div[1]/div[1]/div[3]/div[4]/div/div[1]/div[4]/table/tbody/tr[' +
        str(comb['doc_counter']) + ']/td[8]').click()

    return (driver)


def predownload_Pinellas(driver):
    """
    Switch driver to appropriate frame to download from Pinellas county
    """

    time.sleep(1)
    driver.switch_to.window(driver.window_handles[-1])

    WebDriverWait(driver, 15).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'imgFrame1')))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="SaveDoc"]')))
    driver.find_element_by_xpath('//*[@id="SaveDoc"]').click()
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '//*[@id="form1"]/div[1]/div[2]/button[3]/img')))

    return (driver)


def download_Pinellas(driver):
    """
    Initiate Pinellas download
    """

    driver.find_element_by_xpath('//*[@id="form1"]/div[1]/div[2]/button[3]/img').click()

    return (driver)


def revert_Pinellas(driver):
    """
    Restore Pinellas search after download
    """

    driver.close()
    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Polk)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Polk_search_results_count(driver):
    """
    Retrieve the number of Polk county's search results
    """

    try:
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located(
                (By.XPATH, '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[4]/div/div[2]')))

    except TimeoutException:
        search_results = 0

    else:
        
        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[4]/div/div[2]').text)
        
        search_results = convert_Polk_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Polk_raw_search_results(raw_search_results):
    """
    Convert Polk raw search results
    """

    total = re.search('[0-9]+ total', raw_search_results)
    totalnum = total.group(0)
    getsearch_results = re.search('[0-9]+', totalnum)
    search_results = int(getsearch_results.group(0))
    
    return (search_results)

    
def sort_Polk(driver):
    """
    Sort Polk search results by ascending order
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/' +
                                 'div/div[1]/div/div[1]/div[2]/div/div[5]/div/span[4]').click()

    return (driver)


def pagination_Polk(comb, driver):
    """
    Pagination for Polk county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 25 == 0:

            # Paginate element
            driver.find_element_by_xpath(
                '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[4]/div/div[1]/button[' + str(
                    comb['pagination_counter']) +
                ']').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 1

            # Increment pagination counter
            comb['pagination_counter'] = comb['pagination_counter'] + 1

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/' +
                                                                              'div/div[4]/div[2]/div/div/div[1]/div[1]/button')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_Polk(comb):
    """
    Polk xpath doc type
    """

    xpath_doc_type = '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/' + \
                     'div/div[4]/div[2]/div/div/div[' + str(comb['doc_counter']) + ']/div[5]'

    return (xpath_doc_type)


def doc_type_Polk_match():
    """
    Polk doc type match
    """

    return ('MTG', 'AGD')


def click_image_Polk(comb, driver):
    """
    Polk click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/div/div[4]/' +
        'div[2]/div/div/div[' + str(comb['doc_counter']) + ']/div[1]/button').click()

    return (driver)


def predownload_Polk(driver):
    """
    Switch driver to appropriate frame to download from Polk county
    """
    WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/button[7]')))
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/button[7]').click()
    WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                      '/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/span[2]/a')))

    return (driver)


def download_Polk(driver):
    """
    Initiate Polk download
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[3]/section[2]/div/div[2]/span[2]/a').click()

    return (driver)


def revert_Polk(driver):
    """
    Restore Polk search after download
    """
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(
        '/html/body/div[1]/div[2]/div/div[3]/section[1]/div/table/tbody/tr[9]/td[2]/div')
    owner = str(raw_owner.text)
        
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/ul/li[2]/a').click()

    return (owner, driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((Saint Johns))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def SaintJohns_search_results_count(driver):
    """
    Retrieve the number of Saint Johns county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[1]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[1]/b').text)

    search_results = convert_SaintJohns_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_SaintJohns_raw_search_results(raw_search_results):
    """
    Convert Saint Johns raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_SaintJohns(driver):
    """
    Sort Saint Johns search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/table/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_SaintJohns(driver):
    """
    Pagination for Saint Johns county
    """

    select = Select(driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[3]/label/b/select'))

    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_SaintJohns(comb, driver):
    """
    Saint Johns xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' + \
                     'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' + \
                  'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_SaintJohns_match():
    """
    Saint Johns doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_SaintJohns(comb, driver):
    """
    Saint Johns click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' +
        'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_SaintJohns(driver):
    """
    Initiate Saint Johns download
    """

    driver.find_element_by_xpath(
        '//*[@id="idViewGroup"]').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_SaintJohns(driver):
    """
    Restore Saint Johns search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)

# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Saint Lucie)))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def checkbox_SaintLucie(driver):
    """
    Ensure whether or not names checkbox pops up and switch to mark it if it does
    """

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, '/html/body/div[7]/div[2]/div/form/div[2]/div/ul/li/div/span[2]/input[2]')))
        
    except TimeoutException:
        
        check_box_exist = False
        return (check_box_exist, driver)
    
    else:
        
        check_box_exist = True
        return (check_box_exist, driver)


def SaintLucie_search_results_count(check_box_exist, driver):
    """
    Retrieve the number of Saint Lucie county's search results
    """

    if check_box_exist == False:
        
        search_results = 0
        return (search_results, driver)
    
    else:

        # Select all names variations and delay for 1 sec before beginning search otherwise it won't process
        driver.find_element_by_xpath(
            '/html/body/div[7]/div[2]/div/form/div[2]/div/ul/li/div/span[2]/input[2]').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[7]/div[2]/div/form/div[3]/input').click()

        # Wait for "Loading" image to disappear otherwise search won't process
        WebDriverWait(driver, 10).until(EC.invisibility_of_element_located((By.XPATH, '/html/body/div[4]/img')))

        # Wait for first xpath_doc_type to appear before proceeding to get search results otherwise it won't process
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[7]')))

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[3]')))

        raw_search_results = str(
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[3]').text)
        
        search_results = convert_SaintLucie_raw_search_results(raw_search_results)

        # Toggle search result window so second lender can be fired up if needed
        driver.find_element_by_xpath('//*[@id="toggleForm"]').click()

        return (search_results, driver)


def convert_SaintLucie_raw_search_results(raw_search_results):
    """
    Convert Saint Lucie raw search results
    """
    
    raw_search_results_split = raw_search_results.split()
    search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_SaintLucie(driver):
    """
    Sort Saint Lucie search results by ascending order
    """

    driver.find_element_by_xpath('/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[3]/' +
                                 'div/table/tbody/tr/th[8]/a').click()

    return (driver)


def pagination_SaintLucie(comb, driver):
    """
    Pagination for Saint Lucie county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 10 == 0:
            
            driver.find_element_by_xpath(
                '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[2]/div[2]/a[3]/span').click()
            comb['doc_counter'] = 1
            comb['print_search_instance'] = 1
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[1]/td[9]')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_SaintLucie(comb, driver):
    """
    Saint Lucie xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
        comb['doc_counter']) + ']/td[9]'

    xpath_owner = '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + \
                  str(comb['doc_counter']) + ']/td[7]'
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_SaintLucie_match():
    """
    Saint Lucie doc type match
    """

    return ('MTG', 'MULE')


def click_image_SaintLucie(comb, driver):
    """
    Saint Lucie click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[2]/div[5]/div[1]/div[1]/div[2]/div[4]/div/div[1]/div[4]/table/tbody/tr[' + str(
            comb['doc_counter']) + ']/td[9]').click()

    return (driver)


def predownload_SaintLucie(driver):
    """
    Switch driver to appropriate frame to download from SaintLucie county
    """

    driver.switch_to.window(driver.window_handles[1])
    WebDriverWait(driver, 15).until(EC.frame_to_be_available_and_switch_to_it((By.ID, 'imgFrame1')))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="SaveDoc"]')))

    return (driver)


def download_SaintLucie(driver):
    """
    Initiate Saint Lucie download
    """

    driver.find_element_by_xpath('//*[@id="SaveDoc"]').click()
    driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div[1]/form/div[1]/div[2]/button[3]').click()

    return (driver)


def revert_SaintLucie(driver):
    """
    Restore SaintLucie search after download
    """

    driver.switch_to.window(driver.window_handles[-1])
    driver.close()
    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Sarasota)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Sarasota_search_results_count(driver):
    """
    Retrieve the number of Sarasota county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH, '//*[@id="cphNoMargin_cphNoMargin_SearchCriteriaTop_TotalRows"]')))

    search_results = int(
        driver.find_element_by_xpath('//*[@id="cphNoMargin_cphNoMargin_SearchCriteriaTop_TotalRows"]').text)

    return (search_results, driver)


def sort_Sarasota(driver):
    """
    Sort Sarasota search results by ascending order
    """

    sort = Select(
        driver.find_element_by_xpath('//*[@id="cphNoMargin_cphNoMargin_OptionsBar1_ddlSortColumns"]'))
    sort.select_by_visible_text('Date Filed, Ascending')

    return (driver)


def pagination_Sarasota(comb, driver):
    """
    Pagination for Sarasota county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 25 == 0:

            # Paginate element
            driver.find_element_by_xpath(
                '//*[@id="OptionsBar2_imgNext"]').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 2

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              "//td[@id='x:1533160306.24:val:'D'']")))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Sarasota(comb, driver):
    """
    Sarasota xpath doc type and owner
    """

    xpath_doc_type = '/html[1]/body[1]/div[3]/form[1]/div[3]/div[3]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/' + \
                     'table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[2]/tr[1]/td[1]/div[2]/table[1]/tbody[1]/tr[' + \
                     str(comb['doc_counter']) + ']/td[10]'

    xpath_owner = '/html[1]/body[1]/div[3]/form[1]/div[3]/div[3]/table[1]/tbody[1]/tr[4]/td[1]/div[1]/div[1]/' + \
                  'table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[2]/tr[1]/td[1]/div[2]/table[1]/tbody[1]/tr[' + \
                  str(comb['doc_counter']) + ']/td[11]/div[1]/span[2]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Sarasota_match():
    """
    Sarasota doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Sarasota(comb, driver):
    """
    Sarasota click image
    """

    click = driver.find_element_by_xpath('/html/body/div[3]/form/div[3]/div[3]/table/tbody/tr[4]/td/div/div/table/tbody/' +
                                 'tr[2]/td/table/tbody[2]/tr/td/div[2]/table/tbody/tr[' +
                                 str(comb['doc_counter']) + ']/td[2]/div')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    ActionChains(driver).move_to_element(click).perform()
    click.click()
    
    return (driver)


def predownload_Sarasota(driver):
    """
    Switch driver to appropriate frame to download from Sarasota county
    """

    time.sleep(1)
    view_image = driver.window_handles[1]
    driver.switch_to.window(view_image)
    time.sleep(1)

    driver.switch_to.frame(0)

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="btnProcessNow__5"]')))
    time.sleep(3)
    driver.find_element_by_xpath('//*[@id="btnProcessNow__5"]').click()
    inner_frame = driver.find_element_by_id('ifrImageWindow')
    driver.switch_to.frame(inner_frame)
    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.CSS_SELECTOR, '#pdf > p:nth-child(1) > a:nth-child(1)')))
    
    return (driver)


def download_Sarasota(driver):
    """
    Initiate Sarasota download
    """

    driver.find_element_by_css_selector('#pdf > p:nth-child(1) > a:nth-child(1)').click()

    return (driver)


def revert_Sarasota(driver):
    """
    Restore Sarasota search after download
    """

    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Seminole)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Seminole_search_results_count(driver):
    """
    Retrieve the number of Seminole county's search results
    """

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="grid_pager_label"]')))

    raw_search_results = str(driver.find_element_by_xpath('//*[@id="grid_pager_label"]').text)
    
    search_results = convert_Seminole_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Seminole_raw_search_results(raw_search_results):
    """
    Convert Seminole raw search results
    """

    total = re.search('[0-9]+ records', raw_search_results)
    total_num = total.group(0)
    get_search_results = re.search('[0-9]+', total_num)
    search_results = int(get_search_results.group(0))

    return (search_results)


def sort_Seminole(driver):
    """
    Sort Seminole search results by ascending order
    """

    driver.find_element_by_xpath('/html/body/div[1]/div[3]/div[1]/div[2]/div/div[3]/table/thead/tr[1]/th[8]/span').click()

    return (driver)


def pagination_Seminole(comb, driver):
    """
    Pagination for Seminole county
    """

    if comb['search_instance'] == 0:
        return (comb, driver)
    
    else:
        
        if comb['search_instance'] % 31 == 0:
            
            # Paginate element
            driver.find_element_by_xpath('/html/body/div[1]/div[3]/div[1]/div[2]/div/div[38]/div/div[4]/span').click()

            # Reset search parameter counters
            comb['print_search_instance'] = 1
            comb['doc_counter'] = 1

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,'/html/body/div[1]/div[3]/div[1]/div[2]/div/div[3]/table/tbody/tr[1]/td[7]')))

            return (comb, driver)
        
        else:
            return (comb, driver)


def xpath_doc_type_and_owner_Seminole(comb, driver):
    """
    Seminole xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[1]/div[3]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[7]'

    xpath_owner = '/html/body/div[1]/div[3]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Seminole_match():
    """
    Seminole doc type match
    """

    return ('MORTGAGE', 'AGREEMENT FOR DEED')


def click_image_Seminole(comb, driver):
    """
    Seminole click image
    """

    click = driver.find_element_by_xpath('/html/body/div[1]/div[3]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(comb['doc_counter']) + ']/td[7]')

    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    ActionChains(driver).move_to_element(click).perform()
    click.click()

    return (driver)


def predownload_Seminole(driver):
    """
    Switch driver to appropriate frame to download from Seminole county
    """

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="download_icon"]')))

    click = driver.find_element_by_xpath('//*[@id="download_icon"]')
    
    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    ActionChains(driver).move_to_element(click).perform()
    click.click()

    time.sleep(1)

    driver.switch_to.window(driver.window_handles[-1])
    
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,'//*[@id="download"]')))

    return (driver)


def download_Seminole(driver):
    """
    Initiate Seminole download
    """

    driver.find_element_by_xpath('//*[@id="download"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Seminole(driver):
    """
    Restore Seminole search after download
    """

    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()
    
    click = driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[2]/span')
    
    driver.execute_script("arguments[0].scrollIntoView();", click)
    time.sleep(1)
    ActionChains(driver).move_to_element(click).perform()
    click.click()

    time.sleep(1)
    
    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Suwannee)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Suwannee_search_results_count(driver):
    """
    Retrieve the number of Suwannee county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b').text)
    
    search_results = convert_Suwannee_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Suwannee_raw_search_results(raw_search_results):
    """
    Convert Suwanee raw search_results
    """
    
    if raw_search_results == 'Returned 0 records':
        search_results = 0
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)
    

def sort_Suwannee(driver):
    """
    Sort Suwannee search results by ascending order
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Suwannee(driver):
    """
    Pagination for Suwannee county
    """

    # Show all results so pagination is not required
    select = Select(
        driver.find_element_by_xpath(
            "//select[@size='1']"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Suwannee(comb, driver):
    """
    Suwannee xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' + str(
        comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' + \
                  str(comb['print_search_instance']) + ']/td[6]'
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Suwannee_match():
    """
    Suwannee doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Suwannee(comb, driver):
    """
    Suwannee click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/table/tbody/tr[' + str(
            comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Suwannee(driver):
    """
    Initiate Suwannee download
    """

    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/a').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    return (driver)


def revert_Suwannee(driver):
    """
    Restore Suwannee search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Volusia)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Volusia_search_results_count(driver):
    """
    Retrieve the number of Volusia county's search results
    """

    try:

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH, '/html[1]/body[1]/form[1]/div[5]/div[1]/div[4]/div[1]/div[1]/table[1]/' +
             'tbody[1]/tr[1]/td[1]/select[1]')))

    except TimeoutException:

        search_results = 0

    else:

        show = Select(driver.find_element_by_xpath('/html[1]/body[1]/form[1]/div[5]/div[1]/div[4]/div[1]/div[1]/' +
                                                   'table[1]/tbody[1]/tr[1]/td[1]/select[1]'))
        show.select_by_visible_text('500')

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
            (By.XPATH,
             '/html[1]/body[1]/form[1]/div[5]/div[1]/div[4]/div[1]/div[1]/table[1]/tbody[1]/tr[3]/td[5]/a[1]')))

        search_results = int(driver.find_element_by_xpath(
            '/html/body/form/div[5]/div[1]/div[4]/div/div/table/tbody/tr[1]/td/span[1]/span').text)

        if search_results < 500:
            pass
        
        else:
            search_results = 500

    return (search_results, driver)


def xpath_doc_type_and_owner_Volusia(comb, driver):
    """
    Volusia xpath doc type and owner
    """

    xpath_doc_type = '/html[1]/body[1]/form[1]/div[5]/div[1]/div[4]/div[1]/div[1]/table[1]/tbody[1]/tr[' + \
                     str(comb['doc_counter']) + ']/td[5]/a[1]'

    driver.find_element_by_xpath('/html[1]/body[1]/form[1]/div[5]/div[1]/div[4]/div[1]/div[1]/table[1]' +
                                 '/tbody[1]/tr[' + str(comb['doc_counter']) + ']/td[2]/a[1]').click()

    time.sleep(1)

    driver.switch_to.window(driver.window_handles[1])

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="dirName"]')))
    
    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath('//*[@id="dirName"]')
    owner = str(raw_owner.text)

    driver.close()

    driver.switch_to.window(driver.window_handles[0])

    return (xpath_doc_type, owner, driver)


def doc_type_Volusia_match():
    """
    Volusia doc type match
    """

    return ('MG', 'MULE')


def click_image_Volusia(comb, driver):
    """
    Volusia click image
    """

    driver.find_element_by_xpath('/html[1]/body[1]/form[1]/div[5]/div[1]/div[4]/div[1]/div[1]/table[1]/' +
                                 'tbody[1]/tr[' + str(comb['doc_counter']) + ']/td[1]/a[1]/img[1]').click()

    return (driver)


def predownload_Volusia(driver):
    """
    Switch driver to appropriate frame to download from Volusia county
    """

    time.sleep(1)
    viewimage = driver.window_handles[1]
    driver.switch_to.window(viewimage)

    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '#download')))

    return (driver)


def download_Volusia(driver):
    """
    Initiate Volusia download
    """

    driver.find_element_by_css_selector('#download').click()
    
    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Volusia(driver):
    """
    Restore Volusia search after download
    """

    driver.switch_to.window(driver.window_handles[1])
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Wakulla))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Wakulla_search_results_count(driver):
    """
    Retrieve the number of Wakulla county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b').text)

    search_results = convert_Wakulla_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Wakulla_raw_search_results(raw_search_results):
    """
    Convert Wakulla raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)


def sort_Wakulla(driver):
    """
    Sort Wakulla search results by ascending order
    """

    driver.find_element_by_xpath(
        '//*[@id="resultsTable"]/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Wakulla(driver):
    """
    Pagination for Wakulla county
    """

    select = Select(driver.find_element_by_xpath("//select[@size='1']"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Wakulla(comb, driver):
    """
    Wakulla xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' + \
                     'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[8]/' + \
                  'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Wakulla_match():
    """
    Wakulla doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Wakulla(comb, driver):
    """
    Wakulla click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' +
        'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    driver.find_element_by_xpath(
        '//*[@id="idViewGroup"]').click()
    
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()

    time.sleep(1)
    
    driver.switch_to.window(driver.window_handles[-1])

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="download"]')))
    
    return (driver)


def download_Wakulla(driver):
    """
    Initiate Wakulla download
    """

    driver.find_element_by_xpath('//*[@id="download"]').click()

    open_window = False
    top_windows = mule_top_windows()

    while open_window == False:
        
        # Iterate through open windows to set foreground focus on download confirmation 
        win32gui.EnumWindows(windowEnumerationHandler, top_windows)
        
        for i in top_windows:
            
            if "Opening" in i[1]:
                
                win32gui.ShowWindow(i[0],5)
                win32gui.SetForegroundWindow(i[0])
                open_window = True
                break
            
            else:
                pass

    time.sleep(3)
        
    SendKeys('{ENTER}')

    return (driver)


def revert_Wakulla(driver):
    """
    Restore Wakulla search after download
    """

    driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.switch_to.default_content()
    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Walton))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def Walton_search_results_count(driver):
    """
    Retrieve the number of Walton county's search results
    """

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located(
            (By.XPATH,
             '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b')))

    raw_search_results = str(driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[2]/b').text)

    search_results = convert_Walton_raw_search_results(raw_search_results)

    return (search_results, driver)


def convert_Walton_raw_search_results(raw_search_results):
    """
    Convert Walton raw search results
    """

    if raw_search_results == 'Returned 0 records':
        search_results = 0
    else:
        raw_search_results_split = raw_search_results.split()
        search_results = int(raw_search_results_split[-1])

    return (search_results)

    
def sort_Walton(driver):
    """
    Sort Walton search results by ascending order
    """

    driver.find_element_by_xpath(
        '//*[@id="resultsTable"]/thead/tr[2]/th[8]').click()

    return (driver)


def pagination_Walton(driver):
    """
    Pagination for Walton county
    """

    select = Select(driver.find_element_by_xpath("//select[@size='1']"))
    select.select_by_value('-1')

    view_all = True

    return (view_all, driver)


def xpath_doc_type_and_owner_Walton(comb, driver):
    """
    Walton xpath doc type and owner
    """

    xpath_doc_type = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' + \
                     'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]'

    xpath_owner = '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div/div/div[3]/table/tbody/tr/td/div/div[8]/' + \
                  'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[6]'

    # Encode text as string in the event a value or symbol is returned
    raw_owner = driver.find_element_by_xpath(xpath_owner)
    owner = str(raw_owner.text)

    return (xpath_doc_type, owner, driver)


def doc_type_Walton_match():
    """
    Walton doc type match
    """

    return ('MORTGAGE', 'MULE')


def click_image_Walton(comb, driver):
    """
    Walton click image
    """

    driver.find_element_by_xpath(
        '/html/body/div[6]/div/div/div/div[2]/div/div[3]/div[1]/div/div[3]/table/tbody/tr/td/div/div[8]/' +
        'table/tbody/tr[' + str(comb['print_search_instance']) + ']/td[9]').click()

    WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="documentImageInner"]')))

    return (driver)


def download_Walton(driver):
    """
    Initiate Walton download
    """

    driver.find_element_by_xpath(
        '//*[@id="idViewGroup"]').click()
    driver.find_element_by_xpath(
        '/html/body/div[7]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/ul/li[2]/a').click()
    
    return (driver)


def revert_Walton(driver):
    """
    Restore Walton search after download
    """

    driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div[1]/div[1]/a[1]').click()

    return (driver)


# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------Misc------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def mule_top_windows():
    """
    Mule for download's top_windows
    """
    
    top_windows = []

    return top_windows


def windowEnumerationHandler(hwnd, top_windows):
    """
    Fetch window text from handle and stack it into the "top_windows" list
    """
    
    top_windows.append((hwnd, win32gui.GetWindowText(hwnd)))


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
