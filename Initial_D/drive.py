# ----------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Libraries-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import re
import os
import time
import glob
import json
import shift
import pandas
import shutil
import fnmatch
import datetime
import subprocess
import win32clipboard

from PIL import Image
from dateutil import parser
from itertools import chain
from shutil import copyfile
from selenium import webdriver
from pywinauto.keyboard import SendKeys
from pytesseract import image_to_string
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Functions---------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def ignite(dlc):
    """
    Set driver configs and fire up driver
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    # Designate Firefox
    if dlc['county_flag'] == 1:
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', config['download'])
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'application/pdf')

        if dlc['dataset']['county'][dlc['data']] in config['older_geckodriver_disable_pdfjs']:
            profile.set_preference('browser.download.useDownloadDir', True)
            profile.set_preference('pdfjs.disabled', True)

        else:
            pass

        if dlc['dataset']['county'][dlc['data']] in config['older_geckodriver_load']:
            path = config['older_geckodriver_path']
            driver = webdriver.Firefox(executable_path=path, firefox_profile=profile)
        else:
            driver = webdriver.Firefox(profile)

        driver.get(dlc['website'])

    # Designate Chrome
    else:
        options = webdriver.ChromeOptions()
        options.add_experimental_option("prefs", {
            "download.default_directory": config['download'],
            "plugins.always_open_pdf_externally": True,
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "safebrowsing.enabled": True
        })
        driver = webdriver.Chrome(chrome_options=options)
        driver.get(dlc['website'])

        if dlc['dataset']['county'][dlc['data']] in config['mfc']:

            WebDriverWait(driver, 20).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[2]/div[3]/form/div[6]/p[2]/select')))

        else:
            pass

    return (driver)


def drive(dlc, driver):
    """
    Master case template
    """

    dlc, mortgage_doc_found, owner = engage(dlc, driver)

    # Mark doc not found in Excel
    if mortgage_doc_found == 0:
        dlc = mark_doc_not_found(dlc)
        print(dlc['dataset']['holder'][dlc['data']] + ' marked - Doc Not Found')

    # Mark address error in Excel
    elif mortgage_doc_found == 2:
        dlc = mark_address_error(dlc)
        print(dlc['dataset']['holder'][dlc['data']] + ' marked - Address Error')

    # Mark holder not found in Excel
    elif mortgage_doc_found == 3:
        dlc = mark_holder_not_found(dlc)
        print(dlc['dataset']['holder'][dlc['data']] + ' marked - Holder Not Found')

    # Mark driver crashed in Excel
    elif mortgage_doc_found == 4:

        # Refer to config JSON for specs and designations
        with open('../misc/config.json') as config:
            config = json.load(config)

        dlc = mark_crash_and_cause(dlc)
        print(dlc['dataset']['holder'][dlc['data']] + ' marked - Driver Crashed' + ' - ' + config['crash_cause'][
            str(dlc['crash_cause'])])

    # Mark found in Excel
    else:
        dlc = mark_found(dlc, owner)
        dlc = ocr_maturity(dlc)
        print('Processed ' + dlc['set_name'])


def engage(dlc, driver):
    """
    Master driver session template
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    # Initialize crash flag and cause
    dlc['crash_flag'] = False
    dlc['crash_cause'] = 0
    dlc['name_field_crash'] = 0

    # Initialize termination switch
    termination_switch = False

    while termination_switch == False:

        try:

            print('Performing any necessary inits')
            driver = infuse(dlc, driver)  # Perform any necessary initial actions

        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not

            # Crash checkpoint 1
            print('Crashed at infuse')
            dlc['crash_flag'] = True
            dlc['crash_cause'] = 1
            mortgage_doc_found = 4
            owner = ''
            break

        else:
            if dlc['sole_holder_flag'] == False:

                # Entity initialization
                if dlc['dataset']['first_name'][dlc['data']] == dlc['dataset']['holder'][dlc['data']]:
                    dlc['set_holder_marker'] = 0

                    # NOTE: Entity held promissory notes are single holders so switch to single. Sole_holder_flag initially set
                    # as multiple back in rev. Set to single in order to further differentiate between an entity vs multiple holders.
                    dlc['sole_holder_flag'] = True

                    # Finally set entity name field for MyFloridaCounties
                    if dlc['dataset']['county'][dlc['data']] in config['mfc']:
                        dlc['name_field_1'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[3]/input'
                        dlc['name_field_2'] = ''

                    else:
                        pass

                    dlc, first_holder_found, mortgage_doc_found, owner, driver = auto(dlc, driver)

                    # Entity crash checkpoint
                    if dlc['crash_flag'] == True:
                        break
                    else:
                        pass

                # First holder initialization
                else:
                    dlc['set_holder_marker'] = 1

                    # Set individual name fields for MyFloridaCounties
                    if dlc['dataset']['county'][dlc['data']] in config['mfc']:
                        dlc['name_field_1'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[2]/input[1]'
                        dlc['name_field_2'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[2]/input[2]'

                    else:
                        pass

                    dlc, first_holder_found, mortgage_doc_found, owner, driver = auto(dlc, driver)

                    # First holder crash checkpoint
                    if dlc['crash_flag'] == True:
                        break
                    else:
                        pass

                    if first_holder_found in config['first_holder_found_flag']:
                        pass
                    else:

                        if dlc['crash_flag'] == True:
                            pass
                        else:

                            # Perform any necessary initial actions for second run
                            driver = overdrive(dlc, driver)

                            # Second holder initialization
                            dlc['set_holder_marker'] = 2

                            # Set individual name fields for MyFloridaCounties
                            if dlc['dataset']['county'][dlc['data']] in config['mfc']:
                                dlc['name_field_1'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[2]/input[1]'
                                dlc['name_field_2'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[2]/input[2]'

                            else:
                                pass

                            dlc, first_holder_found, mortgage_doc_found, owner, driver = auto(dlc, driver)

                            # Second holder crash checkpoint
                            if dlc['crash_flag'] == True:
                                break
                            else:
                                pass

            # Single holder initialization
            else:
                dlc['set_holder_marker'] = 1

                # Set name fields for MyFloridaCounties
                if dlc['dataset']['county'][dlc['data']] in config['mfc']:

                    if dlc['entity'] == True:
                        dlc['set_holder_marker'] = 0
                    else:
                        pass

                    if dlc['set_holder_marker'] == 1:
                        dlc['name_field_1'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[2]/input[1]'
                        dlc['name_field_2'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[2]/input[2]'

                    else:
                        dlc['name_field_1'] = '/html/body/div[1]/div[2]/div[3]/form/div[3]/p[3]/input'
                        dlc['name_field_2'] = ''

                else:
                    pass

                dlc, first_holder_found, mortgage_doc_found, owner, driver = auto(dlc, driver)

                # Single holder crash checkpoint
                if dlc['crash_flag'] == True:
                    break
                else:
                    pass

        termination_switch = True
        break

    driver.quit()

    return (dlc, mortgage_doc_found, owner)


def infuse(dlc, driver):
    """
    Perform any necessary prerequisite actions before search can be engaged
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    if county in config['mfc']:

        county = county.upper()

        select_county = Select(driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[6]/p[2]/select'))
        select_county.select_by_visible_text(county)

        if county == 'HOLMES':
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="simplemodal-data"]/p[3]/b/a')))
            driver.find_element_by_xpath('//*[@id="simplemodal-data"]/p[3]/b/a').click()
        else:
            pass

    elif county in config['landmark_infuse']:

        driver.find_element_by_xpath('/html/body/div[6]/div/div/div[3]/div[1]/div[1]/a').click()
        driver.find_element_by_xpath('/html/body/div[1]/div[3]/a[1]').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[2]/td[2]/input')))

    elif county == 'Alachua':

        driver.find_element_by_xpath('/html/body/div[3]/form/div[3]/div[1]/div[2]/div[1]/ul/li[5]/a/span').click()
        driver.find_element_by_xpath(
            '/html/body/div[3]/form/div[3]/div[1]/div[2]/div[2]/div/div/div/ul/li[2]/a/span').click()
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="cphNoMargin_f_txtParty"]')))


    elif county == 'Broward' or county == 'Pinellas':

        driver.find_element_by_xpath('//*[@id="btnButton"]').click()
        WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH,
                                                                         '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[2]/input[1]')))

    elif county == 'Collier':

        driver.switch_to.frame('blockrandom-240')
        driver.find_element_by_xpath('/html/body/div/div[1]/div[1]/ul/li[2]/a/span').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="search_lastname"]')))

    elif county in config['oncore_infuse']:

        driver.find_element_by_xpath('//*[@id="btnButton"]').click()

        if county == 'Lake':
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[2]/input[1]')))

        elif county == 'Brevard':
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/input[1]')))

        else:
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                              '/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[3]/div[2]/input[1]')))

    elif county == 'Marion':

        driver.find_element_by_xpath('//*[@id="ctl00_btnLegacyApp"]').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div/table[3]/tbody/tr/td/a')))
        driver.find_element_by_xpath('/html/body/div/table[3]/tbody/tr/td/a').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/tbody/tr[1]/td/font[2]/input')))

    elif county == 'MiamiDade':

        username = driver.find_element_by_xpath('//*[@id="ctl00_cphPage_txtUserName"]')
        username.click()
        username.send_keys(config['username'])

        password = driver.find_element_by_xpath('//*[@id="ctl00_cphPage_txtPassword"]')
        password.click()
        password.send_keys(config['password'])

        driver.find_element_by_xpath('//*[@id="ctl00_cphPage_btnLogin"]').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/form/span[1]/div[2]/nav/ul[2]/li[1]/a')))

        driver.find_element_by_xpath('/html/body/form/span[1]/div[2]/nav/ul[2]/li[1]/a').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div/div/div[1]/aside/div[1]/ul[2]/li[1]/a')))
        driver.find_element_by_xpath('/html/body/div/div/div[1]/aside/div[1]/ul[2]/li[1]/a').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="lnkStandardSearch2"]')))
        driver.find_element_by_xpath('//*[@id="lnkStandardSearch2"]').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="pfirst_party"]')))

    elif county == 'Nassau':

        driver.find_element_by_xpath('//*[@id="cmdAccept"]').click()
        WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="txtName"]')))

    elif county == 'Polk':

        driver.find_element_by_xpath(
            '//*[@id="post-2214"]/div/div/div[1]/div/div/div[2]/div/div/div/div/div/p[6]/a/img').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//input[@name='partyName']")))

    elif county == 'Seminole':

        click = driver.find_element_by_xpath('//*[@id="instruction_agreement_button"]')

        driver.execute_script("arguments[0].scrollIntoView();", click)
        time.sleep(1)
        ActionChains(driver).move_to_element(click).perform()
        click.click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="criteria_full_name"]')))

    elif county == 'Sarasota':

        driver.find_element_by_xpath(
            '/html/body/div[3]/form/div[3]/div[1]/div[2]/div[1]/ul/li[3]/a/span').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[3]/form/div[3]/div[1]/div[2]/div[2]/div/div/div/ul/li/a/span')))

        driver.find_element_by_xpath(
            '/html/body/div[3]/form/div[3]/div[1]/div[2]/div[2]/div/div/div/ul/li/a/span').click()

        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="cphNoMargin_f_txtParty"]')))

    elif county == 'Osceola':

        driver.find_element_by_xpath('/html/body/div/form/table/tbody/tr[6]/td/font/input').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="criteria_full_name"]')))

    elif county == 'Volusia':

        driver.find_element_by_xpath('//*[@id="accept"]').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="name"]')))

        driver.find_element_by_xpath('//*[@id="keepOpen"]').click()
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="fromDateTxt"]')))

    else:
        pass

    return (driver)


def auto(dlc, driver):
    """
    Master case search template
    """
    
    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)
    
    county = dlc['dataset']['county'][dlc['data']]

    # Initialize termination switch
    termination_switch = False
    
    while termination_switch == False:
        
        # Input search arguments
        dlc, driver = search_holder(dlc, driver)

        # Search holder crashpoint
        if dlc['crash_flag'] == True:
            first_holder_found = 2
            mortgage_doc_found = 4
            owner = ''
            termination_switch = True
            break
            
        else:
            pass


        # Check whether or not names checkbox pops up and fill it out accordingly
        if county in config['checkbox']:
            try:
                print('Checking checkboxes')
                check_box_exist, driver = eval('shift.checkbox_' + county + '(driver)')

            except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                dlc['crash_flag'] = True

            else:
                pass

            finally:

                # Crash checkpoint 11
                if dlc['crash_flag'] == True:
                    print('Crashed at checkbox')
                    dlc['crash_cause'] = 11
                    first_holder_found = 2
                    mortgage_doc_found = 4
                    owner = ''
                    termination_switch = True
                    break

                else:
                    pass
        else:
            pass

        try:

            # Retrieve the number of search results
            if county in config['mfc']:
                search_results, driver = shift.mfc_search_results_count(driver)

            elif county in config['checkbox']:
                search_results, driver = eval('shift.' + county + '_search_results_count(check_box_exist,driver)')

            else:
                search_results, driver = eval('shift.' + county + '_search_results_count(driver)')

        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
            dlc['crash_flag'] = True

        else:
            pass

        finally:

            # Crash checkpoint 12
            if dlc['crash_flag'] == True:
                print('Crashed at search results')
                dlc['crash_cause'] = 12
                first_holder_found = 2
                mortgage_doc_found = 4
                owner = ''
                termination_switch = True
                break

            else:
                pass

                # Comb through search results for image
        if search_results > 0:

            print('Found ' + str(search_results) + ' records for ' + dlc['set_name'])

            # Sort results by ascending order
            if county in config['skip_sort']:
                pass
            else:
                time.sleep(3)

                try:
                    print('Sorting results by ascending order')
                    driver = eval('shift.sort_' + county + '(driver)')

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 13
                    if dlc['crash_flag'] == True:
                        print('Crashed at sort')
                        dlc['crash_cause'] = 13
                        first_holder_found = 2
                        mortgage_doc_found = 4
                        owner = ''
                        termination_switch = True
                        break

                    else:
                        pass

                time.sleep(3)

            # Initialize combing settings
            comb = {}
            comb['search_instance'] = 0
            comb['print_search_instance'] = 1

            if county in config['comb_one']:
                comb['pagination_counter'] = 1
                comb['pagination_load_more'] = 10
                comb['doc_counter'] = 3

            elif county in config['comb_two']:
                comb['doc_counter'] = 1

            elif county in config['comb_three']:
                comb['doc_counter'] = 2
                comb['pagination_counter'] = 2

            elif county in config['comb_four']:
                comb['doc_counter'] = 1
                comb['pagination_counter'] = 2

            elif county == 'Collier' or county == 'Volusia':
                comb['doc_counter'] = 3

            elif dlc['dataset']['county'][dlc['data']] == 'Pinellas':
                comb['doc_counter'] = 1
                comb['pagination_counter'] = 0
                comb['pagination_load_more'] = 10

            else:
                comb['doc_counter'] = 0

            # Switch for counties that do not require pagination due to a view all option being present
            view_all = False

            while comb['print_search_instance'] <= search_results:

                try:
                    # Pagination for MyFloridaCounties
                    if county in config['mfc']:
                        comb, driver = shift.mfc_pagination(comb, driver)

                    # Counties that do not require further pagination after the first
                    elif county in config['pagination_switch']:

                        # Activate switch upon first pagination
                        if view_all == False:
                            view_all, driver = eval('shift.pagination_' + county + '(driver)')
                            time.sleep(3)
                        else:
                            pass

                    # Skip pagination for Volusia due to capped search results
                    elif county == 'Volusia':
                        pass

                    else:
                        comb, driver = eval('shift.pagination_' + county + '(comb,driver)')

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 14
                    if dlc['crash_flag'] == True:
                        print('Crashed at pagination')
                        dlc['crash_cause'] = 14
                        first_holder_found = 2
                        mortgage_doc_found = 4
                        owner = ''
                        termination_switch = True
                        break

                    else:
                        pass

                try:
                    # Obtain doc type's xpath and owner if applicable at this stage
                    if county in config['mfc']:
                        xpath_doc_type, owner, driver = shift.mfc_doc_type(comb, driver)

                    elif county in config['no_owner_yet']:
                        xpath_doc_type = eval('shift.xpath_doc_type_' + county + '(comb)')

                    else:
                        xpath_doc_type, owner, driver = eval(
                            'shift.xpath_doc_type_and_owner_' + county + '(comb,driver)')

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 15
                    if dlc['crash_flag'] == True:
                        print('Crashed at doc type\'s xpath and owner')
                        dlc['crash_cause'] = 15
                        first_holder_found = 2
                        mortgage_doc_found = 4
                        termination_switch = True
                        owner = ''
                        break

                    else:
                        pass

                try:

                    doc_type = id_doc_type(xpath_doc_type, driver)

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 16
                    if dlc['crash_flag'] == True:
                        print('Crashed at doc type')
                        dlc['crash_cause'] = 16
                        first_holder_found = 2
                        mortgage_doc_found = 4
                        owner = ''
                        termination_switch = True
                        break

                    else:
                        pass

                # Set doc type to search for according to county
                if county in config['mfc']:
                    match_doc_type_1, match_doc_type_2 = shift.mfc_doc_type_match()

                else:
                    match_doc_type_1, match_doc_type_2 = eval('shift.doc_type_' + county + '_match()')

                # Perform actions on found doc types
                if doc_type == match_doc_type_1 or doc_type == match_doc_type_2:

                    print('Result ' + str(comb['print_search_instance']) + ' is a ' + doc_type)

                    # Peform misc. actions before downloading if needed
                    if county in config['skip_misc_action']:
                        time.sleep(2)

                    else:

                        try:
                            driver = eval('shift.click_image_' + county + '(comb,driver)')

                        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                            dlc['crash_flag'] = True

                        else:
                            pass

                        finally:

                            # Crash checkpoint 17
                            if dlc['crash_flag'] == True:
                                print('Crashed at click image')
                                dlc['crash_cause'] = 17
                                first_holder_found = 2
                                mortgage_doc_found = 4
                                owner = ''
                                termination_switch = True
                                break

                            else:
                                pass

                        if county in config['skip_predownload']:
                            time.sleep(2)
                        else:

                            try:
                                driver = eval('shift.predownload_' + county + '(driver)')

                            except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                                dlc['crash_flag'] = True

                            else:
                                pass

                            finally:

                                # Crash checkpoint 18
                                if dlc['crash_flag'] == True:
                                    print('Crashed at predownload')
                                    dlc['crash_cause'] = 18
                                    first_holder_found = 2
                                    mortgage_doc_found = 4
                                    owner = ''
                                    termination_switch = True
                                    break

                                else:
                                    pass
                    
                    dlc, driver = download(dlc, comb, driver)

                    if dlc['crash_flag'] == True:

                        # Crash checkpoint 19
                        print('Crashed at download')
                        dlc['crash_cause'] = 19
                        first_holder_found = 2
                        mortgage_doc_found = 4
                        owner = ''
                        termination_switch = True
                        break

                    else:
                        pass

                    try:
                        # Revert drive to a searchable state
                        if county in config['mfc']:
                            driver = shift.mfc_revert(driver)

                        else:

                            if county in config['no_owner_yet']:
                                owner, driver = eval('shift.revert_' + county + '(driver)')
                            else:
                                driver = eval('shift.revert_' + county + '(driver)')

                    except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                        dlc['crash_flag'] = True

                    else:
                        pass

                    finally:

                        # Crash checkpoint 20
                        if dlc['crash_flag'] == True:
                            print('Crashed at revert')
                            dlc['crash_cause'] = 20
                            first_holder_found = 2
                            mortgage_doc_found = 4
                            owner = ''
                            termination_switch = True
                            break

                        else:
                            pass

                    # OCR examination to determine doc validity
                    mortgage_doc_found = initiate_ocr(dlc)

                    # Immediately report back to main drive if the image has been accepted
                    if mortgage_doc_found == 1:
                        break

                    else:
                        pass

                # Report back if the doc isn't a mortgage and resume combing
                else:
                    print('Result ' + str(comb['print_search_instance']) + ' is a ' + doc_type)

                comb['search_instance'] = comb['search_instance'] + 1
                comb['print_search_instance'] = comb['print_search_instance'] + 1

                if county == 'Collier':
                    comb['doc_counter'] = comb['doc_counter'] + 2

                else:
                    comb['doc_counter'] = comb['doc_counter'] + 1

            if dlc['crash_flag'] == True:
                pass
            else:

                first_holder_found = ocr_results(dlc, mortgage_doc_found)

                # Switch for second lender to run if the first holder is inconclusive
                # and the holder is a single entity/individual
                if first_holder_found != 3:
                    break

                else:
                    if first_holder_found == 3 and dlc['sole_holder_flag'] == True:
                        mortgage_doc_found = 3
                        break

                    else:
                        first_holder_found = 4
                        break

        # Immediately report no search results
        else:
            print('No records found for ' + dlc['set_name'])
            first_holder_found = 0
            mortgage_doc_found = 3
            owner = ''
            termination_switch = True
            break

    return (dlc, first_holder_found, mortgage_doc_found, owner, driver)


def search_holder(dlc, driver):
    """
    Input search arguments
    """
    
    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Initialize termination switch
    termination_switch = False
    
    while termination_switch == False:

        # ID as entity
        if dlc['set_holder_marker'] == 0:

            # MyFloridaCounty entity activation
            if county in config['mfc']:

                try:
                    driver = shift.mfc_entity(driver)

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 2
                    if dlc['crash_flag'] == True:
                        print('Crashed at mfc entity')
                        dlc['crash_cause'] = 2
                        break
                    else:
                        pass

            else:
                pass

            # Set entity name
            dlc['set_name'] = dlc['dataset']['holder'][dlc['data']]

        # ID as first lender
        elif dlc['set_holder_marker'] == 1:

            # Format first holder name
            if dlc['mid_initial_flag'] == True and dlc['comma_flag'] == False:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ' ' + dlc['dataset']['first_name'][
                    dlc['data']] + ' ' + dlc['dataset']['first_mid'][dlc['data']]

            elif dlc['mid_initial_flag'] == False and dlc['comma_flag'] == True:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ', ' + dlc['dataset']['first_name'][
                    dlc['data']]

            elif dlc['mid_initial_flag'] == True and dlc['comma_flag'] == True:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ', ' + dlc['dataset']['first_name'][
                    dlc['data']] + ' ' + dlc['dataset']['first_mid'][dlc['data']]

            elif dlc['mid_initial_flag'] == False and dlc['comma_flag'] == False:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ' ' + dlc['dataset']['first_name'][
                    dlc['data']]

        # ID as second lender
        elif dlc['set_holder_marker'] == 2:

            # Input second lender
            if dlc['mid_initial_flag'] == True and dlc['comma_flag'] == False:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ' ' + dlc['dataset']['second_name'][
                    dlc['data']] + ' ' + dlc['dataset']['second_mid'][dlc['data']]

            elif dlc['mid_initial_flag'] == False and dlc['comma_flag'] == True:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ', ' + dlc['dataset']['second_name'][
                    dlc['data']]

            elif dlc['mid_initial_flag'] == True and dlc['comma_flag'] == True:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ', ' + dlc['dataset']['second_name'][
                    dlc['data']] + ' ' + dlc['dataset']['second_mid'][dlc['data']]

            elif dlc['mid_initial_flag'] == False and dlc['comma_flag'] == False:
                dlc['set_name'] = dlc['dataset']['last_name'][dlc['data']] + ' ' + dlc['dataset']['second_name'][
                    dlc['data']]

        dlc, driver = fill_name(dlc, driver)

        if dlc['crash_flag'] == True:

            # Crash checkpoint 3
            if dlc['name_field_crash'] == 1:
                print('Crashed at one name field')
                dlc['crash_cause'] = 3

            # Crach checkpoint 4
            elif dlc['name_field_crash'] == 2:
                print('Crashed at two name fields')
                dlc['crash_cause'] = 4

            else:
                pass

            break

        else:
            pass

        # Input date for MyFloridaCounty counties
        if county in config['mfc']:

            try:
                driver = shift.mfc_dates(dlc, driver)

            except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                dlc['crash_flag'] = True

            else:
                pass

            finally:

                # Crash checkpoint 5
                if dlc['crash_flag'] == True:
                    print('Crashed at mfc dates')
                    dlc['crash_cause'] = 5
                    break

                else:
                    pass

        # Input date for other counties
        else:

            # Format dates if needed
            if county == 'Okeechobee' or county == 'Polk' or county == 'Seminole':
                mule_from_date = parser.parse(dlc['from_date'])
                from_date = mule_from_date.strftime('mule%m/mule%d/%Y').replace('mule0', 'mule').replace('mule', '')
                dlc['from_date'] = from_date

                mule_to_date = parser.parse(dlc['to_date'])
                to_date = mule_to_date.strftime('mule%m/mule%d/%Y').replace('mule0', 'mule').replace('mule', '')
                dlc['to_date'] = to_date

            else:
                pass

            print('Setting from date to ' + dlc['from_date'])

            try:

                setter = 'from'
                driver = fill_date(dlc, driver, setter)

            except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                dlc['crash_flag'] = True

            else:
                pass

            finally:

                # Crash checkpoint 6
                if dlc['crash_flag'] == True:
                    print('Crashed at setting from date')
                    dlc['crash_cause'] = 6
                    break

                else:
                    pass

            print('Setting to date to ' + dlc['to_date'])

            try:

                setter = 'to'
                driver = fill_date(dlc, driver, setter)

            except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                dlc['crash_flag'] = True

            else:
                pass

            finally:

                # Crash checkpoint 7
                if dlc['crash_flag'] == True:
                    print('Crashed at setting to date')
                    dlc['crash_cause'] = 7
                    break

                else:
                    pass

            # Set records per page if needed
            if county == 'Okeechobee' or county == 'Polk':

                try:
                    display = Select(
                        driver.find_element_by_xpath(
                            '/html/body/div[1]/div[2]/div/div[1]/div/div[3]/div[2]/span/select'))
                    display.select_by_visible_text('25')

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 8
                    if dlc['crash_flag'] == True:
                        print('Crashed at setting records')
                        dlc['crash_cause'] = 8
                        break

                    else:
                        pass

                        # Set records limit if needed
            elif county == 'Osceola' or county == 'PalmBeach':

                try:
                    show = Select(
                        driver.find_element_by_xpath('/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/' +
                                                     'td/form/table/tbody/tr[1]/td/table/tbody/tr/td[3]/font[2]/select[1]'))
                    show.select_by_visible_text('Show first 2000 records')

                except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
                    dlc['crash_flag'] = True

                else:
                    pass

                finally:

                    # Crash checkpoint 9
                    if dlc['crash_flag'] == True:
                        print('Crashed at setting records limit')
                        dlc['crash_cause'] = 9
                        break

                    else:
                        pass
            else:
                pass

        print('Attempting search for ' + dlc['set_name'])

        time.sleep(2)

        try:
            driver = search(dlc, driver)

        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
            dlc['crash_flag'] = True

        else:
            pass

        finally:

            # Crash checkpoint 10
            if dlc['crash_flag'] == True:
                print('Crashed at clicking search element')
                dlc['crash_cause'] = 10
                break

            else:
                pass

        break

    return (dlc, driver)


def fill_name(dlc, driver):
    """
    Determine how to input name based on retrieved number of name fields
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    if dlc['dataset']['county'][dlc['data']] in config['mfc']:
        if dlc['set_holder_marker'] == 0:
            dlc['num_name_fields'] = 1
        else:
            dlc['num_name_fields'] = 2
    else:

        if dlc['dataset']['county'][dlc['data']] in config['county_one']:
            dlc['num_name_fields'] = 1

        elif dlc['dataset']['county'][dlc['data']] == 'Collier' and dlc['set_holder_marker'] == 0:
            dlc['num_name_fields'] = 1

        else:
            dlc['num_name_fields'] = 2

    if dlc['num_name_fields'] == 1:

        try:
            driver = one_name_field(dlc, driver)

        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
            dlc['crash_flag'] = True
            dlc['name_field_crash'] = 1

        else:
            pass

    else:
        try:
            driver = two_name_fields(dlc, driver)

        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
            dlc['crash_flag'] = True
            dlc['name_field_crash'] = 2

        else:
            pass

    return (dlc, driver)


def one_name_field(dlc, driver):
    """
    Input for one name field
    """

    print('Inputting ' + dlc['set_name'])
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(dlc['set_name'])
    win32clipboard.CloseClipboard()
    driver.find_element_by_xpath(dlc['name_field_1']).clear()
    ActionChains(driver).key_down(Keys.CONTROL).send_keys('v').key_up(Keys.CONTROL).perform()

    return (driver)


def two_name_fields(dlc, driver):
    """
    Input for two name fields
    """

    if dlc['set_holder_marker'] == 1:
        print('Inputting ' + dlc['dataset']['last_name'][dlc['data']] + ' ' + dlc['dataset']['first_name'][dlc['data']])
    else:
        print(
            'Inputting ' + dlc['dataset']['last_name'][dlc['data']] + ' ' + dlc['dataset']['second_name'][dlc['data']])

    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(dlc['dataset']['last_name'][dlc['data']])
    win32clipboard.CloseClipboard()
    driver.find_element_by_xpath(dlc['name_field_1']).clear()
    ActionChains(driver).key_down(Keys.CONTROL).send_keys('v').key_up(Keys.CONTROL).perform()

    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()

    if dlc['set_holder_marker'] == 1:
        win32clipboard.SetClipboardText(dlc['dataset']['first_name'][dlc['data']])
    else:
        win32clipboard.SetClipboardText(dlc['dataset']['second_name'][dlc['data']])

    win32clipboard.CloseClipboard()
    driver.find_element_by_xpath(dlc['name_field_2']).clear()
    ActionChains(driver).key_down(Keys.CONTROL).send_keys('v').key_up(Keys.CONTROL).perform()

    return (driver)


def fill_date(dlc, driver, setter):
    """
    Set from or to date
    """

    if setter == 'from':
        date = dlc['from_date']
        date_field = dlc['from_date_field']

    else:
        date = dlc['to_date']
        date_field = dlc['to_date_field']
    
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(date)
    win32clipboard.CloseClipboard() 
    driver.find_element_by_xpath(date_field).clear()
    time.sleep(1)
    ActionChains(driver).key_down(Keys.CONTROL).send_keys('v').key_up(Keys.CONTROL).perform()

    return (driver)


def search(dlc, driver):
    """
    Click search element
    """
    
    driver.find_element_by_xpath(dlc['search_button']).click()

    return (driver)


def id_doc_type(xpath_doc_type,driver):
    """
    Identify document type
    """

    doc_type = driver.find_element_by_xpath(xpath_doc_type).text

    return (doc_type)


def download(dlc, comb, driver):
    """
    Download image and prep for OCR
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    subprocess.Popen(config['watchdog_path'])

    print('Waiting for WatchDog to load')
    loading_precaution(3)
    print('~~~ Commencing download ~~~')

    # Initialize termination switch
    termination_switch = False

    while termination_switch == False:

        try:
            if county in config['mfc']:
                driver = shift.mfc_download(comb, driver)
            elif county in config['download_doc_counter']:
                driver = eval('shift.download_' + county + '(comb,driver)')
            else:
                driver = eval('shift.download_' + county + '(driver)')

        except:  # Explicitly except with no regards in order to account for all anomalies whether accounted for or not
            dlc['crash_flag'] = True

        else:
            pass

        finally:

            # Crash checkpoint 18
            if dlc['crash_flag'] == True:
                break
            else:
                pass

        # Holmes needs a loading precaution for its download phase
        if dlc['dataset']['county'][dlc['data']] == 'HOLMES':
            loading_precaution(15)
        else:

            os.chdir(config['download_dir'])

            time.sleep(1)

            checker = False

            # Required file downloading/downloaded actions
            if dlc['dataset']['county'][dlc['data']] == 'Duval':

                start_counter = False
                end_counter = False

                # Start tracking part file from download
                while start_counter == False:

                    start_counter = Duval_track()

                    if start_counter == True:
                        break
                    else:
                        pass

                # Stop tracking part file as download has finished
                while end_counter == False:

                    end_counter = Duval_stop_track()

                    if end_counter == True:
                        break
                    else:
                        pass

                print('~~~ Changing extension ~~~')

                source = config['download_dir']

                # Set PDF extension for downloaded file
                for root, source, files in os.walk(source):
                    for file in files:
                        if file.count('.'):
                            pass
                        else:
                            os.rename(os.path.join(root, file), os.path.join(root, "{}.pdf".format(file)))
            else:

                while checker == False:

                    if county == 'Osceola':
                        time.sleep(5)
                    else:
                        pass

                    checker = download_complete()

        print('Transferring image')
        hold_path = str(os.listdir())
        mule_hold_1 = hold_path.strip('[')
        mule_hold_2 = mule_hold_1.strip('\'')
        mule_hold_3 = mule_hold_2.strip(']')
        pdf = mule_hold_3.strip('\'')

        os.chdir(config['config_path'])

        mule = config['mule'] + pdf
        xfer = config['xfer'] + pdf
        shutil.move(mule, xfer)

        break

    print('Terminating WatchDog')
    os.system("taskkill /f /im WatchDog.exe")

    return (dlc, driver)


def download_complete():
    
    for file in os.listdir('.'):
        if fnmatch.fnmatch(file, '*.pdf'):
            print('~~~ Download Complete ~~~')
            checker = True
        else:
            checker = False
            
    return (checker)


def Duval_track():
    
    for file in os.listdir('.'):
        if fnmatch.fnmatch(file, '*.part'):
            print('~~~ File set ~~~')
            start_counter = True
            break
        else:
            start_counter = False

    return (start_counter)


def Duval_stop_track():

    end_file_match = 0

    for file in os.listdir('.'):
        end_file_match += 1

    if end_file_match == 1:
        print('~~~ File locked ~~~')
        end_counter = True
    else:
        end_counter = False

    return (end_counter)


def loading_precaution(delay):
    """
    Loading precaution
    """

    print('Commencing loading precaution')
    
    for count in range(delay, 0, -1):
        time.sleep(1)
        print(count)
        
    print('Completed loading precaution')


def initiate_ocr(dlc):
    """
    Perform OCR
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    os.chdir(config['image_dir'])

    os.system('gswin64c -sDEVICE=tiffpack -r300 -o check%02d.tiff check.pdf')

    loanamount = '${:,}'.format(int(dlc['dataset']['principal'][dlc['data']]))

    os.chdir(config['config_path'])  # Change directory to config path for possibilities

    possibilities = gen_possibilities(dlc)

    os.chdir(config['image_dir'])  # Change directory to image to continue OCR

    address_flag = False
    principal_flag = False
    confirm_doc = 0
    mortgage_doc_found = 0

    # Immediately bypass OCR if Error in address possibilities
    if 'Error' in possibilities:
        
        address_flag = False
        principal_flag = False
        confirm_doc = 2
        
    else:
        
        address_flag = False
        principal_flag = False
        confirm_doc = 0

        for file in glob.glob('*.tiff'):
            print('Performing OCR on ' + str(file))
            image = image_to_string(Image.open(str(file)))
            ocr = str(image)

            # Flag if address found
            if address_flag == False:
                if any(pos in ocr for pos in possibilities):
                    print('Found address')
                    address_flag = True
                else:
                    print('Did not find address')
            else:
                pass

            # Flag if loan amount found
            if principal_flag == False:
                
                if loanamount in ocr:
                    print('Found principal')
                    principal_flag = True
                    
                else:
                    print('Did not find principal')
            else:
                pass

            # Immediately stop if address and principal have both been found
            if address_flag == True and principal_flag == True:
                confirm_doc = 1
                break
            
            else:
                pass

    # Remove files if OCR is inconclusive and flag holder as not found
    if address_flag == False and principal_flag == False and confirm_doc == 0:
        
        mortgage_doc_found = 0
        print('Incorrect document')
        
        for file in glob.glob('*.tiff'):
            os.remove(file)
            
        for file in glob.glob('*.pdf'):
            os.remove(file)
            
    elif address_flag == True and principal_flag == False and confirm_doc == 0:
        
        mortgage_doc_found = 0
        print('Incorrect document')
        
        for file in glob.glob('*.tiff'):
            os.remove(file)
            
        for file in glob.glob('*.pdf'):
            os.remove(file)
            
    elif address_flag == False and principal_flag == True and confirm_doc == 0:
        
        mortgage_doc_found = 0
        print('Incorrect document')
        
        for file in glob.glob('*.tiff'):
            os.remove(file)
            
        for file in glob.glob('*.pdf'):
            os.remove(file)

    # Flag as mortgage found
    elif address_flag == True and principal_flag == True and confirm_doc == 1:
        
        mortgage_doc_found = 1

    # Flag error as address is not accounted for
    elif address_flag == False and principal_flag == False and confirm_doc == 2:
        
        mortgage_doc_found = 2
        for file in glob.glob('*.tiff'):
            os.remove(file)
            
        for file in glob.glob('*.pdf'):
            os.remove(file)

    os.chdir(config['config_path'])

    return (mortgage_doc_found)


def gen_possibilities(dlc):
    """
    Validate whether address will be accounted for as a search argument and if so then generate OCR terms
    """

    # Prep for address validation
    address = dlc['dataset']['address'][dlc['data']]
    up_address = address.upper()
    po_mark = re.match('PO', up_address)

    # Check if address is a PO Box and if it is then set OCR terms
    try:
        
        po = po_mark.group(0)
        
    except:
        
        po_flag = False
        
    else:
        
        po_flag = True

        # Refer to config JSON for specs and designations
        with open('../misc/config.json') as config:
            config = json.load(config)

        ocr = config['ocr']

        possibilities = ocr

    # If address is not a PO Box then check if it is a current valid address and if so then set OCR terms
    if po_flag == False:

        filter_address = re.search('^[0-9]+', address)

        try:
            
            address_digits = filter_address.group(0)

        except AttributeError:
            
            possibilities = []
            possibilities.append('Error')
            
        else:
            
            possibilities = []
            possibilities.append(address_digits)

    else:
        
        pass

    return (possibilities)


def ocr_results(dlc, mortgage_doc_found):
    """
    Set appropriate actions based on OCR results
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    os.chdir(config['image_dir'])  # Change directory to remove temp files

    # Relocate image and delete temp files
    if mortgage_doc_found == 1:
        
        print('Confirmed mortgage doc as ' + dlc['dataset']['address'][dlc['data']] + '.pdf')
        print('Completed search for ' + dlc['set_name'])
        mule = config['mule_2']
        xfer = config['xfer_2'] + dlc['dataset']['address'][dlc['data']] + '.pdf'
        shutil.move(mule, xfer)
        
        for file in glob.glob('*.tiff'):
            os.remove(file)
            
        for file in glob.glob('*.pdf'):
            os.remove(file)
            
        first_holder_found = 1

    # Flag address error and jump to final report
    elif mortgage_doc_found == 2:
        first_holder_found = 2

    # Flag holder not found and jump to final report
    elif mortgage_doc_found == 3:
        first_holder_found = 3

    # Flag doc not found and jump to final report
    elif mortgage_doc_found == 0:
        first_holder_found = 2

    os.chdir(config['config_path'])  # Change directory back to config path

    return (first_holder_found)


def overdrive(dlc, driver):
    """
    Perform actions before second run can be engaged
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Initial actions according to county
    if county in config['mfc']:

        county = county.upper()
        driver.find_element_by_xpath('/html/body/div[1]/div[1]/a/img').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[3]/div[2]/a[2]/img')))
        driver.find_element_by_xpath('/html/body/div[3]/div[2]/a[2]/img').click()

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[1]/div[2]/div[3]/form/input')))

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,
                                                                          '/html/body/div[1]/div[2]/div[3]/form/div[6]/p[2]/select')))
        select_county = Select(
            driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/form/div[6]/p[2]/select'))
        select_county.select_by_visible_text(county)

    elif county == 'Alachua' or county == 'Sarasota':

        driver.find_element_by_xpath(
            '//*[@id="Table1"]/tbody/tr[3]/td/div/table/tbody/tr/td[3]/div/a[1]').click()
        time.sleep(1)

    elif county == 'Collier':

        driver.find_element_by_xpath('//*[@id="searchbutton"]').click()
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="search_lastname"]')))

    elif county == 'SaintLucie':

        driver.find_element_by_xpath('//*[@id="toggleForm"]').click()
        time.sleep(1)

    elif county == 'Lee':

        driver.execute_script("window.history.go(-1)")
        time.sleep(1)

    elif county == 'Marion':

        driver.find_element_by_xpath(
            '/html/body/table[2]/tbody/tr/td/a[2]').click()
        time.sleep(1)

    elif county == 'Miami-dade':

        driver.find_element_by_xpath(
            '//*[@id="LinkButton1"]').click()
        time.sleep(1)

    elif county == 'Polk' or county == 'Seminole':

        driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[2]/ul[1]/li[1]/a[1]').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/' +
                                     'div[1]/form[1]/div[1]/div[3]/button[1]').click()
        time.sleep(1)

    elif county == 'Osceola':

        driver.find_element_by_xpath('/html/body/table[2]/tbody/tr/td/a[2]').click()
        time.sleep(1)
        driver.find_element_by_xpath('/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/form/table/' +
                                     'tbody/tr[1]/td/input[2]').click()
        time.sleep(1)

    elif county == 'Volusia':

        driver.find_element_by_xpath(
            '//*[@id="reset"]').click()
        time.sleep(1)

    else:
        pass

    return (driver)


def mark_doc_not_found(dlc):
    """
    Edit dataset to reflect unaccounted doc cases and reload dataframe
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Revert county back to Excel format
    if dlc['dataset']['county'][dlc['data']] == 'IndianRiver':
        county = 'Indian River'
    elif dlc['dataset']['county'][dlc['data']] == 'MiamiDade':
        county = 'Miami-dade'
    elif dlc['dataset']['county'][dlc['data']] == 'PalmBeach':
        county = 'Palm Beach'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintJohns':
        county = 'Saint Johns'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintLucie':
        county = 'Saint Lucie'
    else:
        pass

    for row in dlc['dataframe'].itertuples():
        if getattr(row, '_3') == dlc['dataset']['address'][dlc['data']] and getattr(row, 'County') == county:
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Doc Not Found'] = 1
            break
        else:
            pass

    save_dataframe(dlc)

    return (dlc)


def mark_address_error(dlc):
    """
    Edit dataset to reflect unaccounted address cases and reload dataframe
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Revert county back to Excel format
    if dlc['dataset']['county'][dlc['data']] == 'IndianRiver':
        county = 'Indian River'
    elif dlc['dataset']['county'][dlc['data']] == 'MiamiDade':
        county = 'Miami-dade'
    elif dlc['dataset']['county'][dlc['data']] == 'PalmBeach':
        county = 'Palm Beach'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintJohns':
        county = 'Saint Johns'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintLucie':
        county = 'Saint Lucie'
    else:
        pass

    for row in dlc['dataframe'].itertuples():
        if getattr(row, '_3') == dlc['dataset']['address'][dlc['data']] and getattr(row, 'County') == county:
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Address Error'] = 1
            break
        else:
            pass

    save_dataframe(dlc)

    return (dlc)


def mark_holder_not_found(dlc):
    """
    Edit dataset to reflect unaccounted holder cases and reload dataframe
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Revert county back to Excel format
    if dlc['dataset']['county'][dlc['data']] == 'IndianRiver':
        county = 'Indian River'
    elif dlc['dataset']['county'][dlc['data']] == 'MiamiDade':
        county = 'Miami-dade'
    elif dlc['dataset']['county'][dlc['data']] == 'PalmBeach':
        county = 'Palm Beach'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintJohns':
        county = 'Saint Johns'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintLucie':
        county = 'Saint Lucie'
    else:
        pass

    for row in dlc['dataframe'].itertuples():
        if getattr(row, '_3') == dlc['dataset']['address'][dlc['data']] and getattr(row, 'County') == county:
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Holder Not Found'] = 1
            break
        else:
            pass

    save_dataframe(dlc)

    return (dlc)


def mark_crash_and_cause(dlc):
    """
    Edit dataset to reflect crashed driver sessions and cause
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Revert county back to Excel format
    if dlc['dataset']['county'][dlc['data']] == 'IndianRiver':
        county = 'Indian River'
    elif dlc['dataset']['county'][dlc['data']] == 'MiamiDade':
        county = 'Miami-dade'
    elif dlc['dataset']['county'][dlc['data']] == 'PalmBeach':
        county = 'Palm Beach'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintJohns':
        county = 'Saint Johns'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintLucie':
        county = 'Saint Lucie'
    else:
        pass

    crash_cause = config['crash_cause'][str(dlc['crash_cause'])]

    for row in dlc['dataframe'].itertuples():
        if getattr(row, '_3') == dlc['dataset']['address'][dlc['data']] and getattr(row, 'County') == county:
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Driver Crashed'] = 1
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Crash Cause'] = crash_cause

            break
        else:
            pass

    save_dataframe(dlc)

    return (dlc)


def mark_found(dlc, owner):
    """
    Edit dataset to reflect found owners and reload dataframe
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Revert county back to Excel format
    if dlc['dataset']['county'][dlc['data']] == 'IndianRiver':
        county = 'Indian River'
    elif dlc['dataset']['county'][dlc['data']] == 'MiamiDade':
        county = 'Miami-dade'
    elif dlc['dataset']['county'][dlc['data']] == 'PalmBeach':
        county = 'Palm Beach'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintJohns':
        county = 'Saint Johns'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintLucie':
        county = 'Saint Lucie'
    else:
        pass

    for row in dlc['dataframe'].itertuples():
        if getattr(row, '_3') == dlc['dataset']['address'][dlc['data']] and getattr(row, 'County') == county:
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Owner'] = owner
            break
        else:
            pass

    save_dataframe(dlc)

    return (dlc)


def mark_maturity_date(dlc, maturity_date):
    """
    Edit dataset to reflect maturity date and reload dataframe
    """

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    county = dlc['dataset']['county'][dlc['data']]

    # Revert county back to Excel format
    if dlc['dataset']['county'][dlc['data']] == 'IndianRiver':
        county = 'Indian River'
    elif dlc['dataset']['county'][dlc['data']] == 'MiamiDade':
        county = 'Miami-dade'
    elif dlc['dataset']['county'][dlc['data']] == 'PalmBeach':
        county = 'Palm Beach'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintJohns':
        county = 'Saint Johns'
    elif dlc['dataset']['county'][dlc['data']] == 'SaintLucie':
        county = 'Saint Lucie'
    else:
        pass

    for row in dlc['dataframe'].itertuples():
        if getattr(row, '_3') == dlc['dataset']['address'][dlc['data']] and getattr(row, 'County') == county:
            dlc['dataframe'].loc[getattr(row, 'Index'), 'Matures'] = maturity_date
            break
        else:
            pass
        
    save_dataframe(dlc)

    return (dlc)


def save_dataframe(dlc):
    
    dlc['dataframe'].to_excel(dlc['path'])


def ocr_maturity(dlc):
    """
    OCR maturity date
    """

    print('Attempting search for maturity date')

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)
        
    ocr_maturity_prep(dlc)

    dates = []
    screen = []
    final = []

    # OCR
    for file in glob.glob('*.tiff'):
        print('Performing OCR on ' + str(file))
        image = image_to_string(Image.open(str(file)))
        ocr = str(image)
        
        # Search for dates
        for month in range(0,12):
     
            current_month = config['month_counter'][str(month)]
            
            dates = ocr_algorithm_alpha(dates, ocr, current_month)    
            dates = ocr_algorithm_bravo(dates, ocr, current_month)
            dates = ocr_algorithm_charlie(dates, ocr, current_month)
            dates = ocr_algorithm_delta(dates, ocr, current_month)
            dates = ocr_algorithm_echo(dates, ocr, current_month)

    # Clean up
    for file in glob.glob('*.tiff'):
        os.remove(file)
        
    for file in glob.glob('*.pdf'):
        os.remove(file)

    # Revert directory back to config path
    os.chdir(config['config_path'])

    # Filter for dates only
    screen = ocr_cull(dates, screen)

    # Unpack screen list and chain it all together into a single, clean list (currently a horrendeous nested mass)
    cleaned_list = list(chain(*screen))

    # Convert dates to datetime format
    final = ocr_convert(cleaned_list, final)
    
    # Date comp
    maturity_date = ocr_compare_dates(dlc, final)

    dlc = mark_maturity_date(dlc, maturity_date)

    return (dlc)


def ocr_maturity_prep(dlc):
    
    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)

    # Copy instance to OCR, rename, and run Ghostscript
    os.chdir(config['xfer_2'])

    pdf = dlc['dataset']['address'][dlc['data']] + '.pdf'

    source = config['xfer_2'] + pdf
    destination = config['ocr_dir'] + pdf

    copyfile(source, destination)

    os.chdir(config['ocr_dir'])

    rename = dlc['dataset']['address'][dlc['data']].replace(' ', '_')
    rename = rename + '.pdf'

    os.rename(pdf, rename)

    tiff = rename.replace('.pdf', '')

    ghost_script = 'gswin64c -sDEVICE=tiffpack -r300 -o ' + tiff + '%02d.tiff ' + rename

    os.system(ghost_script)


def ocr_algorithm_alpha(dates, ocr, current_month):
    """
    Algorithm for standard format
    """

    pattern = current_month + ' [0-9]+, [0-9]+'
    match = re.findall(pattern, ocr)

    # Stack "None" if current pattern is not deteced
    if len(match) == 0:
        
        dates.append('None')

    # Otherwise fiter extraction(s)
    else:

        match_counter = 0

        for matched in match:
            
            mule = match[match_counter]

            # Fetch year
            match_year = re.search('[0-9]+$', mule)
            year = match_year.group(0)
            
            # Reject as "None" if year is not four digits
            if len(year) != 4:
                dates.append('None')

            # Checkout
            else:
                dates.append([mule])

            match_counter += 1

    return (dates)
    

def ocr_algorithm_bravo(dates, ocr, current_month):
    """
    Algorithm for suffix format with comma
    """
    
    pattern = current_month + ' [0-9]+[aA-zZ]+, [0-9]+'
    match = re.findall(pattern, ocr)

    # Stack "None" if current pattern is not deteced
    if len(match) == 0:
        
        dates.append('None')
        
    # Otherwise fiter extraction(s)
    else:
        
        match_counter = 0

        for matched in match:

            mule = match[match_counter]

            # Fetch day
            match_day = re.search('[0-9]+[aA-zZ]+', mule)
            raw_day = match_day.group(0)
            processed_day = re.search('[0-9]+', raw_day)
            day = processed_day.group(0)

            # Fetch year
            match_year = re.search('[0-9]+$', mule)
            year = match_year.group(0)

            # Reject as 'None' if it is not four digits
            if len(year) != 4:
                dates.append('None')

            # Checkout
            else:
                join = [current_month + ' ' + day + ', ' + year]
                dates.append(join)

            match_counter += 1

    return (dates)


def ocr_algorithm_charlie(dates, ocr, current_month):
    """
    Algorithm for suffix format without comma
    """
    
    pattern = current_month + ' [0-9]+[aA-zZ]+ [0-9]+'
    match = re.findall(pattern, ocr)

    # Stack "None" if current pattern is not deteced
    if len(match) == 0:
        
        dates.append('None')
        
    # Otherwise fiter extraction(s)
    else:
        
        match_counter = 0

        for matched in match:

            mule = match[match_counter]

            # Fetch day
            match_day = re.search('[0-9]+[aA-zZ]+', mule)
            raw_day = match_day.group(0)
            processed_day = re.search('[0-9]+', raw_day)
            day = processed_day.group(0)

            # Fetch year
            match_year = re.search('[0-9]+$', mule)
            year = match_year.group(0)

            # Reject as 'None' if it is not four digits
            if len(year) != 4:
                dates.append('None')

            # Checkout
            else:
                join = [current_month + ' ' + day + ', ' + year]
                dates.append(join)

            match_counter += 1

    return (dates)


def ocr_algorithm_delta(dates, ocr, current_month):
    """
    Algorithm for "day of" with comma format
    """
    
    pattern = '[0-9]+[aA-zZ]+ day of ' + current_month + ', [0-9]+'
    match = re.findall(pattern, ocr)

    # Stack "None" if current pattern is not deteced
    if len(match) == 0:
        
        dates.append('None')
        
    # Otherwise fiter extraction(s)
    else:
        
        match_counter = 0

        for matched in match:
            
            mule = match[match_counter]

            # Fetch day
            match_day = re.match('[0-9]+', mule)
            day = match_day.group(0)

            # Fetch year
            match_year = re.search('[0-9]+$', mule)
            year = match_year.group(0)

            # Reject as 'None' if it is not four digits
            if len(year) != 4:
                dates.append('None')

            # Checkout
            else:
                join = [current_month + ' ' + day + ', ' + year]
                dates.append(join)

            match_counter += 1

    return (dates)


def ocr_algorithm_echo(dates, ocr, current_month):
    """
    Algorithm for "day of" without comma format
    """
    
    pattern = '[0-9]+[aA-zZ]+ day of ' + current_month + ' [0-9]+'
    match = re.findall(pattern, ocr)

    # Stack "None" if current pattern is not deteced
    if len(match) == 0:
        
        dates.append('None')
        
    # Otherwise fiter extraction(s)     
    else:
        
        match_counter = 0

        for matched in match:
            
            mule = match[match_counter]

            # Fetch day
            match_day = re.match('[0-9]+', mule)
            day = match_day.group(0)
            
            # Fetch year
            match_year = re.search('[0-9]+$', mule)
            year = match_year.group(0)

            # Reject as 'None' if it is not four digits
            if len(year) != 4:
                dates.append('None')

            # Checkout
            else:
                join = [current_month + ' ' + day + ', ' + year]
                dates.append(join)

            match_counter += 1

    return (dates)


def ocr_cull(dates, screen):
    """
    Cull out all None's and form a new list with dates only
    """
    
    clean_counter = 0

    for dates_found in dates:

        # Cull out 'None's
        if dates[clean_counter] == 'None':
            pass

        # List comprehend a new list with dates only
        else:
            result = [dates[clean_counter] for dates[clean_counter] in dates[clean_counter]]
            screen.append(result)

        clean_counter += 1

    return (screen)


def ocr_convert(cleaned_list, final):
    """
    Convert dates to datetime format
    """
    
    cleaned_list_counter = 0

    for cleaned in cleaned_list:
        
        ocr_results = cleaned_list[cleaned_list_counter]

        converted_date = datetime.datetime.strptime(ocr_results, '%B %d, %Y')

        final.append(converted_date)

        cleaned_list_counter += 1

    return(final)


def ocr_compare_dates(dlc, final):

    # Refer to config JSON for specs and designations
    with open('../misc/config.json') as config:
        config = json.load(config)
    
    len_final = len(final)

    if len_final != 0:

        # Fetch to_date month
        raw_to_date_month = re.match('[0-9]+', dlc['to_date'])
        raw_month = raw_to_date_month.group(0)
        month = config['to_date_month_convert'][str(raw_month)]

        # Fetch to_date day
        raw_day = re.search('/[0-9]+/', dlc['to_date'])
        mule_day = raw_day.group(0)
        day = str(mule_day.replace('/', ''))

        # Remove any preceding zeroes in fetched day
        check_raw_day = re.match('0', str(mule_day))

        if check_raw_day == None:
            pass
        else:
            day = str(mule_day.replace('0', ''))

        # Fetch to_date year
        raw_year = re.search('[0-9]+$', dlc['to_date'])
        year = str(raw_year.group(0))

        # To_date 
        to_date = month + ' ' + day + ', ' + year

        # Sort final list according to descending order
        final.sort(reverse=True)

        # Fetch latest date from final and convert to month day, year string format for Excel later (if applicable)
        latest_date = final[0]
        maturity = latest_date.strftime('%B %d, %Y')

        # Format dates to be comparable
        maturity_formatted = datetime.datetime.strptime(maturity, '%B %d, %Y')
        to_date_formatted = datetime.datetime.strptime(to_date, '%B %d, %Y')

        if to_date_formatted >= maturity_formatted:
            maturity_date = 'Inconclusive'
            
        else:
            maturity_date = maturity

    else:
        maturity_date = 'Inconclusive'

    return (maturity_date)


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
