# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import re
import json
import pytest
import datetime
import sys
sys.path.append(r'..\Initial_D')

import drive
import shift

from unittest.mock import MagicMock, patch

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------Tests/Test Suite------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def test_ignite(drive_fixture):
    """
    Ensure WebDriver calls site
    """

    dlc, driver = drive_fixture

    with patch('drive.webdriver', autospec=True) as mock_webdriver:
        
        driver = drive.ignite(dlc)

    assert driver.get.call_count == 1


@pytest.mark.parametrize('mortgage_doc_found, mock_callback', [
    (0, 'mark_doc_not_found'),
    (2, 'mark_address_error'),
    (3, 'mark_holder_not_found'),
    (4, 'mark_crash_and_cause'),
    (1, 'mark_found')
])
def test_drive(drive_fixture, mortgage_doc_found, mock_callback):
    """
    Ensure proper routing and calls based on case
    """

    dlc, driver = drive_fixture

    with patch('drive.engage', return_value=[dlc, mortgage_doc_found, 1]) as mock_engage, \
         patch('drive.mark_doc_not_found', return_value=dlc) as mock_mark_doc_not_found, \
         patch('drive.mark_address_error', return_value=dlc) as mock_mark_address_error, \
         patch('drive.mark_holder_not_found', return_value=dlc) as mock_mark_holder_not_found, \
         patch('drive.mark_crash_and_cause', return_value=dlc) as mock_mark_crash_and_cause, \
         patch('drive.mark_found', return_value=dlc) as mock_mark_found, \
         patch('drive.ocr_maturity', return_value=dlc) as mock_ocr_maturity:

        drive.drive(dlc, driver)

        mock_call = eval('mock_' + mock_callback)

        assert mock_engage.call_count == 1

        assert mock_call.call_count == 1

        if mortgage_doc_found == 1:
            
            assert mock_ocr_maturity.call_count == 1
            
        else:
            pass


@pytest.mark.parametrize('reroute_session', [
    ('entity'),
    ('multi_one'),
    ('multi_two'),
    ('single')
])
def test_engage(drive_fixture, reroute_session):
    """
    Ensure proper routing and calls based on session
    """

    dlc, driver = drive_fixture

    with patch('drive.infuse', return_value=driver) as mock_infuse, \
         patch('drive.auto', return_value=[dlc, 1, 1, 'dummy', driver]) as mock_auto, \
         patch('drive.overdrive', return_value=driver) as mock_overdrive:

        if reroute_session == 'entity':
            
            dlc['sole_holder_flag'] = False
            dlc['entity'] = True

        elif reroute_session == 'multi_one': 
            dlc['sole_holder_flag'] = False

        elif reroute_session == 'multi_two':
            
            dlc['sole_holder_flag'] = False
            mock_auto.return_value = [dlc, 4, 2, 'dummy', driver]

        else:
            
            pass

        drive.engage(dlc, driver)

        assert mock_infuse.call_count == 1

        if reroute_session == 'multi_two':
            
            assert mock_overdrive.call_count == 1
            assert mock_auto.call_count == 2

        else:
            assert mock_auto.call_count == 1

        assert driver.call_count == 1


@pytest.mark.parametrize('working_infuse', [
    ('Functioning as intended'),
    ('Not functioning as intended')
])
def test_infuse(drive_fixture, working_infuse):
    """
    Ensure infuse functions in both normal and error state
    """

    dlc, driver = drive_fixture

    with patch('drive.infuse') as mock_infuse, \
         patch('drive.auto', return_value=[dlc, 1, 1, 'dummy', driver]) as mock_auto:

        if working_infuse == 'Functioning as intended':
            mock_infuse.return_value = driver
            
        else:
            mock_infuse.side_effect = AttributeError

        dlc, mortgage_doc_found, owner = drive.engage(dlc, driver)

        assert mock_infuse.call_count == 1

        if working_infuse == 'Functioning as intended':
            assert mock_auto.call_count == 1

        else:

            assert dlc['crash_flag'] == True
            assert dlc['crash_cause'] == 1
            assert mortgage_doc_found == 4
            assert owner == ''


def checkbox_activated(dlc, driver):
    """
    Checkbox activated case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Brevard'
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 1

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.checkbox_' + county, return_value=[True, driver]) as mock_checkbox, \
         patch('shift.' + county + '_search_results_count', return_value=[5, driver]) as mock_search_results, \
         patch('shift.pagination_' + county, return_value=[comb, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('shift.predownload_' + county, return_value=driver) as mock_predownload, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=driver) as mock_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_checkbox.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_predownload.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def mfc_auto(dlc, driver):
    """
    MFC case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Bradford'

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.mfc_search_results_count', return_value=[5, driver]) as mock_mfc_search_results, \
         patch('shift.mfc_pagination', return_value=[comb, driver]) as mock_mfc_pagination, \
         patch('shift.mfc_doc_type', return_value=['dummy', 'dummy', driver]) as mock_mfc_doc_type, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.mfc_doc_type_match', return_value=['MORTGAGE', 'AGREEMENT AND/OR CONTRACT FOR DEED']) as mock_mfc_doc_type_match, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.mfc_revert', return_value=driver) as mock_mfc_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_mfc_search_results.call_count == 1
        assert mock_mfc_pagination.call_count == 1
        assert mock_mfc_doc_type.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_mfc_doc_type_match.call_count == 1
        assert mock_download.call_count == 1
        assert mock_mfc_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def standard(dlc, driver):
    """
    Standard case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Alachua'

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[5, driver]) as mock_search_results, \
         patch('shift.sort_' + county, return_value=driver) as mock_sort, \
         patch('shift.pagination_' + county, return_value=[comb, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('shift.predownload_' + county, return_value=driver) as mock_predownload, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=driver) as mock_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_sort.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_predownload.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def pagination_switch_activated(dlc, driver):
    """
    Pagination switch activated case
    """

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[1, driver]) as mock_search_results, \
         patch('shift.sort_' + county, return_value=driver) as mock_sort, \
         patch('shift.pagination_' + county, return_value=[comb, True, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.doc_type_' + county + '_match', return_value=['MORTGAGE', 'MULE']) as mock_doc_type_match, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=['dummy', driver]) as mock_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_sort.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_doc_type_match.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def downstream(dlc, driver):
    """
    Downstream case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Marion'

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[5, driver]) as mock_search_results, \
         patch('shift.sort_' + county, return_value=driver) as mock_sort, \
         patch('shift.pagination_' + county, return_value=[comb, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_' + county, return_value='dummy') as mock_xpath_doc_type, \
         patch('drive.id_doc_type', return_value='MTG ') as mock_id_doc_type, \
         patch('shift.doc_type_' + county + '_match', return_value=['MTG ', 'MULE']) as mock_doc_type_match, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('shift.predownload_' + county, return_value=driver) as mock_predownload, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=['dummy', driver]) as mock_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_sort.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_doc_type_match.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_predownload.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def skip_misc(dlc, driver):
    """
    Skip misc case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Collier'

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[5, driver]) as mock_search_results, \
         patch('shift.pagination_' + county, return_value=[comb, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MTGE') as mock_id_doc_type, \
         patch('shift.doc_type_' + county + '_match', return_value=['MTGE', 'MULE']) as mock_doc_type_match, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=['dummy', driver]) as mock_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_doc_type_match.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def skip_predl(dlc, driver):
    """
    Skip preDL case
    """

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[5, driver]) as mock_search_results, \
         patch('shift.sort_' + county, return_value=driver) as mock_sort, \
         patch('shift.pagination_' + county, return_value=[comb, True, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.doc_type_' + county + '_match', return_value=['MORTGAGE', 'MULE']) as mock_doc_type_match, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=['dummy', driver]) as mock_revert, \
         patch('drive.initiate_ocr', return_value=1) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=1) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_sort.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_doc_type_match.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 1
        assert dlc['crash_flag'] == False


def inconclusive_search(dlc, driver):
    """
    Inconclusive search case
    """

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[1, driver]) as mock_search_results, \
         patch('shift.sort_' + county, return_value=driver) as mock_sort, \
         patch('shift.pagination_' + county, return_value=[comb, True, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.doc_type_' + county + '_match', return_value=['MORTGAGE', 'MULE']) as mock_doc_type_match, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=['dummy', driver]) as mock_revert, \
         patch('drive.initiate_ocr', return_value=0) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=3) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_sort.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_doc_type_match.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 3
        assert dlc['crash_flag'] == False


def overdrive(dlc, driver):
    """
    Overdrive case
    """

    # Mod stub dlc to reflect case
    dlc['sole_holder_flag'] = False

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.time'), \
         patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[1, driver]) as mock_search_results, \
         patch('shift.sort_' + county, return_value=driver) as mock_sort, \
         patch('shift.pagination_' + county, return_value=[comb, True, driver]) as mock_pagination, \
         patch('shift.xpath_doc_type_and_owner_' + county, return_value=['dummy', 'dummy', driver]) as mock_xpath_doc_type_and_owner, \
         patch('drive.id_doc_type', return_value='MORTGAGE') as mock_id_doc_type, \
         patch('shift.doc_type_' + county + '_match', return_value=['MORTGAGE', 'MULE']) as mock_doc_type_match, \
         patch('shift.click_image_' + county, return_value=driver) as mock_click_image, \
         patch('drive.download', return_value=[dlc, driver]) as mock_download, \
         patch('shift.revert_' + county, return_value=['dummy', driver]) as mock_revert, \
         patch('drive.initiate_ocr', return_value=0) as mock_initiate_ocr, \
         patch('drive.ocr_results', return_value=3) as mock_ocr_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert mock_sort.call_count == 1
        assert mock_pagination.call_count == 1
        assert mock_xpath_doc_type_and_owner.call_count == 1
        assert mock_id_doc_type.call_count == 1
        assert mock_doc_type_match.call_count == 1
        assert mock_click_image.call_count == 1
        assert mock_download.call_count == 1
        assert mock_revert.call_count == 1
        assert mock_initiate_ocr.call_count == 1
        assert mock_ocr_results.call_count == 1
        assert first_holder_found == 4
        assert dlc['crash_flag'] == False


def no_records(dlc, driver):
    """
    No records case
    """

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.search_holder', return_value=[dlc, driver]) as mock_search_holder, \
         patch('shift.' + county + '_search_results_count', return_value=[0, driver]) as mock_search_results:
        
        dlc, first_holder_found, mortgage_doc_found, owner, driver = drive.auto(dlc, driver)

        assert mock_search_holder.call_count == 1
        assert mock_search_results.call_count == 1
        assert first_holder_found == 0
        assert mortgage_doc_found == 3
        assert owner == ''
        assert dlc['crash_flag'] == False


@pytest.mark.parametrize('sitrep', [
    ('Checkbox Activated'),
    ('MFC'),
    ('Standard'),
    ('Pagination Switch Activated'),
    ('Downstream'),
    ('Skip Misc'),
    ('Skip PreDL'),
    ('Inconclusive Search'),
    ('Overdrive'),
    ('No Records')
])
def test_auto(drive_fixture, sitrep):
    """
    Ensure proper routing and calls based on case search/Integration test for shift
    """

    dlc, driver = drive_fixture

    if sitrep == 'Checkbox Activated':
        checkbox_activated(dlc, driver)

    elif sitrep == 'MFC':
        mfc_auto(dlc, driver)

    elif sitrep == 'Standard':
        standard(dlc, driver)

    elif sitrep == 'Pagination Switch Activated':
        pagination_switch_activated(dlc, driver)

    elif sitrep == 'Downstream':
        downstream(dlc, driver)

    elif sitrep == 'Skip Misc':
        skip_misc(dlc, driver)

    elif sitrep == 'Skip PreDL':
        skip_predl(dlc, driver)

    elif sitrep == 'Inconclusive Search':
        inconclusive_search(dlc, driver)

    elif sitrep == 'Overdrive':
        overdrive(dlc, driver)

    elif sitrep == 'No Records':
        no_records(dlc, driver)


def mfc_search_holder(dlc, driver, expected_set_name):
    """
    MFC search holder
    """

    # Mod stub dlc to reflect case
    dlc['set_holder_marker'] = 0
    dlc['dataset']['county'][dlc['data']] = 'Bradford'
    dlc['dataset']['holder'][dlc['data']] = 'Dummy LLC'

    with patch('shift.mfc_entity', return_value=driver) as mock_mfc_entity, \
         patch('drive.fill_name', return_value=[dlc, driver]) as mock_fill_name, \
         patch('shift.mfc_dates', return_value=driver) as mock_mfc_dates, \
         patch('drive.search', return_value=driver) as mock_search, \
         patch('drive.time'):
        
        dlc, driver = drive.search_holder(dlc, driver)

        assert mock_mfc_entity.call_count == 1
        assert dlc['set_name'] == expected_set_name
        assert mock_fill_name.call_count == 1
        assert mock_mfc_dates.call_count == 1
        assert mock_search.call_count == 1
        assert dlc['crash_flag'] == False


def search_holder(dlc, driver, expected_set_name):
    """
    Search holder based on preset marker and flags
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'IndianRiver'

    # Mod stub dlc to reflect the appropriate preset marker and flags
    try:
        
        mule = re.search('RANDALL', expected_set_name)
        mule_two = mule.group(0)

    except AttributeError:
        
        dlc['set_holder_marker'] = 2
        dlc['dataset']['second_name'][dlc['data']] = 'JESSICA'
        dlc['dataset']['second_mid'][dlc['data']] = 'P'

    else:
        dlc['dataset']['holder'][dlc['data']] = 'RANDALL B BELL'

    if expected_set_name == 'BELL RANDALL B' or expected_set_name == 'BELL JESSICA P':
        dlc['mid_initial_flag'] = True

    elif expected_set_name == 'BELL, RANDALL' or expected_set_name == 'BELL, JESSICA':
        dlc['comma_flag'] = True

    elif expected_set_name == 'BELL, RANDALL B' or expected_set_name == 'BELL, JESSICA P':
        
        dlc['mid_initial_flag'] = True
        dlc['comma_flag'] = True

    elif expected_set_name == 'BELL RANDALL' or expected_set_name == 'BELL JESSICA':
        pass

    with patch('drive.fill_name', return_value=[dlc, driver]) as mock_fill_name, \
         patch('drive.fill_date', return_value=driver) as mock_fill_date, \
         patch('drive.search', return_value=driver) as mock_search, \
         patch('drive.time'):

        dlc, driver = drive.search_holder(dlc, driver)

        assert mock_fill_name.call_count == 1
        assert dlc['set_name'] == expected_set_name
        assert mock_fill_date.call_count == 2
        assert mock_search.call_count == 1
        assert dlc['crash_flag'] == False


@pytest.mark.parametrize('sitrep,expected_set_name', [
    ('MFC', 'Dummy LLC'),
    ('First Holder - Mid True - Comma False', 'BELL RANDALL B'),
    ('First Holder - Mid False - Comma True', 'BELL, RANDALL'),
    ('First Holder - Mid True - Comma True', 'BELL, RANDALL B'),
    ('First Holder - Mid False - Comma False', 'BELL RANDALL'),
    ('Second Holder - Mid True - Comma False', 'BELL JESSICA P'),
    ('Second Holder - Mid False - Comma True', 'BELL, JESSICA'),
    ('Second Holder - Mid True - Comma True', 'BELL, JESSICA P'),
    ('Second Holder - Mid False - Comma False', 'BELL JESSICA')
])
def test_search_holder(drive_fixture, sitrep, expected_set_name):
    """
    Ensure proper routing and call of search holder
    """

    dlc, driver = drive_fixture

    if sitrep == 'MFC':
        mfc_search_holder(dlc, driver, expected_set_name)

    else:
        search_holder(dlc, driver, expected_set_name)


@pytest.mark.parametrize('sitrep,expected_num_name_fields', [
    ('MFC - Entity', 1),
    ('MFC - Standard', 2),
    ('Collier Solo', 1),
    ('Collier Dual', 2),
    ('Solo', 1)
])
def test_fill_name(drive_fixture, sitrep, expected_num_name_fields):
    """
    Ensure proper routing and call of the appropriate filling of name(s)
    """

    dlc, driver = drive_fixture

    # Mod stub dlc to reflect case
    if sitrep == 'MFC - Entity' or sitrep == 'MFC - Standard':
        dlc['dataset']['county'][dlc['data']] = 'Bradford'

        if sitrep == 'MFC - Entity':
            dlc['set_holder_marker'] = 0
        else:
            dlc['set_holder_marker'] = 1

    elif sitrep == 'Collier Solo':
        
        dlc['dataset']['county'][dlc['data']] = 'Collier'
        dlc['set_holder_marker'] = 0

    elif sitrep == 'Collier Dual':
        
        dlc['dataset']['county'][dlc['data']] = 'Collier'
        dlc['set_holder_marker'] = 1

    elif sitrep == 'Solo':
        dlc['dataset']['county'][dlc['data']] = 'Alachua'

    with patch('drive.one_name_field', return_value=driver) as mock_one_name_field, \
         patch('drive.two_name_fields', return_value=driver) as mock_two_name_fields:

        dlc, driver = drive.fill_name(dlc, driver)

        assert dlc['num_name_fields'] == expected_num_name_fields


def test_one_name_field(drive_fixture):
    """
    Ensure one name field is filled
    """

    dlc, driver = drive_fixture

    with patch('drive.ActionChains') as mock_ActionChains, \
         patch('drive.win32clipboard.OpenClipboard') as mock_OpenClipboard, \
         patch('drive.win32clipboard.EmptyClipboard') as mock_EmptyClipboard, \
         patch('drive.win32clipboard.SetClipboardText') as mock_SetClipboardText, \
         patch('drive.win32clipboard.CloseClipboard') as mock_CloseClipboard:
        
        driver = drive.one_name_field(dlc, driver)

    assert mock_OpenClipboard.call_count == 1
    assert mock_EmptyClipboard.call_count == 1
    assert mock_SetClipboardText.call_count == 1
    assert mock_CloseClipboard.call_count == 1
    assert driver.call_count == 1
    assert mock_ActionChains.call_count == 1


@pytest.mark.parametrize('set_holder_marker', [
    ('First Holder'),
    ('Second Holder')
])
def test_two_name_fields(drive_fixture, set_holder_marker):
    """
    Ensure two name fields are filled
    """

    dlc, driver = drive_fixture

    if set_holder_marker == 'First Holder':
        pass

    else:
        dlc['set_holder_marker'] = 2
        dlc['dataset']['second_name'][dlc['data']] = 'JESSICA'

    with patch('drive.ActionChains') as mock_ActionChains, \
         patch('drive.win32clipboard.OpenClipboard') as mock_OpenClipboard, \
         patch('drive.win32clipboard.EmptyClipboard') as mock_EmptyClipboard, \
         patch('drive.win32clipboard.SetClipboardText') as mock_SetClipboardText, \
         patch('drive.win32clipboard.CloseClipboard') as mock_CloseClipboard:

        driver = drive.two_name_fields(dlc, driver)

    assert mock_OpenClipboard.call_count == 2
    assert mock_EmptyClipboard.call_count == 2
    assert mock_SetClipboardText.call_count == 2
    assert mock_CloseClipboard.call_count == 2
    assert driver.call_count == 2
    assert mock_ActionChains.call_count == 2


@pytest.mark.parametrize('set_setter', [
    ('From Date'),
    ('To Date')
])
def test_fill_date(drive_fixture, set_setter):
    """
    Ensure date field gets filled
    """

    dlc, driver = drive_fixture

    if set_setter == 'From Date':
        setter = 'from'

    else:
        setter = 'to'

    with patch('drive.ActionChains') as mock_ActionChains, \
         patch('drive.win32clipboard.OpenClipboard') as mock_OpenClipboard, \
         patch('drive.win32clipboard.EmptyClipboard') as mock_EmptyClipboard, \
         patch('drive.win32clipboard.SetClipboardText') as mock_SetClipboardText, \
         patch('drive.win32clipboard.CloseClipboard') as mock_CloseClipboard, \
         patch('drive.time'):

        driver = drive.fill_date(dlc, driver, setter)

    assert mock_OpenClipboard.call_count == 1
    assert mock_EmptyClipboard.call_count == 1
    assert mock_SetClipboardText.call_count == 1
    assert mock_CloseClipboard.call_count == 1
    assert driver.call_count == 1
    assert mock_ActionChains.call_count == 1


def test_search(drive_fixture):
    """
    Ensure search element is clicked through driver
    """

    dlc, driver = drive_fixture

    driver = drive.search(dlc, driver)

    assert driver.call_count == 1


def test_id_doc_type(drive_fixture):
    """
    Ensure doc_type is retrieved through driver
    """

    dlc, driver = drive_fixture

    xpath_doc_type = 'dummy'

    doc_type = drive.id_doc_type(xpath_doc_type, driver)

    assert driver.call_count == 1


def download_standard(dlc, driver):
    """
    Standard download case
    """

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.subprocess.Popen') as mock_subprocess, \
         patch('drive.loading_precaution') as mock_loading_precaution, \
         patch('shift.download_' + county, return_value=driver) as mock_shift_download, \
         patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.time.sleep') as mock_time_sleep, \
         patch('drive.download_complete', return_value=True) as mock_download_complete, \
         patch('drive.os.listdir') as mock_os_listdir, \
         patch('drive.shutil.move') as mock_os_shutil_move, \
         patch('drive.os.system') as mock_os_system:
        
        dlc, driver = drive.download(dlc, comb, driver)

        assert mock_subprocess.call_count == 1
        assert mock_loading_precaution.call_count == 1
        assert mock_shift_download.call_count == 1
        assert mock_os_chdir.call_count == 2
        assert mock_time_sleep.call_count == 1
        assert mock_download_complete.call_count == 1
        assert mock_os_listdir.call_count == 1
        assert mock_os_shutil_move.call_count == 1
        assert mock_os_system.call_count == 1


def download_mfc(dlc, driver):
    """
    MFC download case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Bradford'

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.subprocess.Popen') as mock_subprocess, \
         patch('drive.loading_precaution') as mock_loading_precaution, \
         patch('shift.mfc_download', return_value=driver) as mock_shift_download, \
         patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.time.sleep') as mock_time_sleep, \
         patch('drive.download_complete', return_value=True) as mock_download_complete, \
         patch('drive.os.listdir') as mock_os_listdir, \
         patch('drive.shutil.move') as mock_os_shutil_move, \
         patch('drive.os.system') as mock_os_system:
        
        dlc, driver = drive.download(dlc, comb, driver)

        assert mock_subprocess.call_count == 1
        assert mock_loading_precaution.call_count == 1
        assert mock_shift_download.call_count == 1
        assert mock_os_chdir.call_count == 2
        assert mock_time_sleep.call_count == 1
        assert mock_download_complete.call_count == 1
        assert mock_os_listdir.call_count == 1
        assert mock_os_shutil_move.call_count == 1
        assert mock_os_system.call_count == 1


def download_collier(dlc, driver):
    """
    Collier download case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Collier'

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.subprocess.Popen') as mock_subprocess, \
         patch('drive.loading_precaution') as mock_loading_precaution, \
         patch('shift.download_' + county, return_value=driver) as mock_shift_download, \
         patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.time.sleep') as mock_time_sleep, \
         patch('drive.download_complete', return_value=True) as mock_download_complete, \
         patch('drive.os.listdir') as mock_os_listdir, \
         patch('drive.shutil.move') as mock_os_shutil_move, \
         patch('drive.os.system') as mock_os_system:
        
        dlc, driver = drive.download(dlc, comb, driver)

        assert mock_subprocess.call_count == 1
        assert mock_loading_precaution.call_count == 1
        assert mock_shift_download.call_count == 1
        assert mock_os_chdir.call_count == 2
        assert mock_time_sleep.call_count == 1
        assert mock_download_complete.call_count == 1
        assert mock_os_listdir.call_count == 1
        assert mock_os_shutil_move.call_count == 1
        assert mock_os_system.call_count == 1


def download_duval(dlc, driver):
    """
    Duval download case
    """

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = 'Duval'

    # Replicate shift call
    county = dlc['dataset']['county'][dlc['data']]

    # Stub comb
    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 0

    with patch('drive.subprocess.Popen') as mock_subprocess, \
         patch('drive.loading_precaution') as mock_loading_precaution, \
         patch('shift.download_' + county, return_value=driver) as mock_shift_download, \
         patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.time.sleep') as mock_time_sleep, \
         patch('drive.Duval_track', return_value=True) as mock_Duval_track, \
         patch('drive.Duval_stop_track', return_value=True) as mock_Duval_stop_track, \
         patch('drive.os.listdir') as mock_os_listdir, \
         patch('drive.shutil.move') as mock_os_shutil_move, \
         patch('drive.os.system') as mock_os_system:
        
        dlc, driver = drive.download(dlc, comb, driver)

        assert mock_subprocess.call_count == 1
        assert mock_loading_precaution.call_count == 1
        assert mock_shift_download.call_count == 1
        assert mock_os_chdir.call_count == 2
        assert mock_time_sleep.call_count == 1
        assert mock_Duval_track.call_count == 1
        assert mock_Duval_stop_track.call_count == 1
        assert mock_os_listdir.call_count == 1
        assert mock_os_shutil_move.call_count == 1
        assert mock_os_system.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Standard'),
    ('MFC'),
    ('Collier'),
    ('Duval')
])
def test_download(drive_fixture, sitrep):
    """
    Ensure proper downloading route and calls based on case
    """

    dlc, driver = drive_fixture

    if sitrep == 'Standard':
        download_standard(dlc, driver)

    elif sitrep == 'MFC':
        download_mfc(dlc, driver)

    elif sitrep == 'Collier':
        download_collier(dlc, driver)

    elif sitrep == 'Duval':
        download_duval(dlc, driver)


def test_download_complete():
    """
    Ensure standard download finds completed download
    """

    with patch('drive.os.listdir', return_value=r'C:\test.pdf') as mock_os_listdir, \
         patch('drive.fnmatch.fnmatch', return_value=True) as mock_fnmatch_fnmatch:
        
        start_counter = drive.download_complete()

    assert start_counter == True


def test_Duval_track():
    """
    Ensure Duval download starts tracking
    """

    with patch('drive.os.listdir', return_value=r'C:\test.part') as mock_os_listdir, \
         patch('drive.fnmatch.fnmatch', return_value=True) as mock_fnmatch_fnmatch:
        
        start_counter = drive.Duval_track()

    assert start_counter == True


def test_Duval_stop_track():
    """
    Ensure Duval download stops tracking
    """

    with patch('drive.os.listdir', return_value=os.listdir('../docs')) as mock_os_listdir:
        
        end_counter = drive.Duval_stop_track()

    assert end_counter == True


@pytest.mark.parametrize('count', [
    (3),
    (5)
])
def test_loading_precaution(count):
    """
    Ensure loading precaution delays
    """

    with patch('drive.time.sleep') as mock_time_sleep:
        
        drive.loading_precaution(count)

    assert mock_time_sleep.call_count == count


def initiate_ocr_standard(dlc, ocr, expected_mortgage_doc_found):
    """
    Standard OCR cases
    """

    with patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.os.system') as mock_os_system, \
         patch('drive.gen_possibilities', return_value=['1234']) as mock_gen_possibilities, \
         patch('drive.glob.glob', return_value=os.listdir('../test mules/')) as mock_glob_glob, \
         patch('drive.Image.open') as mock_Image_open, \
         patch('drive.image_to_string', return_value=ocr) as mock_image_to_string, \
         patch('drive.os.remove') as mock_os_remove:

        mortgage_doc_found = drive.initiate_ocr(dlc)

        assert mock_os_chdir.call_count == 4
        assert mock_os_system.call_count == 1

        if expected_mortgage_doc_found == 1:
            assert mock_glob_glob.call_count == 1
            
        else:
            assert mock_glob_glob.call_count == 3

        assert mock_Image_open.call_count == 1
        assert mock_image_to_string.call_count == 1

        if expected_mortgage_doc_found == 1:
            assert mock_os_remove.call_count == 0

        else:
            assert mock_os_remove.call_count == 2

        assert mortgage_doc_found == expected_mortgage_doc_found


def initiate_ocr_error(dlc, ocr, expected_mortgage_doc_found):
    """
    Error OCR case
    """

    with patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.os.system') as mock_os_system, \
         patch('drive.gen_possibilities', return_value=['Error']) as mock_gen_possibilities, \
         patch('drive.glob.glob', return_value=os.listdir('../test mules/')) as mock_glob_glob, \
         patch('drive.os.remove') as mock_os_remove:
        
        mortgage_doc_found = drive.initiate_ocr(dlc)

        assert mortgage_doc_found == expected_mortgage_doc_found


@pytest.mark.parametrize('ocr,expected_mortgage_doc_found', [
    ('Dual', 1),
    ('Address', 0),
    ('Principal', 0),
    ('Address Error', 2)
])
def test_initiate_ocr(drive_fixture, ocr, expected_mortgage_doc_found):
    """
    Ensure proper OCR route, calls, and outcomes
    """

    dlc, driver = drive_fixture

    if ocr == 'Dual':
        
        ocr = '1234 $1,000,000'
        initiate_ocr_standard(dlc, ocr, expected_mortgage_doc_found)

    elif ocr == 'Address':
        
        ocr = '1234'
        initiate_ocr_standard(dlc, ocr, expected_mortgage_doc_found)

    elif ocr == 'Principal':
        
        ocr = '$1,000,000'
        initiate_ocr_standard(dlc, ocr, expected_mortgage_doc_found)

    elif ocr == 'Address Error':
        initiate_ocr_error(dlc, ocr, expected_mortgage_doc_found)


@pytest.mark.parametrize('sitrep,expected_possibilities', [
    ('Standard', ['700']),
    ('PO Box', 'dummy'),
    ('Address Error', ['Error'])
])
def test_gen_possibilities(drive_fixture, sitrep, expected_possibilities):
    """
    Ensure generated possibilities are returned based on case
    """

    dlc, driver = drive_fixture

    if sitrep == 'Standard':
        pass

    elif sitrep == 'PO Box':
        
        dlc['dataset']['address'][dlc['data']] = 'PO BOX 1234'

        # Refer to config JSON for determination
        with open('../misc/config.json') as config:
            config = json.load(config)

        expected_possibilities = config['ocr']

    elif sitrep == 'Address Error':
        dlc['dataset']['address'][dlc['data']] = 'dummy'

    possibilities = drive.gen_possibilities(dlc)

    assert possibilities == expected_possibilities


@pytest.mark.parametrize('mortgage_doc_found,expected_first_holder_found', [
    (1, 1),
    (2, 2),
    (3, 3),
    (0, 2)
])
def test_ocr_results(drive_fixture, mortgage_doc_found, expected_first_holder_found):
    """
    Ensure proper routing calls for last phase of auto based on ocr outcomes
    """

    dlc, driver = drive_fixture

    if mortgage_doc_found == 1:

        with patch('drive.os.chdir') as mock_os_chdir, \
             patch('drive.shutil.move') as mock_shutil_move, \
             patch('drive.glob.glob', return_value=os.listdir('../test mules/')) as mock_glob_glob, \
             patch('drive.os.remove') as mock_os_remove:

            first_holder_found = drive.ocr_results(dlc, mortgage_doc_found)

            assert mock_os_chdir.call_count == 2
            assert mock_shutil_move.call_count == 1
            assert mock_glob_glob.call_count == 2
            assert mock_os_remove.call_count == 2

    else:

        with patch('drive.os.chdir') as mock_os_chdir:

            first_holder_found = drive.ocr_results(dlc, mortgage_doc_found)

            assert mock_os_chdir.call_count == 2

    assert first_holder_found == expected_first_holder_found


@pytest.mark.parametrize('county', [
    ('MFC'),
    ('Alachua'),
    ('Sarasota'),
    ('Collier'),
    ('SaintLucie'),
    ('Lee'),
    ('Marion'),
    ('Miami-dade'),
    ('Polk'),
    ('Seminole'),
    ('Osceola'),
    ('Volusia')
])
def test_overdrive(drive_fixture, county):
    """
    Ensure respective overdrive actions are executed
    """

    dlc, driver = drive_fixture

    singularity = ['Alachua', 'Sarasota', 'SaintLucie', 'Lee', 'Marion', 'Miami-dade', 'Volusia']

    quad = ['Polk', 'Seminole', 'Osceola']

    # Mod stub dlc to reflect case
    dlc['dataset']['county'][dlc['data']] = county

    if county == 'MFC':

        # Mod stub dlc to reflect case
        dlc['dataset']['county'][dlc['data']] = 'Bradford'

        with patch('drive.WebDriverWait') as mock_WebDriverWait, \
             patch('drive.Select') as mock_Select:

            driver = drive.overdrive(dlc, driver)

            assert mock_WebDriverWait.call_count == 3
            assert mock_Select.call_count == 1
            assert driver.call_count == 3

    elif county in singularity:

        with patch('drive.time.sleep') as mock_time_sleep:

            driver = drive.overdrive(dlc, driver)

            assert driver.call_count == 1
            assert mock_time_sleep.call_count == 1

    elif county in quad:

        with patch('drive.time.sleep') as mock_time_sleep:

            driver = drive.overdrive(dlc, driver)

            assert driver.call_count == 2
            assert mock_time_sleep.call_count == 2

    elif county == 'Collier':

        with patch('drive.WebDriverWait') as mock_WebDriverWait:

            driver = drive.overdrive(dlc, driver)

            assert driver.call_count == 1
            assert mock_WebDriverWait.call_count == 1


@pytest.mark.parametrize('index,expected_doc_not_found', [
    (0, 1)
])
def test_mark_doc_not_found(drive_fixture, index, expected_doc_not_found):
    """
    Ensure doc not found cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        dlc = drive.mark_doc_not_found(dlc)

        mock_save_dataframe.assert_called_with(dlc)

        assert dlc['dataframe'].loc[index, 'Doc Not Found'] == expected_doc_not_found


@pytest.mark.parametrize('index,expected_address_error', [
    (0, 1)
])
def test_mark_address_error(drive_fixture, index, expected_address_error):
    """
    Ensure address error cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        dlc = drive.mark_address_error(dlc)

        mock_save_dataframe.assert_called_with(dlc)

        assert dlc['dataframe'].loc[index, 'Address Error'] == expected_address_error


@pytest.mark.parametrize('index,expected_holder_not_found', [
    (0, 1)
])
def test_mark_holder_not_found(drive_fixture, index, expected_holder_not_found):
    """
    Ensure holder not found cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        dlc = drive.mark_holder_not_found(dlc)

        mock_save_dataframe.assert_called_with(dlc)

        assert dlc['dataframe'].loc[index, 'Holder Not Found'] == expected_holder_not_found


@pytest.mark.parametrize('index,expected_driver_crash,expected_crash_cause', [
    (0, 1, 'Infuse')
])
def test_mark_crash_and_cause(drive_fixture, index, expected_driver_crash, expected_crash_cause):
    """
    Ensure crashed driver session and cause cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        dlc = drive.mark_crash_and_cause(dlc)

        mock_save_dataframe.assert_called_with(dlc)

        assert dlc['dataframe'].loc[index, 'Driver Crashed'] == expected_driver_crash
        assert dlc['dataframe'].loc[index, 'Crash Cause'] == expected_crash_cause


@pytest.mark.parametrize('index,owner', [
    (0, 'dummy')
])
def test_mark_found(drive_fixture, index, owner):
    """
    Ensure found cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        dlc = drive.mark_found(dlc, owner)

        mock_save_dataframe.assert_called_with(dlc)

        assert dlc['dataframe'].loc[index, 'Owner'] == owner


@pytest.mark.parametrize('index,maturity_date', [
    (0, 'dummy')
])
def test_mark_maturity_date(drive_fixture, index, maturity_date):
    """
    Ensure found cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        dlc = drive.mark_maturity_date(dlc, maturity_date)

        mock_save_dataframe.assert_called_with(dlc)

        assert dlc['dataframe'].loc[index, 'Matures'] == maturity_date

 
def test_save_dataframe(drive_fixture):
    """
    Validate save called
    """

    dlc, driver = drive_fixture

    with patch('drive.save_dataframe') as mock_save_dataframe:
        
        drive.save_dataframe(dlc)

    mock_save_dataframe.assert_called_with(dlc)


def test_ocr_maturity(drive_fixture):
    """
    Ensure proper amount of calls for ocr maturity
    """

    dlc, driver = drive_fixture

    with patch('drive.ocr_maturity_prep') as mock_ocr_maturity_prep, \
         patch('drive.glob.glob', return_value = os.listdir('../test mules/')) as mock_glob_glob, \
         patch('drive.Image.open') as mock_Image_open, \
         patch('drive.image_to_string') as mock_image_to_string, \
         patch('drive.ocr_algorithm_alpha') as mock_ocr_algorithm_alpha, \
         patch('drive.ocr_algorithm_bravo') as mock_ocr_algorithm_bravo, \
         patch('drive.ocr_algorithm_charlie') as mock_ocr_algorithm_charlie, \
         patch('drive.ocr_algorithm_delta') as mock_ocr_algorithm_delta, \
         patch('drive.os.remove') as mock_os_remove, \
         patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.ocr_cull') as mock_ocr_cull, \
         patch('drive.ocr_convert') as mock_ocr_cull, \
         patch('drive.ocr_compare_dates') as mock_ocr_compare_dates, \
         patch('drive.mark_maturity_date') as mock_mark_maturity_date:

        dlc = drive.ocr_maturity(dlc)

        assert mock_ocr_maturity_prep.call_count == 1
        assert mock_glob_glob.call_count == 3
        assert mock_Image_open.call_count == 1
        assert mock_image_to_string.call_count == 1
        assert mock_image_to_string.call_count == 1
        assert mock_ocr_algorithm_alpha.call_count == 12
        assert mock_ocr_algorithm_bravo.call_count == 12
        assert mock_ocr_algorithm_charlie.call_count == 12
        assert mock_ocr_algorithm_delta.call_count == 12
        assert mock_os_remove.call_count == 2
        assert mock_os_chdir.call_count == 1
        assert mock_ocr_cull.call_count == 1
        assert mock_ocr_compare_dates.call_count == 1
        assert mock_mark_maturity_date.call_count == 1


def test_ocr_maturity_prep(drive_fixture):
    """
    Ensure proper amount of calls for ocr maturity prep
    """

    dlc, driver = drive_fixture

    with patch('drive.os.chdir') as mock_os_chdir, \
         patch('drive.copyfile') as mock_copyfile, \
         patch('drive.os.rename') as mock_os_rename, \
         patch('drive.os.system') as mock_os_system:

        drive.ocr_maturity_prep(dlc)

        assert mock_os_chdir.call_count == 2
        assert mock_copyfile.call_count == 1
        assert mock_os_rename.call_count == 1
        assert mock_os_system.call_count == 1

        
@pytest.mark.parametrize('current_month,ocr,expected_dates', [
    ('May', 'fake text May 1, 2021 fake text', 'May 1, 2021'),
    ('June', 'fake text May 1, 2021 fake text', 'None')
])
def test_ocr_algorithm_alpha(current_month, ocr, expected_dates):
    """
    Validate algorithm for standard format
    """

    dates = []
    dates = drive.ocr_algorithm_alpha(dates, ocr, current_month)

    if current_month == 'May':
        assert dates == [[expected_dates]]

    else:
        assert dates == [expected_dates]


@pytest.mark.parametrize('current_month,ocr,expected_dates', [
    ('May', 'fake text May 1st, 2021 fake text', 'May 1, 2021'),
    ('June', 'fake text May 1st, 2021 fake text', 'None')
])
def test_ocr_algorithm_bravo(current_month, ocr, expected_dates):
    """
    Validate algorithm for suffix format with comma
    """

    dates = []
    dates = drive.ocr_algorithm_bravo(dates, ocr, current_month)

    if current_month == 'May':
        assert dates == [[expected_dates]]

    else:
        assert dates == [expected_dates]

@pytest.mark.parametrize('current_month,ocr,expected_dates', [
    ('May', 'fake text May 1st 2021 fake text', 'May 1, 2021'),
    ('June', 'fake text May 1st 2021 fake text', 'None')
])
def test_ocr_algorithm_charlie(current_month, ocr, expected_dates):
    """
    Validate algorithm for suffix format without comma
    """

    dates = []
    dates = drive.ocr_algorithm_charlie(dates, ocr, current_month)

    if current_month == 'May':
        assert dates == [[expected_dates]]

    else:
        assert dates == [expected_dates]


@pytest.mark.parametrize('current_month,ocr,expected_dates', [
    ('May', 'fake text 1st day of May, 2021 fake text', 'May 1, 2021'),
    ('June', 'fake text 1st day of May, 2021 fake text', 'None')
])       
def test_ocr_algorithm_delta(current_month, ocr, expected_dates):
    """
    Validate algorithm for "day of" format with comma
    """

    dates = []
    dates = drive.ocr_algorithm_delta(dates, ocr, current_month)

    if current_month == 'May':
        assert dates == [[expected_dates]]

    else:
        assert dates == [expected_dates]


@pytest.mark.parametrize('current_month,ocr,expected_dates', [
    ('May', 'fake text 1st day of May 2021 fake text', 'May 1, 2021'),
    ('June', 'fake text 1st day of May 2021 fake text', 'None')
])       
def test_ocr_algorithm_echo(current_month, ocr, expected_dates):
    """
    Validate algorithm for "day of" format without comma
    """

    dates = []
    dates = drive.ocr_algorithm_echo(dates, ocr, current_month)

    if current_month == 'May':
        assert dates == [[expected_dates]]

    else:
        assert dates == [expected_dates]


def test_ocr_cull():
    """
    Ensure lists are culled of None's
    """

    dates = ['None',['May 1, 2021']]
    screen = []

    screen = drive.ocr_cull(dates, screen)

    assert screen == [['May 1, 2021']]


@pytest.mark.parametrize('sitrep,final_in,expected_maturity_date', [
    ('Standard', 'May 1, 2021', 'May 01, 2021'),
    ('Unspecified', 'May 1, 2000', 'Inconclusive'),
    ('Null', 'dummy', 'Inconclusive')
])       
def test_ocr_compare_dates(drive_fixture, sitrep, final_in, expected_maturity_date):
    """
    Ensure comps check out appropriately
    """

    dlc, driver = drive_fixture

    if sitrep == 'Standard' or sitrep == 'Unspecified':
        final = [datetime.datetime.strptime(final_in, '%B %d, %Y')]

    else:
        final = []

    maturity_date = drive.ocr_compare_dates(dlc, final)

    assert maturity_date == expected_maturity_date


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':  
    pass


