# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import pytest
import win32gui
import sys
sys.path.append(r'..\Initial_D')

import shift

from unittest.mock import patch, MagicMock

from selenium.common.exceptions import TimeoutException, NoSuchElementException

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------Tests/Test Suite------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((MyFloridaCounty)))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_mfc_entity(shift_fixture):
    """
    Ensure MFC entity activation calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        shift.mfc_entity(driver)

    assert mock_time_sleep.call_count == 1
    assert driver.call_count == 1


def test_mfc_dates(shift_fixture):
    """
    Ensure MFC input dates calls
    """

    driver, comb = shift_fixture

    # Stub dlc for this scenario only
    dlc = {
             'from_date':'06/30/2017',
             'to_date':'12/31/2017'
          }

    with patch('shift.Select') as mock_Select, \
         patch('shift.win32clipboard.OpenClipboard') as mock_OpenClipBoard, \
         patch('shift.win32clipboard.EmptyClipboard') as mock_EmptyClipBoard, \
         patch('shift.win32clipboard.SetClipboardText') as mock_SetClipboardText, \
         patch('shift.win32clipboard.CloseClipboard') as mock_CloseClipboard, \
         patch('shift.ActionChains') as mock_ActionChains:

        driver = shift.mfc_dates(dlc, driver)

    assert mock_Select.call_count == 4
    assert mock_OpenClipBoard.call_count == 2
    assert mock_EmptyClipBoard.call_count == 2
    assert mock_SetClipboardText.call_count == 2
    assert mock_CloseClipboard.call_count == 2
    assert mock_ActionChains.call_count == 2


@pytest.mark.parametrize('sitrep', [
    ('Found search results'),
    ('No records found'),
    ('NoSuchElementException')
])
def test_mfc_search_results_count(shift_fixture,sitrep):
    """
    Ensure MFC search results are retrieved properly based on case
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.re.match') as mock_re:

        # Set side effects according to case
        if sitrep == 'No records found':
            mock_WebDriverWait.side_effect = TimeoutException

        elif sitrep == 'NoSuchElementException':
            mock_WebDriverWait.side_effect = TimeoutException
            driver.side_effect = NoSuchElementException

        else:
            pass

        # Execute function with proper unpacking vars according to case
        if sitrep == 'NoSuchElementException':
            crash = shift.mfc_search_results_count(driver)

        else:
            search_results, driver = shift.mfc_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1

    # Additional asserts according to case
    if sitrep == 'Found search results':
        assert mock_re.call_count == 1
        assert search_results == 1

    elif sitrep == 'No records found':
        assert search_results == 0

    elif sitrep == 'NoSuchElementException':
        assert crash == NoSuchElementException


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_mfc_pagination(shift_fixture,sitrep):
    """
    Ensure MFC paginates based on case
    """
    
    driver, comb = shift_fixture

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 25

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.mfc_pagination(comb, driver)

    else:
        comb, driver = shift.mfc_pagination(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1 
        
    # Additional asserts according to case       
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 25
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
    

def test_mfc_doc_type(shift_fixture):
    """
    Ensure MFC doc type calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep: \
         
        xpath_doc_type, owner, driver = shift.mfc_doc_type(comb, driver)

        assert mock_time_sleep.call_count == 1
        assert driver.call_count == 3


def test_mfc_doc_type_match():
    """
    Ensure MFC matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.mfc_doc_type_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'AGREEMENT AND/OR CONTRACT FOR DEED'


def test_mfc_download(shift_fixture):
    """
    Ensure MFC download call
    """

    driver, comb = shift_fixture

    driver = shift.mfc_download(comb, driver)

    assert driver.call_count == 1


def test_mfc_revert(shift_fixture):
    """
    Ensure MFC download call
    """

    driver, comb = shift_fixture

    driver = shift.mfc_revert(driver)

    assert driver.call_count == 3


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Alachua)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

    
def test_Alachua_search_results_count(shift_fixture):
    """
    Ensure Alachua search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        search_results, driver = shift.Alachua_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1
    assert search_results == 1


def test_sort_Alachua(shift_fixture):
    """
    Ensure Alachua sort calls
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:
        
        driver = shift.sort_Alachua(driver)

    assert mock_Select.call_count == 1
    assert driver.call_count == 1

        
@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Alachua(shift_fixture, sitrep):
    """
    Ensure Alachua paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 0

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 25

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Alachua(comb, driver)

    else:
        comb, driver = shift.pagination_Alachua(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 0
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 25
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
    

def test_xpath_doc_type_and_owner_Alachua(shift_fixture):
    """
    Ensure Alachua doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Alachua(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Alachua_match():
    """
    Ensure Alachua matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Alachua_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'
    

def test_click_image_Alachua(shift_fixture):
    """
    Ensure Alachua click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.ActionChains') as mock_ActionChains:

        driver = shift.click_image_Alachua(comb, driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1
    assert mock_ActionChains.call_count == 1


def test_predownload_Alachua(shift_fixture):
    """
    Ensure Alachua predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Alachua(driver)

    assert mock_time_sleep.call_count == 3
    assert driver.call_count == 4
    assert mock_WebDriverWait.call_count == 4


def test_download_Alachua(shift_fixture):
    """
    Ensure Alachua download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Alachua(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Alachua(shift_fixture):
    """
    Ensure Alachua revert calls
    """
    
    driver, comb = shift_fixture

    driver = shift.revert_Alachua(driver)

    assert driver.call_count == 3


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Baker))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))    


def test_Baker_search_results_count(shift_fixture):
    """
    Ensure Baker search results calls
    """

    driver, comb = shift_fixture
    
    with patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Baker_raw_search_results') as mock_convert_Baker_raw_search_results:
               
        search_results, driver = shift.Baker_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Baker_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results',1),
    ('No records found',0)
])
def test_convert_Baker_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Baker converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Baker_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Baker(shift_fixture):
    """
    Ensure Baker sort call
    """

    driver, comb = shift_fixture
    
    driver = shift.sort_Baker(driver)

    assert driver.call_count == 1


def test_pagination_Baker(shift_fixture):
    """
    Ensure proper Baker pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:
        
        view_all, driver = shift.pagination_Baker(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Baker(shift_fixture):
    """
    Ensure Baker doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Baker(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Baker_match():
    """
    Ensure Baker matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Baker_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Baker(shift_fixture):
    """
    Ensure Baker click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.click_image_Baker(comb, driver)

    assert mock_time_sleep.call_count == 1
    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 2


def test_download_Baker(shift_fixture):
    """
    Ensure Baker download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Baker(driver)

    assert driver.call_count == 2


def test_revert_Baker(shift_fixture):
    """
    Ensure Baker revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Baker(driver)

    assert driver.call_count == 1


# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((Bay))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Bay_search_results_count(shift_fixture):
    """
    Ensure Bay search results calls
    """

    driver, comb = shift_fixture
    
    with patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Bay_raw_search_results') as mock_convert_Bay_raw_search_results:
               
        search_results, driver = shift.Bay_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Bay_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results',1),
    ('No records found',0)
])
def test_convert_Bay_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Bay converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Bay_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Bay(shift_fixture):
    """
    Ensure Bay sort call
    """

    driver, comb = shift_fixture
    
    driver = shift.sort_Bay(driver)

    assert driver.call_count == 1


def test_pagination_Bay(shift_fixture):
    """
    Ensure proper Bay pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Bay(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Bay(shift_fixture):
    """
    Ensure Bay doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Baker(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Bay_match():
    """
    Ensure Bay matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Bay_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Bay(shift_fixture):
    """
    Ensure Bay click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.click_image_Bay(comb, driver)

    assert mock_time_sleep.call_count == 1
    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 2


def test_download_Bay(shift_fixture):
    """
    Ensure Bay download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Bay(driver)

    assert driver.call_count == 2


def test_revert_Bay(shift_fixture):
    """
    Ensure Bay revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Bay(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Brevard)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_checkbox_exist', [
    ('Standard', True),
    ('TimeoutException', False)
])
def test_checkbox_Brevard(shift_fixture, sitrep, expected_checkbox_exist):
    """
    Ensure Brevard checkbox call and failure
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        if sitrep == 'Standard':
            pass
        
        else:
            mock_WebDriverWait.side_effect = TimeoutException

        check_box_exist, driver = shift.checkbox_Brevard(driver)

    assert mock_WebDriverWait.call_count == 1
    assert check_box_exist == expected_checkbox_exist

    
@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Checkbox False', 0),
    ('Standard', 1)
])
def test_Brevard_search_results(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Brevard search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    if sitrep == 'Checkbox False':
        check_box_exist = False
        
    else:
        check_box_exist = True

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Brevard_raw_search_results', return_value = 1) as mock_convert_Brevard_raw_search_results:
    
        search_results, driver = shift.Brevard_search_results_count(check_box_exist, driver)

    assert search_results == expected_search_results

    # Additional asserts for Standard case       
    if sitrep == 'Standard':

        assert mock_time_sleep.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert mock_convert_Brevard_raw_search_results.call_count == 1


def test_convert_Brevard_raw_search_results():
    """
    Ensure Brevard converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Brevard_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Brevard(shift_fixture, sitrep):
    """
    Ensure Brevard search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 11

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Brevard(comb, driver)

    else:
        comb, driver = shift.pagination_Brevard(comb, driver)
        
    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1 
        
    # Additional asserts according to case           
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 11
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
    

def test_xpath_doc_type_and_owner_Brevard(shift_fixture):
    """
    Ensure Brevard doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Brevard(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Brevard_match():
    """
    Ensure Bay matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Brevard_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'AGREEMENT/CONTRACT FOR DEED'


def test_click_image_Brevard(shift_fixture):
    """
    Ensure Brevard click image call
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.click_image_Brevard(comb, driver)

    assert mock_time_sleep.call_count == 1
    assert driver.call_count == 2


def test_predownload_Brevard(shift_fixture):
    """
    Ensure Brevard predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Brevard(driver)

    assert mock_WebDriverWait.call_count == 4
    assert driver.call_count == 3


def test_download_Brevard(shift_fixture):
    """
    Ensure Brevard download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Brevard(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Brevard(shift_fixture):
    """
    Ensure Brevard revert calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.revert_Brevard(driver)

    assert driver.call_count == 3
    assert mock_time_sleep.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Broward)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_Broward_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Broward search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Broward_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Broward_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
               
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0

            
def test_convert_Broward_raw_search_results():
    """
    Ensure Broward converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Broward_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Broward(shift_fixture, sitrep):
    """
    Ensure Broward search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 12

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Broward(comb, driver)

    else:
        comb, driver = shift.pagination_Broward(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1 
        
    # Additional asserts according to case          
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 12
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_Broward(shift_fixture):
    """
    Ensure Broward doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Broward(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Broward_match():
    """
    Ensure Broward matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Broward_match()

    assert match_doc_type_1 == 'Mortgage/ Modifications & Assumptions'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Broward(shift_fixture):
    """
    Ensure Broward click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.ActionChains') as mock_ActionChains, \
         patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.click_image_Broward(comb, driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1
    assert mock_ActionChains.call_count == 1


def test_predownload_Broward(shift_fixture):
    """
    Ensure Broward predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Broward(driver)

    assert mock_WebDriverWait.call_count == 6
    assert driver.call_count == 3


def test_download_Broward(shift_fixture):
    """
    Ensure Broward download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Broward(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1
    

def test_revert_Broward(shift_fixture):
    """
    Ensure Broward revert calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.revert_Broward(driver)

    assert driver.call_count == 3
    assert mock_time_sleep.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((Charlotte)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Charlotte_search_results_count(shift_fixture):
    """
    Ensure Charlotte search results calls
    """

    driver, comb = shift_fixture
    
    with patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Charlotte_raw_search_results') as mock_convert_Charlotte_raw_search_results:
               
        search_results, driver = shift.Charlotte_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Charlotte_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results',1),
    ('No records found',0)
])
def test_convert_Charlotte_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Charlotte converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Charlotte_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Charlotte(shift_fixture):
    """
    Ensure Charlotte sort call
    """

    driver, comb = shift_fixture
    
    driver = shift.sort_Charlotte(driver)

    assert driver.call_count == 1


def test_pagination_Charlotte(shift_fixture):
    """
    Ensure proper Charlotte pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Charlotte(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Charlotte(shift_fixture):
    """
    Ensure Charlotte doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Charlotte(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Charlotte_match():
    """
    Ensure Charlotte matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Charlotte_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Charlotte(shift_fixture):
    """
    Ensure Charlotte click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.click_image_Charlotte(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_Charlotte(shift_fixture):
    """
    Ensure Charlotte download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Charlotte(driver)

    assert driver.call_count == 2


def test_revert_Charlotte(shift_fixture):
    """
    Ensure Charlotte revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Charlotte(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Citrus)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Citrus_search_results_count(shift_fixture):
    """
    Ensure Citrus search results calls
    """

    driver, comb = shift_fixture
    
    with patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Citrus_raw_search_results') as mock_convert_Citrus_raw_search_results:
               
        search_results, driver = shift.Citrus_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Citrus_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results',1),
    ('No records found',0)
])
def test_convert_Citrus_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Citrus converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Citrus_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Citrus(shift_fixture):
    """
    Ensure Citrus sort call
    """

    driver, comb = shift_fixture
    
    driver = shift.sort_Citrus(driver)

    assert driver.call_count == 1


def test_pagination_Citrus(shift_fixture):
    """
    Ensure proper Citrus pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Citrus(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Citrus(shift_fixture):
    """
    Ensure Citrus doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Citrus(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Citrus_match():
    """
    Ensure Citrus matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Citrus_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Citrus(shift_fixture):
    """
    Ensure Citrus click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.click_image_Citrus(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_Citrus(shift_fixture):
    """
    Ensure Citrus download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Citrus(driver)

    assert driver.call_count == 2


def test_revert_Citrus(shift_fixture):
    """
    Ensure Citrus revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Citrus(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((Clay))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Clay_search_results_count(shift_fixture):
    """
    Ensure Clay search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Clay_raw_search_results') as mock_convert_Clay_raw_search_results:

        search_results, driver = shift.Clay_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Clay_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Clay_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Clay converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Clay_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Clay(shift_fixture):
    """
    Ensure Clay sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Clay(driver)

    assert driver.call_count == 1


def test_pagination_Clay(shift_fixture):
    """
    Ensure proper Clay pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:
        
        view_all, driver = shift.pagination_Clay(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Clay(shift_fixture):
    """
    Ensure Clay doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Clay(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Clay_match():
    """
    Ensure Clay matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Clay_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Clay(shift_fixture):
    """
    Ensure Clay click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        driver = shift.click_image_Clay(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_Clay(shift_fixture):
    """
    Ensure Clay download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Clay(driver)

    assert driver.call_count == 2


def test_revert_Clay(shift_fixture):
    """
    Ensure Clay revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Clay(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Collier)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Collier_search_results_count(shift_fixture):
    """
    Ensure Collier searh results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Collier_raw_search_results') as mock_convert_Collier_raw_search_results:

        search_results, driver = shift.Collier_search_results_count(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Collier_raw_search_results.call_count == 1

    
@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Collier_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Collier converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'criteria.'

    search_results = shift.convert_Collier_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Collier(shift_fixture, sitrep):
    """
    Ensure Collier search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    # Mod stub comb for scenario
    comb['doc_counter'] = 3

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 50

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Collier(comb, driver)

    else:
        comb, driver = shift.pagination_Collier(comb, driver)
        
    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 3
        
    # Additional asserts according to case        
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 50
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
    
def test_xpath_doc_type_and_owner_Collier(shift_fixture):
    """
    Ensure Collier doc type calls
    """

    driver, comb = shift_fixture

    with patch('shift.re.findall') as mock_re_findall:

            xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Collier(comb, driver)

    assert driver.call_count == 1
    assert mock_re_findall.call_count == 1


def test_doc_type_Collier_match():
    """
    Ensure Collier doc type match
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Collier_match()

    assert match_doc_type_1 == 'MTGE'
    assert match_doc_type_2 == 'MULE'


def test_download_Collier(shift_fixture):
    """
    Ensure Collier download call
    """

    driver, comb = shift_fixture

    driver = shift.download_Collier(comb, driver)

    assert driver.call_count == 1


def test_revert_Collier(shift_fixture):
    """
    Ensure Collier revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_Collier(driver)

    assert driver.call_count == 4


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((Duval)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_checkbox_exist', [
    ('Standard', True),
    ('TimeoutException', False)
])
def test_checkbox_Duval(shift_fixture, sitrep, expected_checkbox_exist):
    """
    Ensure Duval checkbox call and failure
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        if sitrep == 'Standard':
            pass
        
        else:
            mock_WebDriverWait.side_effect = TimeoutException

        check_box_exist, driver = shift.checkbox_Duval(driver)

    assert mock_WebDriverWait.call_count == 1
    assert check_box_exist == expected_checkbox_exist

    
@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Checkbox False', 0),
    ('Standard', 1)
])
def test_Duval_search_results(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Duval search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    if sitrep == 'Checkbox False':
        check_box_exist = False
        
    else:
        check_box_exist = True

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Duval_raw_search_results', return_value = 1) as mock_convert_Duval_raw_search_results:
    
        search_results, driver = shift.Duval_search_results_count(check_box_exist, driver)

    assert search_results == expected_search_results
    
    # Additional assert for Standard case      
    if sitrep == 'Standard':

        assert mock_time_sleep.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert mock_convert_Duval_raw_search_results.call_count == 1


def test_convert_Duval_raw_search_results():
    """
    Ensure Duval converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Duval_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Duval(shift_fixture, sitrep):
    """
    Ensure Duval search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 11

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Duval(comb, driver)

    else:
        comb, driver = shift.pagination_Duval(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1 

    # Additional asserts according to case         
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 11
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
    

def test_xpath_doc_type_and_owner_Duval(shift_fixture):
    """
    Ensure Duval doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Duval(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Duval_match():
    """
    Ensure Duval matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Duval_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_download_Duval(shift_fixture):
    """
    Ensure Duval download call
    """

    driver, comb = shift_fixture

    driver = shift.download_Duval(comb, driver)

    assert driver.call_count == 1


def test_revert_Duval(shift_fixture):
    """
    Ensure Duval revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_Duval(driver)

    assert driver.call_count == 3


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Escambia))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Escambia_search_results_count(shift_fixture):
    """
    Ensure Escambia search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
            patch('shift.convert_Escambia_raw_search_results') as mock_convert_Escambia_raw_search_results:
        search_results, driver = shift.Escambia_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Escambia_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Escambia_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Escambia converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Escambia_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Escambia(shift_fixture):
    """
    Ensure Escambia sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Escambia(driver)

    assert driver.call_count == 1


def test_pagination_Escambia(shift_fixture):
    """
    Ensure proper Escambia pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Escambia(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Escambia(shift_fixture):
    """
    Ensure Escambia doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Escambia(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Escambia_match():
    """
    Ensure Escambia matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Escambia_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Escambia(shift_fixture):
    """
    Ensure Escambia click image calls
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Escambia(comb, driver)

    assert driver.call_count == 1


def test_download_Escambia(shift_fixture):
    """
    Ensure Escambia download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Escambia(driver)

    assert driver.call_count == 2


def test_revert_Escambia(shift_fixture):
    """
    Ensure Escambia revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Escambia(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Hernando))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Hernando_search_results_count(shift_fixture):
    """
    Ensure Hernando search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
            patch('shift.convert_Hernando_raw_search_results') as mock_convert_Hernando_raw_search_results:
        search_results, driver = shift.Hernando_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Hernando_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Hernando_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Hernando converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Hernando_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Hernando(shift_fixture):
    """
    Ensure Hernando sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Hernando(driver)

    assert driver.call_count == 1


def test_pagination_Hernando(shift_fixture):
    """
    Ensure proper Hernando pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Hernando(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Hernando(shift_fixture):
    """
    Ensure Hernando doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Hernando(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Hernando_match():
    """
    Ensure Hernando matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Hernando_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Hernando(shift_fixture):
    """
    Ensure Hernando click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Hernando(comb, driver)

    assert driver.call_count == 1


def test_download_Hernando(shift_fixture):
    """
    Ensure Hernando download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Hernando(driver)

    assert driver.call_count == 2


def test_revert_Hernando(shift_fixture):
    """
    Ensure Hernando revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Hernando(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Highlands))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 0),
    ('TimeoutException', 1)
])
def test_Highlands_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Highlands search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.Highlands_loading_image') as mock_Highlands_loading_image, \
         patch('shift.convert_Highlands_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Highlands_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_Highlands_loading_image.call_count == 1
    
    # Additional asserts according to case                      
    if sitrep == 'Standard':
        assert search_results == 0

    else:
        assert search_results == 1


def test_Highlands_loading_image(shift_fixture):
    """
    Ensure Highlands loading image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        
        driver = shift.Highlands_loading_image(driver)

    assert mock_WebDriverWait.call_count == 2

    
def test_convert_Highlands_raw_search_results():
    """
    Ensure Highlands converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Highlands_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Highlands(shift_fixture, sitrep):
    """
    Ensure Highlands paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 1

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 15

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Highlands(comb, driver)

    else:
        comb, driver = shift.pagination_Highlands(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1
    
    # Additional asserts according to case           
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 15
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_Highlands(shift_fixture):
    """
    Ensure Highlands doc type call
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Highlands(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Highlands_match():
    """
    Ensure Highlands matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Highlands_match()

    assert match_doc_type_1 == 'M'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Highlands(shift_fixture):
    """
    Ensure Highlands click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Highlands(comb, driver)

    assert driver.call_count == 1


def test_predownload_Highlands(shift_fixture):
    """
    Ensure Highlands predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Highlands(driver)

    assert mock_time_sleep.call_count == 1
    assert driver.call_count == 2
    assert mock_WebDriverWait.call_count == 1


def test_download_Highlands(shift_fixture):
    """
    Ensure Highlands download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Highlands(driver)

    assert driver.call_count == 2


def test_revert_Highlands(shift_fixture):
    """
    Ensure Highlands revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_Highlands(driver)

    assert driver.call_count == 5


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((Hillsborough)))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_Hillsborough_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Hillsborough search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Hillsborough_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Hillsborough_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    
    # Additional asserts according to case                    
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0

            
def test_convert_Hillsborough_raw_search_results():
    """
    Ensure Hillsborough converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Hillsborough_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Hillsborough(shift_fixture):
    """
    Ensure Hillsborough sort calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.sort_Hillsborough(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate'),
    ('Load More')
])
def test_pagination_Hillsborough(shift_fixture,sitrep):
    """
    Ensure Hillsborough search results are retrieved properly based on case
    """

    driver, comb = shift_fixture

    # Mod stub comb for this scenario
    comb['pagination_counter'] = 1
    comb['pagination_load_more'] = 10
    comb['doc_counter'] = 3

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 30

    elif sitrep == 'Load More':
        comb['pagination_counter'] = 10

    # Execute function according to case
    if sitrep == 'Paginate' or sitrep == 'Load More':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Hillsborough(comb, driver)

    else:
        comb, driver = shift.pagination_Hillsborough(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 3

    # Additional asserts according to case         
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0
        assert comb['pagination_counter'] == 1

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 30
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 2

    elif sitrep == 'Load More':

        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 2
        assert comb['pagination_load_more'] == 20
        
    
def test_xpath_doc_type_and_owner_Hillsborough(shift_fixture):
    """
    Ensure Hillsborough doc type call
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Hillsborough(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Hillsborough_match():
    """
    Ensure Hillsborough matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Hillsborough_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'AGREEMENT'


def test_click_image_Hillsborough(shift_fixture):
    """
    Ensure Hillsborough click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Hillsborough(comb, driver)

    assert driver.call_count == 1


def test_predownload_Hillsborough(shift_fixture):
    """
    Ensure Hillsborough predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Hillsborough(driver)

    assert mock_WebDriverWait.call_count == 2

    
def test_download_Hillsborough(shift_fixture):
    """
    Ensure Alachua download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Hillsborough(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Hillsborough(shift_fixture):
    """
    Ensure Hillsborough revert calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.revert_Hillsborough(driver)

    assert driver.call_count == 3
    assert mock_time_sleep.call_count == 1
        

# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((Indian River)))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_IndianRiver_search_results_count(shift_fixture):
    """
    Ensure IndianRiver search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
            patch('shift.convert_IndianRiver_raw_search_results') as mock_convert_IndianRiver_raw_search_results:
        search_results, driver = shift.IndianRiver_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_IndianRiver_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_IndianRiver_raw_search_results(sitrep, expected_search_results):
    """
    Ensure IndianRiver converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_IndianRiver_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_IndianRiver(shift_fixture):
    """
    Ensure IndianRiver sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_IndianRiver(driver)

    assert driver.call_count == 1


def test_pagination_IndianRiver(shift_fixture):
    """
    Ensure proper IndianRiver pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_IndianRiver(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_IndianRiver(shift_fixture):
    """
    Ensure IndianRiver doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_IndianRiver(comb, driver)

    assert driver.call_count == 1


def test_doc_type_IndianRiver_match():
    """
    Ensure IndianRiver matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_IndianRiver_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_IndianRiver(shift_fixture):
    """
    Ensure IndianRiver click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.time.sleep') as mock_time_sleep:
        
        driver = shift.click_image_IndianRiver(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1


def test_download_IndianRiver(shift_fixture):
    """
    Ensure IndianRiver download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_IndianRiver(driver)

    assert driver.call_count == 2


def test_revert_IndianRiver(shift_fixture):
    """
    Ensure IndianRiver revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_IndianRiver(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((Lake)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_checkbox_exist', [
    ('Standard', True),
    ('TimeoutException', False)
])
def test_checkbox_Lake(shift_fixture, sitrep, expected_checkbox_exist):
    """
    Ensure Lake checkbox call and failure
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        if sitrep == 'Standard':
            pass
        
        else:
            mock_WebDriverWait.side_effect = TimeoutException

        check_box_exist, driver = shift.checkbox_Lake(driver)

    assert mock_WebDriverWait.call_count == 1
    assert check_box_exist == expected_checkbox_exist

    
@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Checkbox False', 0),
    ('Standard', 1)
])
def test_Lake_search_results(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Lake search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    if sitrep == 'Checkbox False':
        check_box_exist = False
        
    else:
        check_box_exist = True

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Lake_raw_search_results', return_value = 1) as mock_convert_Lake_raw_search_results:
    
        search_results, driver = shift.Lake_search_results_count(check_box_exist, driver)

    assert search_results == expected_search_results
    
    # Additional asserts for Standard case      
    if sitrep == 'Standard':

        assert driver.call_count == 4
        assert mock_time_sleep.call_count == 1
        assert mock_WebDriverWait.call_count == 3
        assert mock_convert_Lake_raw_search_results.call_count == 1


def test_convert_Lake_raw_search_results():
    """
    Ensure Lake converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Lake_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Lake(shift_fixture):
    """
    Ensure Lake sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Lake(driver)

    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Lake(shift_fixture, sitrep):
    """
    Ensure Lake paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 1

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 50

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Lake(comb, driver)

    else:
        comb, driver = shift.pagination_Lake(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1
    
    # Additional asserts according to case         
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 50
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_Lake(shift_fixture):
    """
    Ensure Lake doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Lake(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Lake_match():
    """
    Ensure Lake matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Lake_match()

    assert match_doc_type_1 == 'MTG'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Lake(shift_fixture):
    """
    Ensure Lake click image calls
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Lake(comb, driver)

    assert driver.call_count == 1


def test_predownload_Lake(shift_fixture):
    """
    Ensure Lake predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Lake(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 2


def test_revert_Lake(shift_fixture):
    """
    Ensure Lake revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_Lake(driver)

    assert driver.call_count == 6


# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((Lee)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Lee_search_results_count(shift_fixture):
    """
    Ensure Lee search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
            patch('shift.convert_Lee_raw_search_results') as mock_convert_Lee_raw_search_results:
        search_results, driver = shift.Lee_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Lee_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Lee_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Lee converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Lee_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Lee(shift_fixture):
    """
    Ensure Lee sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Lee(driver)

    assert driver.call_count == 1


def test_pagination_Lee(shift_fixture):
    """
    Ensure proper Lee pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Lee(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Lee(shift_fixture):
    """
    Ensure Lee doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Lee(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Lee_match():
    """
    Ensure Lee matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Lee_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Lee(shift_fixture):
    """
    Ensure Lee click image calls
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Lee(comb, driver)

    assert driver.call_count == 1


def test_download_Lee(shift_fixture):
    """
    Ensure Lee download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Lee(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Lee(shift_fixture):
    """
    Ensure Lee revert call
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:
        
        driver = shift.revert_Lee(driver)

    assert driver.call_count == 3
    assert mock_time_sleep.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Marion))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('NoSuchElementException', 0)
])
def test_Marion_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Marion search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Marion_raw_search_results', return_value = 1):

        if sitrep == 'NoSuchElementException':
            mock_WebDriverWait.side_effect = NoSuchElementException

        search_results, driver = shift.Marion_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    
    # Additional asserts according to case                    
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0


def test_convert_Marion_raw_search_results():
    """
    Ensure Marion converts raw search results
    """

    raw_search_results = '1 Records'

    search_results = shift.convert_Marion_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Marion(shift_fixture):
    """
    Ensure Marion sort calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.sort_Marion(driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Marion(shift_fixture, sitrep):
    """
    Ensure Marion paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 20

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Marion(comb, driver)

    else:
        comb, driver = shift.pagination_Marion(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 2
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 20
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_Marion(shift_fixture):
    """
    Ensure Marion xpath doc type
    """

    driver, comb = shift_fixture

    xpath_doc_type = shift.xpath_doc_type_Marion(comb)

    assert xpath_doc_type == '/html/body/form/table[4]/tbody/tr[1]/td[4]/a/font'


def test_doc_type_Marion_match():
    """
    Ensure Marion matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Marion_match()

    assert match_doc_type_1 == 'MTG  '
    assert match_doc_type_2 == 'MULE'


def test_click_image_Marion(shift_fixture):
    """
    Ensure Marion click image calls
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Marion(comb, driver)

    assert driver.call_count == 1


def test_predownload_Marion(shift_fixture):
    """
    Ensure Marion predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.predownload_Marion(driver)

    assert mock_WebDriverWait.call_count == 4
    assert mock_time_sleep.call_count == 1
    assert driver.call_count == 5


def test_download_Marion(shift_fixture):
    """
    Ensure Marion download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Marion(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Marion(shift_fixture):
    """
    Ensure Marion revert calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:
        
        owner, driver = shift.revert_Marion(driver)

    assert driver.call_count == 7
    assert mock_time_sleep.call_count == 2


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Martin)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Martin_search_results_count(shift_fixture):
    """
    Ensure Martin search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
            patch('shift.convert_Martin_raw_search_results') as mock_convert_Martin_raw_search_results:
        search_results, driver = shift.Martin_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Martin_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Martin_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Martin converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Martin_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Martin(shift_fixture):
    """
    Ensure Martin sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Martin(driver)

    assert driver.call_count == 1


def test_pagination_Martin(shift_fixture):
    """
    Ensure proper Martin pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Martin(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Martin(shift_fixture):
    """
    Ensure Martin doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Martin(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Martin_match():
    """
    Ensure Martin matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Martin_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Martin(shift_fixture):
    """
    Ensure Martin click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        driver = shift.click_image_Martin(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_Martin(shift_fixture):
    """
    Ensure Martin download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Martin(driver)

    assert driver.call_count == 2


def test_revert_Martin(shift_fixture):
    """
    Ensure Martin revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Martin(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((MiamiDade))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep', [
    ('Found search results'),
    ('No records found'),
    ('NoSuchElementException')
])
def test_MiamiDade_search_results_count(shift_fixture,sitrep):
    """
    Ensure MiamiDade search results are retrieved properly based on case
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_MiamiDade_raw_search_results', return_value = 1) as mock_MiamiDade_raw_search_results:

        # Set side effects according to case
        if sitrep == 'No records found':
            mock_WebDriverWait.side_effect = TimeoutException

        elif sitrep == 'NoSuchElementException':
            mock_WebDriverWait.side_effect = TimeoutException
            driver.side_effect = NoSuchElementException

        else:
            pass

        # Execute function with proper unpacking vars according to case
        if sitrep == 'NoSuchElementException':
            crash = shift.MiamiDade_search_results_count(driver)

        else:
            search_results, driver = shift.MiamiDade_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1

    # Additional asserts according to case
    if sitrep == 'Found search results':
        assert mock_MiamiDade_raw_search_results.call_count == 1
        assert search_results == 1

    elif sitrep == 'No records found':
        assert search_results == 0

    elif sitrep == 'NoSuchElementException':
        assert crash == NoSuchElementException


def test_convert_MiamiDade_raw_search_results():
    """
    Ensure MiamiDade converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_MiamiDade_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_MiamiDade(shift_fixture, sitrep):
    """
    Ensure MiamiDade paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 2
    comb['pagination_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 50

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            
            comb, driver = shift.pagination_MiamiDade(comb, driver)

    else:
        comb, driver = shift.pagination_MiamiDade(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 2
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 50
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 3


def test_xpath_doc_type_and_owner_MiamiDade(shift_fixture):
    """
    Ensure MiamiDade doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_MiamiDade(comb, driver)

    assert driver.call_count == 1


def test_doc_type_MiamiDade_match():
    """
    Ensure MiamiDade matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_MiamiDade_match()

    assert match_doc_type_1 == 'MOR'
    assert match_doc_type_2 == 'MULE'


def test_click_image_MiamiDade(shift_fixture):
    """
    Ensure MiamiDade click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.click_image_MiamiDade(comb, driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1


def test_predownload_MiamiDade(shift_fixture):
    """
    Ensure MiamiDade predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_MiamiDade(driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1
    assert mock_WebDriverWait.call_count == 1


def test_download_MiamiDade(shift_fixture):
    """
    Ensure MiamiDade download calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.download_MiamiDade(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 1


def test_revert_MiamiDade(shift_fixture):
    """
    Ensure MiamiDade revert calls
    """

    driver, comb = shift_fixture
    driver = shift.revert_MiamiDade(driver)

    assert driver.call_count == 5


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((Nassau))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_Nassau_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Nassau search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Nassau_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Nassau_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
               
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0


def test_convert_Nassau_raw_search_results():
    """
    Ensure Nassau converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Nassau_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Nassau(shift_fixture):
    """
    Ensure Nassau sort calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.sort_Nassau(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate'),
    ('Load More')
])
def test_pagination_Nassau(shift_fixture,sitrep):
    """
    Ensure Nassau search results are retrieved properly based on case
    """

    driver, comb = shift_fixture

    # Mod stub comb for this scenario
    comb['pagination_counter'] = 1
    comb['pagination_load_more'] = 10
    comb['doc_counter'] = 3

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 15

    elif sitrep == 'Load More':
        comb['pagination_counter'] = 10

    # Execute function according to case
    if sitrep == 'Paginate' or sitrep == 'Load More':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Nassau(comb, driver)

    else:
        comb, driver = shift.pagination_Nassau(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 3

    # Additional asserts according to case         
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0
        assert comb['pagination_counter'] == 1

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 15
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 2

    elif sitrep == 'Load More':

        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 2
        assert comb['pagination_load_more'] == 20


def test_xpath_doc_type_and_owner_Nassau(shift_fixture):
    """
    Ensure Nassau doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Nassau(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Nassau_match():
    """
    Ensure Nassau matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Nassau_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Nassau(shift_fixture):
    """
    Ensure Nassau click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:
        driver = shift.click_image_Nassau(comb, driver)

    assert mock_time_sleep.call_count == 1
    assert driver.call_count == 2
    

def test_predownload_Nassau(shift_fixture):
    """
    Ensure Nassau predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        driver = shift.predownload_Nassau(driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 2


def test_download_Nassau(shift_fixture):
    """
    Ensure Nassau download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.download_Nassau(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1
    assert mock_WebDriverWait.call_count == 1


def test_revert_Nassau(shift_fixture):
    """
    Ensure Nassau revert calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:
        driver = shift.revert_Nassau(driver)

    assert mock_time_sleep.call_count == 1
    assert driver.call_count == 3


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((Okeechobee))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_Okeechobee_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Okeechobee search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Okeechobee_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Okeechobee_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
               
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0


def test_convert_Okeechobee_raw_search_results():
    """
    Ensure Okeechobee converts raw search results
    """

    raw_search_results = '1 total'

    search_results = shift.convert_Okeechobee_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Okeechobee(shift_fixture):
    """
    Ensure Okeechobee sort call
    """

    driver, comb = shift_fixture


    driver = shift.sort_Okeechobee(driver)

    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Okeechobee(shift_fixture, sitrep):
    """
    Ensure Okeechobee paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 1
    comb['pagination_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 25

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Okeechobee(comb, driver)

    else:
        comb, driver = shift.pagination_Okeechobee(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1
    
    # Additional asserts according to case           
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 25
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 3


def test_xpath_doc_type_and_owner_Okeechobee(shift_fixture):
    """
    Ensure Okeechobee doc type call
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Okeechobee(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Okeechobee_match():
    """
    Ensure Okeechobee matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Okeechobee_match()

    assert match_doc_type_1 == 'MTG'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Okeechobee(shift_fixture):
    """
    Ensure Okeechobee click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Okeechobee(comb, driver)

    assert driver.call_count == 1


def test_predownload_Okeechobee(shift_fixture):
    """
    Ensure Okeechobee predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        
        driver = shift.predownload_Okeechobee(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 2


def test_donwnload_Okeechobee(shift_fixture):
    """
    Ensure Okeechobee download call
    """

    driver, comb = shift_fixture

    driver = shift.download_Okeechobee(driver)

    assert driver.call_count == 1


def test_revert_Okeechobee(shift_fixture):
    """
    Ensure Okeechobee revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Okeechobee(driver)

    assert driver.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Osceola)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_Osceola_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Osceola search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Osceola_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Osceola_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
               
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0


def test_convert_Osceola_raw_search_results():
    """
    Ensure Osceola converts raw search results
    """

    raw_search_results = '1 Records'

    search_results = shift.convert_Osceola_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Osceola(shift_fixture, sitrep):
    """
    Ensure Osceola paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 20

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Osceola(comb, driver)

    else:
        comb, driver = shift.pagination_Osceola(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 2
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 20
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_Osceola(shift_fixture):
    """
    Ensure Osceola xpath doc type
    """

    driver, comb = shift_fixture

    xpath_doc_type = shift.xpath_doc_type_Osceola(comb)

    assert xpath_doc_type == '/html/body/form/table[4]/tbody/tr[1]/td[4]/a/font'


def test_doc_type_Osceola_match():
    """
    Ensure Osceola matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Osceola_match()

    assert match_doc_type_1 == 'MTG  '
    assert match_doc_type_2 == 'MULE'


def test_click_image_Osceola(shift_fixture):
    """
    Ensure Osceola click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Osceola(comb, driver)

    assert driver.call_count == 1


def test_predownload_Osceola(shift_fixture):
    """
    Ensure Osceola predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        
        driver = shift.predownload_Osceola(driver)

    assert mock_WebDriverWait.call_count == 3
    assert driver.call_count == 5


def test_download_Osceola(shift_fixture):
    """
    Ensure Osceola download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Osceola(driver)

    assert driver.call_count == 3


def test_revert_Osceola(shift_fixture):
    """
    Ensure Osceola revert calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep:
        
        owner, driver = shift.revert_Osceola(driver)

    assert driver.call_count == 5
    assert mock_time_sleep.call_count == 1


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Palm Beach)))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_PalmBeach_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Palm Beach search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_PalmBeach_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.PalmBeach_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
               
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0


def test_convert_PalmBeach_raw_search_results():
    """
    Ensure Palm Beach converts raw search results
    """

    raw_search_results = '1 Records'

    search_results = shift.convert_PalmBeach_raw_search_results(raw_search_results)

    assert search_results == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_PalmBeach(shift_fixture, sitrep):
    """
    Ensure Palm Beach paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 20

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_PalmBeach(comb, driver)

    else:
        comb, driver = shift.pagination_PalmBeach(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 2
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 20
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_PalmBeach(shift_fixture):
    """
    Ensure Palm Beach doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_PalmBeach(comb, driver)

    assert driver.call_count == 1


def test_doc_type_PalmBeach_match():
    """
    Ensure Palm Beach matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_PalmBeach_match()

    assert match_doc_type_1 == 'MTG  '
    assert match_doc_type_2 == 'MULE'


def test_click_image_PalmBeach(shift_fixture):
    """
    Ensure Palm Beach click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_PalmBeach(comb, driver)

    assert driver.call_count == 1


def test_predownload_PalmBeach(shift_fixture):
    """
    Ensure Palm Beach predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        
        driver = shift.predownload_PalmBeach(driver)

    assert mock_WebDriverWait.call_count == 3
    assert driver.call_count == 5


def test_download_PalmBeach(shift_fixture):
    """
    Ensure Palm Beach download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.download_PalmBeach(driver)

    assert driver.call_count == 5
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1
    assert mock_WebDriverWait.call_count == 2


def test_revert_PalmBeach(shift_fixture):
    """
    Ensure Palm Beach revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_PalmBeach(driver)

    assert driver.call_count == 5


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Pinellas)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_checkbox_exist', [
    ('Standard', True),
    ('TimeoutException', False)
])
def test_checkbox_Pinellas(shift_fixture, sitrep, expected_checkbox_exist):
    """
    Ensure Pinellas checkbox call and failure
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        if sitrep == 'Standard':
            pass
        
        else:
            mock_WebDriverWait.side_effect = TimeoutException

        check_box_exist, driver = shift.checkbox_Pinellas(driver)

    assert mock_WebDriverWait.call_count == 1
    assert check_box_exist == expected_checkbox_exist

    
@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Checkbox False', 0),
    ('Standard', 1)
])
def test_Pinellas_search_results(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Pinellas search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    if sitrep == 'Checkbox False':
        check_box_exist = False
        
    else:
        check_box_exist = True

    with patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_Pinellas_raw_search_results', return_value = 1) as mock_convert_Pinellas_raw_search_results:
    
        search_results, driver = shift.Pinellas_search_results_count(check_box_exist, driver)

    assert search_results == expected_search_results
    
    # Additional assert for Standard case      
    if sitrep == 'Standard':

        assert mock_WebDriverWait.call_count == 1
        assert mock_convert_Pinellas_raw_search_results.call_count == 1
        assert driver.call_count == 2


def test_convert_Pinellas_raw_search_results():
    """
    Ensure Pinellas converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_Pinellas_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Pinellas(shift_fixture):
    """
    Ensure Pinellas sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Pinellas(driver)

    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate'),
    ('Load More')
])
def test_pagination_Pinellas(shift_fixture,sitrep):
    """
    Ensure Pinellas search results are retrieved properly based on case
    """

    driver, comb = shift_fixture

    # Mod stub comb for this scenario
    comb['doc_counter'] = 1
    comb['pagination_counter'] = 0
    comb['pagination_load_more'] = 10

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 11

    elif sitrep == 'Load More':
        comb['pagination_counter'] = 10

    # Execute function according to case
    if sitrep == 'Paginate' or sitrep == 'Load More':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            
            comb, driver = shift.pagination_Pinellas(comb, driver)

    else:
        comb, driver = shift.pagination_Pinellas(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1

    # Additional asserts according to case         
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0
        assert comb['pagination_counter'] == 1

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 11
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 1

    elif sitrep == 'Load More':

        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 10
        assert comb['pagination_load_more'] == 20


def test_xpath_doc_type_and_owner_Pinellas(shift_fixture):
    """
    Ensure Pinellas doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Pinellas(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Pinellas_match():
    """
    Ensure Pinellas matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Pinellas_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'AGREEMENT AND OR CONTRACT FOR DEED'


def test_click_image_Pinellas(shift_fixture):
    """
    Ensure Pinellas click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Pinellas(comb, driver)

    assert driver.call_count == 1


def test_predownload_Pinellas(shift_fixture):
    """
    Ensure Pinellas predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.time.sleep') as mock_time_sleep:
        
        driver = shift.predownload_Pinellas(driver)

    assert mock_WebDriverWait.call_count == 3
    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1


def test_donload_Pinellas(shift_fixture):
    """
    Ensure Pinellas download call
    """

    driver, comb = shift_fixture

    driver = shift.download_Pinellas(driver)

    assert driver.call_count == 1


def test_revert_Pinellas(shift_fixture):
    """
    Ensure Pinellas click image call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Pinellas(driver)

    assert driver.call_count == 4


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((Polk)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('TimeoutException', 0)
])
def test_Polk_search_results_count(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Polk search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Polk_raw_search_results', return_value = 1):

        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Polk_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
               
    if sitrep == 'Standard':
        assert search_results == 1

    else:
        assert search_results == 0


def test_convert_Polk_raw_search_results():
    """
    Ensure Polk converts raw search results
    """

    raw_search_results = '1 total'

    search_results = shift.convert_Polk_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_Polk(shift_fixture):
    """
    Ensure Polk sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Polk(driver)

    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Polk(shift_fixture, sitrep):
    """
    Ensure Polk paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['pagination_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 25

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Polk(comb, driver)

    else:
        comb, driver = shift.pagination_Polk(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 25
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
        assert comb['pagination_counter'] == 3


def test_xpath_doc_type_Polk(shift_fixture):
    """
    Ensure Polk xpath doc type
    """

    driver, comb = shift_fixture

    xpath_doc_type = shift.xpath_doc_type_Polk(comb)

    assert xpath_doc_type == '/html/body/div[1]/div[2]/div/div[2]/section[3]/div[3]/div/div/div/div[1]/div[2]/div/div[1]/' + \
                     'div/div[4]/div[2]/div/div/div[1]/div[5]'


def test_doc_type_Polk_match():
    """
    Ensure Polk matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Polk_match()

    assert match_doc_type_1 == 'MTG'
    assert match_doc_type_2 == 'AGD'


def test_click_image_Polk(shift_fixture):
    """
    Ensure Polk click image call
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Polk(comb, driver)

    assert driver.call_count == 1


def test_predownload_Polk(shift_fixture):
    """
    Ensure Polk predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        
        driver = shift.predownload_Polk(driver)

    assert mock_WebDriverWait.call_count == 2
    assert driver.call_count == 1


def test_download_Polk(shift_fixture):
    """
    Ensure Polk download call
    """

    driver, comb = shift_fixture

    driver = shift.download_Polk(driver)

    assert driver.call_count == 1


def test_revert_Polk(shift_fixture):
    """
    Ensure Polk revert call
    """

    driver, comb = shift_fixture

    owner, driver = shift.revert_Polk(driver)

    assert driver.call_count == 2


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((Saint Johns))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_SaintJohns_search_results_count(shift_fixture):
    """
    Ensure SaintJohns search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_SaintJohns_raw_search_results') as mock_convert_SaintJohns_raw_search_results:
        
        search_results, driver = shift.SaintJohns_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_SaintJohns_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_SaintJohns_raw_search_results(sitrep, expected_search_results):
    """
    Ensure SaintJohns converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_SaintJohns_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_SaintJohns(shift_fixture):
    """
    Ensure SaintJohns sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_SaintJohns(driver)

    assert driver.call_count == 1


def test_pagination_SaintJohns(shift_fixture):
    """
    Ensure proper SaintJohns pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_SaintJohns(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_SaintJohns(shift_fixture):
    """
    Ensure SaintJohns doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_SaintJohns(comb, driver)

    assert driver.call_count == 1


def test_doc_type_SaintJohns_match():
    """
    Ensure SaintJohns matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_SaintJohns_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_SaintJohns(shift_fixture):
    """
    Ensure SaintJohns click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        driver = shift.click_image_SaintJohns(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_SaintJohns(shift_fixture):
    """
    Ensure SaintJohns download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_SaintJohns(driver)

    assert driver.call_count == 2


def test_revert_SaintJohns(shift_fixture):
    """
    Ensure SaintJohns revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_SaintJohns(driver)

    assert driver.call_count == 1


# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Saint Lucie)))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep,expected_checkbox_exist', [
    ('Standard', True),
    ('TimeoutException', False)
])
def test_checkbox_SaintLucie(shift_fixture, sitrep, expected_checkbox_exist):
    """
    Ensure Saint Lucie checkbox call and failure
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        if sitrep == 'Standard':
            pass
        
        else:
            mock_WebDriverWait.side_effect = TimeoutException

        check_box_exist, driver = shift.checkbox_SaintLucie(driver)

    assert mock_WebDriverWait.call_count == 1
    assert check_box_exist == expected_checkbox_exist

    
@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Checkbox False', 0),
    ('Standard', 1)
])
def test_SaintLucie_search_results(shift_fixture, sitrep, expected_search_results):
    """
    Ensure Saint Lucie search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    if sitrep == 'Checkbox False':
        check_box_exist = False
        
    else:
        check_box_exist = True

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait,\
         patch('shift.convert_SaintLucie_raw_search_results', return_value = 1) as mock_convert_SaintLucie_raw_search_results:
    
        search_results, driver = shift.SaintLucie_search_results_count(check_box_exist, driver)

    assert search_results == expected_search_results
    
    # Additional asserts for Standard case      
    if sitrep == 'Standard':

        assert driver.call_count == 4
        assert mock_time_sleep.call_count == 1
        assert mock_WebDriverWait.call_count == 3
        assert mock_convert_SaintLucie_raw_search_results.call_count == 1


def test_convert_SaintLucie_raw_search_results():
    """
    Ensure Saint Lucie converts raw search results
    """

    raw_search_results = '1'

    search_results = shift.convert_SaintLucie_raw_search_results(raw_search_results)

    assert search_results == 1


def test_sort_SaintLucie(shift_fixture):
    """
    Ensure Saint Lucie sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_SaintLucie(driver)

    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_SaintLucie(shift_fixture, sitrep):
    """
    Ensure Saint Lucie paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 1

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 10

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_SaintLucie(comb, driver)

    else:
        comb, driver = shift.pagination_SaintLucie(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1
    
    # Additional asserts according to case         
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 10
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_SaintLucie(shift_fixture):
    """
    Ensure Saint Lucie doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_SaintLucie(comb, driver)

    assert driver.call_count == 1


def test_doc_type_SaintLucie_match():
    """
    Ensure Saint Lucie matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_SaintLucie_match()

    assert match_doc_type_1 == 'MTG'
    assert match_doc_type_2 == 'MULE'


def test_click_image_SaintLucie(shift_fixture):
    """
    Ensure Saint Lucie click image calls
    """

    driver, comb = shift_fixture

    driver = shift.click_image_SaintLucie(comb, driver)

    assert driver.call_count == 1


def test_predownload_SaintLucie(shift_fixture):
    """
    Ensure Saint Lucie predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_SaintLucie(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 2


def test_revert_SaintLucie(shift_fixture):
    """
    Ensure Saint Lucie revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_SaintLucie(driver)

    assert driver.call_count == 6


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Sarasota)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Sarasota_search_results_count(shift_fixture):
    """
    Ensure Sarasota search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:

        search_results = shift.Sarasota_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_sort_Sarasota(shift_fixture):
    """
    Ensure Sarasta sort call
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        driver = shift.sort_Sarasota(driver)

    assert mock_Select.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Sarasota(shift_fixture, sitrep):
    """
    Ensure Sarasota paginates based on case
    """
    
    driver, comb = shift_fixture

    #Mod stub comb according to scenario
    comb['doc_counter'] = 2

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 25

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Sarasota(comb, driver)

    else:
        comb, driver = shift.pagination_Sarasota(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 2
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 25
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1
    

def test_xpath_doc_type_and_owner_Sarasota(shift_fixture):
    """
    Ensure Sarasota doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Sarasota(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Sarasota_match():
    """
    Ensure Sarasota matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Sarasota_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'
    

def test_click_image_Sarasota(shift_fixture):
    """
    Ensure Sarasotaclick image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.ActionChains') as mock_ActionChains:

        driver = shift.click_image_Sarasota(comb, driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1
    assert mock_ActionChains.call_count == 1


def test_predownload_Sarasota(shift_fixture):
    """
    Ensure Sarasota predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait:

        driver = shift.predownload_Sarasota(driver)

    assert mock_time_sleep.call_count == 3
    assert driver.call_count == 5
    assert mock_WebDriverWait.call_count == 2


def test_download_Sarasota(shift_fixture):
    """
    Ensure Sarasota click image call
    """

    driver, comb = shift_fixture

    driver = shift.download_Sarasota(driver)

    assert driver.call_count == 1


def test_revert_Sarasota(shift_fixture):
    """
    Ensure Sarasota click image call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Sarasota(driver)

    assert driver.call_count == 3


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Seminole)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Seminole_search_results_count(shift_fixture):
    """
    Ensure Seminole seac results count calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Seminole_raw_search_results') as mock_convert_Seminole_raw_search_results:

        search_results, driver = shift.Seminole_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Seminole_raw_search_results.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Standard', 1),
    ('No records', 0)
])
def test_convert_Seminole_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Seminole converts raw search results
    """

    if sitrep == 'Standard':
        raw_search_results = '1 records'

    else:
        raw_search_results = '0 records'

    search_results = shift.convert_Seminole_raw_search_results(raw_search_results)

    assert search_results == expected_search_results
    

def test_sort_Seminole(shift_fixture):
    """
    Ensure Seminole sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Seminole(driver)

    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep', [
    ('Origin'),
    ('Standard'),
    ('Paginate')
])
def test_pagination_Seminole(shift_fixture, sitrep):
    """
    Ensure Seminole paginates based on case
    """
    
    driver, comb = shift_fixture

    # Mod stub comb according to case
    if sitrep == 'Origin':
        pass

    elif sitrep == 'Standard':
        comb['search_instance'] = 5

    elif sitrep == 'Paginate':
        comb['search_instance'] = 31

    # Execute function according to case
    if sitrep == 'Paginate':
        
        with patch('shift.WebDriverWait') as mock_WebDriverWait:
            comb, driver = shift.pagination_Seminole(comb, driver)

    else:
        comb, driver = shift.pagination_Seminole(comb, driver)

    assert comb['print_search_instance'] == 1
    assert comb['doc_counter'] == 1
    
    # Additional asserts according to case   
    if sitrep == 'Origin':
        
        assert comb['search_instance'] == 0

    elif sitrep == 'Standard':

        assert comb['search_instance'] == 5

    elif sitrep == 'Origin' or sitrep == 'Standard':
        
        assert driver.call_count == 0

    elif sitrep == 'Paginate':

        assert comb['search_instance'] == 31
        assert driver.call_count == 1
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_Seminole(shift_fixture):
    """
    Ensure Seminole doc type calls
    """

    driver, comb = shift_fixture
 
    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Seminole(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Seminole_match():
    """
    Ensure Seminole matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Seminole_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'AGREEMENT FOR DEED'
    

def test_click_image_Seminole(shift_fixture):
    """
    Ensure Seminole click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.ActionChains') as mock_ActionChains:

        driver = shift.click_image_Seminole(comb, driver)

    assert driver.call_count == 2
    assert mock_time_sleep.call_count == 1
    assert mock_ActionChains.call_count == 1


def test_predownload_Seminole(shift_fixture):
    """
    Ensure Seminole predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.ActionChains') as mock_ActionChains:

        driver = shift.predownload_Seminole(driver)

    assert mock_time_sleep.call_count == 2
    assert driver.call_count == 3
    assert mock_WebDriverWait.call_count == 2
    assert mock_ActionChains.call_count == 1


def test_download_Seminole(shift_fixture):
    """
    Ensure Seminole download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Seminole(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Seminole(shift_fixture):
    """
    Ensure Seminole revert calls
    """
    
    driver, comb = shift_fixture

    with patch('shift.ActionChains') as mock_ActionChains, \
         patch('shift.time.sleep') as mock_time_sleep:
        
        driver = shift.revert_Seminole(driver)

    assert driver.call_count == 5
    assert mock_ActionChains.call_count == 1
    assert mock_time_sleep.call_count == 2
    
    
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Suwannee)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Suwannee_search_results_count(shift_fixture):
    """
    Ensure Suwannee search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
            patch('shift.convert_Suwannee_raw_search_results') as mock_convert_Suwannee_raw_search_results:
        search_results, driver = shift.Suwannee_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Suwannee_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Suwannee_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Suwannee converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Suwannee_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Suwannee(shift_fixture):
    """
    Ensure Suwannee sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Suwannee(driver)

    assert driver.call_count == 1


def test_pagination_Suwannee(shift_fixture):
    """
    Ensure proper Suwannee pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Suwannee(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Suwannee(shift_fixture):
    """
    Ensure Suwannee doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Suwannee(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Suwannee_match():
    """
    Ensure Suwannee matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Suwannee_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Suwannee(shift_fixture):
    """
    Ensure Suwannee click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        driver = shift.click_image_Suwannee(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_Suwannee(shift_fixture):
    """
    Ensure Suwannee download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Suwannee(driver)

    assert driver.call_count == 2


def test_revert_Suwannee(shift_fixture):
    """
    Ensure Suwannee revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Suwannee(driver)

    assert driver.call_count == 1
    
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Volusia)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


@pytest.mark.parametrize('sitrep', [
    ('Standard'),
    ('TimeoutException')
])
def test_Volusia_search_results_count(shift_fixture, sitrep):
    """
    Ensure Volusia search results are retrieved properly based on case
    """
    
    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.Select') as mock_Select:
        
        if sitrep == 'TimeoutException':
            mock_WebDriverWait.side_effect = TimeoutException

        search_results, driver = shift.Volusia_search_results_count(driver)
               
    if sitrep == 'Standard':
        assert search_results == 1
        assert mock_WebDriverWait.call_count == 2
        assert mock_Select.call_count == 1
        assert driver.call_count == 2

    else:
        assert search_results == 0
        assert mock_WebDriverWait.call_count == 1


def test_xpath_doc_type_and_owner_Volusia(shift_fixture):
    """
    Ensure Volusia doc type calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.time.sleep') as mock_time_sleep:
        
        xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Volusia(comb, driver)

    assert driver.call_count == 5
    assert mock_WebDriverWait.call_count == 1
    assert mock_time_sleep.call_count == 1

    
def test_doc_type_Volusia_match():
    """
    Ensure Volusia matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Volusia_match()

    assert match_doc_type_1 == 'MG'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Volusia(shift_fixture):
    """
    Ensure Volusia click image calls
    """

    driver, comb = shift_fixture

    driver = shift.click_image_Volusia(comb, driver)

    assert driver.call_count == 1


def test_predownload_Volusia(shift_fixture):
    """
    Ensure Volusia predownload calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.time.sleep') as mock_time_sleep:

        driver = shift.predownload_Volusia(driver)

    assert driver.call_count == 1
    assert mock_WebDriverWait.call_count == 1
    assert mock_time_sleep.call_count == 1


def test_download_Volusia(shift_fixture):
    """
    Ensure Volusia download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Volusia(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Volusia(shift_fixture):
    """
    Ensure Volusia revert calls
    """

    driver, comb = shift_fixture

    driver = shift.revert_Volusia(driver)

    assert driver.call_count == 3


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# (((((((((((((((((((((((((((((((((((((((((((((((((((Wakulla))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Wakulla_search_results_count(shift_fixture):
    """
    Ensure Wakulla search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Wakulla_raw_search_results') as mock_convert_Wakulla_raw_search_results:
        
        search_results, driver = shift.Wakulla_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Wakulla_raw_search_results.call_count == 1
    assert driver.call_count == 1


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Wakulla_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Wakulla converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Wakulla_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Wakulla(shift_fixture):
    """
    Ensure Wakulla sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Wakulla(driver)

    assert driver.call_count == 1


def test_pagination_Wakulla(shift_fixture):
    """
    Ensure proper Wakulla pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Wakulla(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Wakulla(shift_fixture):
    """
    Ensure Wakulla doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Wakulla(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Wakulla_match():
    """
    Ensure Wakulla matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Wakulla_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Wakulla(shift_fixture):
    """
    Ensure Wakulla click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.time.sleep') as mock_time_sleep:
        
        driver = shift.click_image_Wakulla(comb, driver)

    assert mock_WebDriverWait.call_count == 2
    assert driver.call_count == 4
    assert mock_time_sleep.call_count == 1


def test_download_Wakulla(shift_fixture):
    """
    Ensure Wakulla download calls
    """

    driver, comb = shift_fixture

    with patch('shift.win32gui.EnumWindows') as mock_win32gui_EnumWindows, \
         patch('shift.win32gui.ShowWindow') as mock_win32gui_ShowWindow, \
         patch('shift.win32gui.SetForegroundWindow') as mock_win32gui_SetForegroundWindow, \
         patch('shift.SendKeys') as mock_SendKeys, \
         patch('shift.time.sleep') as mock_time_sleep, \
         patch('shift.mule_top_windows',return_value = [('dummy','Opening')]) as mock_mule_top_windows:

        driver = shift.download_Wakulla(driver)

    assert driver.call_count == 1
    assert mock_win32gui_EnumWindows.call_count == 1
    assert mock_win32gui_ShowWindow.call_count == 1
    assert mock_win32gui_SetForegroundWindow.call_count == 1
    assert mock_SendKeys.call_count == 1
    assert mock_time_sleep.call_count == 1
    assert mock_mule_top_windows.call_count == 1


def test_revert_Wakulla(shift_fixture):
    """
    Ensure Wakulla revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Wakulla(driver)

    assert driver.call_count == 4


# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((Walton))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
# ((((((((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))


def test_Walton_search_results_count(shift_fixture):
    """
    Ensure Walton search results calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait, \
         patch('shift.convert_Walton_raw_search_results') as mock_convert_Walton_raw_search_results:
        
        search_results, driver = shift.Walton_search_results_count(driver)

    assert mock_WebDriverWait.call_count == 1
    assert mock_convert_Walton_raw_search_results.call_count == 1
    assert driver.call_count == 1    


@pytest.mark.parametrize('sitrep,expected_search_results', [
    ('Found search results', 1),
    ('No records found', 0)
])
def test_convert_Walton_raw_search_results(sitrep, expected_search_results):
    """
    Ensure Walton converts raw search results
    """

    if sitrep == 'Found search results':
        raw_search_results = '1'

    else:
        raw_search_results = 'Returned 0 records'

    search_results = shift.convert_Walton_raw_search_results(raw_search_results)

    assert search_results == expected_search_results


def test_sort_Walton(shift_fixture):
    """
    Ensure Walton sort call
    """

    driver, comb = shift_fixture

    driver = shift.sort_Walton(driver)

    assert driver.call_count == 1


def test_pagination_Walton(shift_fixture):
    """
    Ensure proper Walton pagination
    """

    driver, comb = shift_fixture

    with patch('shift.Select') as mock_Select:

        view_all, driver = shift.pagination_Walton(driver)

    assert view_all == True
    assert mock_Select.call_count == 1
    assert driver.call_count == 1


def test_xpath_doc_type_and_owner_Walton(shift_fixture):
    """
    Ensure Walton doc type calls
    """

    driver, comb = shift_fixture

    xpath_doc_type, owner, driver = shift.xpath_doc_type_and_owner_Walton(comb, driver)

    assert driver.call_count == 1


def test_doc_type_Walton_match():
    """
    Ensure Walton matching documents
    """

    match_doc_type_1, match_doc_type_2 = shift.doc_type_Walton_match()

    assert match_doc_type_1 == 'MORTGAGE'
    assert match_doc_type_2 == 'MULE'


def test_click_image_Walton(shift_fixture):
    """
    Ensure Walton click image calls
    """

    driver, comb = shift_fixture

    with patch('shift.WebDriverWait') as mock_WebDriverWait:
        driver = shift.click_image_Walton(comb, driver)

    assert mock_WebDriverWait.call_count == 1
    assert driver.call_count == 1


def test_download_Walton(shift_fixture):
    """
    Ensure Walton download calls
    """

    driver, comb = shift_fixture

    driver = shift.download_Walton(driver)

    assert driver.call_count == 2


def test_revert_Walton(shift_fixture):
    """
    Ensure Walton revert call
    """

    driver, comb = shift_fixture

    driver = shift.revert_Walton(driver)

    assert driver.call_count == 1


# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------Misc------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def test_mule_top_windows():
    """
    Ensure top_windows is an empty dictionary
    """

    top_windows = shift.mule_top_windows()

    assert top_windows == []


def test_windowEnumerationHandler():
    """
    Ensure win32gui.GetWindowText call
    """

    hwnd = MagicMock()
    top_windows = []

    with patch('shift.win32gui.GetWindowText') as mock_win32gui_GetWindowText:

        shift.windowEnumerationHandler(hwnd, top_windows)

    assert mock_win32gui_GetWindowText.call_count == 1


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
