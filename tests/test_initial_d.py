# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import os
import re
import pytest

import sys
sys.path.append(r'..\Initial_D')

import initial_d

from datetime import datetime
from unittest.mock import patch, MagicMock

# ----------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------Test Cases/Test Suite----------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def test_initial_d():
    """
    Ensure initial d call
    """

    with patch('rev.select') as mock_select:

        dlc1 = initial_d.initial_d()

    assert mock_select.call_count == 1

        
@pytest.mark.parametrize('sitrep,ampersand', [
    ('Address Fail', None),
    ('Entity', None),
    ('Multi Standard', True),
    ('Multi Fail', True),
    ('Single Standard', False),
    ('Single Fail', False),
    ('Inactive County', True)
])
def test_lap(initial_d_fixture, sitrep, ampersand):
    """
    Ensure lap calls based on data
    """

    dlc1 = initial_d_fixture

    # Mod stub according to case
    if sitrep == 'Address Fail':
        dlc1['address_flag'] = True

    elif sitrep == 'Entity':
        dlc1['entity'] = True

    elif sitrep == 'Multi Fail' or sitrep == 'Single Fail':
        dlc1['skip'] = True

    elif sitrep == 'Inactive County':
        dlc1['county_flag'] = 0
         
    with patch('rev.check_address', return_value = dlc1) as mock_check_address, \
         patch('rev.check_entity', return_value = dlc1) as mock_check_entity, \
         patch('rev.check_abbreviations', return_value = dlc1) as mock_check_abbreviations, \
         patch('initial_d.re.search', return_value = ampersand) as mock_re_search, \
         patch('rev.check_multi', return_value = dlc1) as mock_check_multi, \
         patch('rev.check_single', return_value = dlc1) as mock_check_single, \
         patch('rev.check_county', return_value = dlc1) as mock_check_county, \
         patch('rev.gen_dlc2', return_value = dlc1) as mock_gen_dlc2, \
         patch('drive.ignite') as mock_ignite, \
         patch('drive.drive') as mock_drive, \
         patch('rev.mark_address_error', return_value = dlc1) as mock_mark_address_error, \
         patch('initial_d.log') as mock_log:
        
        initial_d.lap(dlc1)

    assert mock_log.call_count == 1

    # Assert according to case
    if sitrep == 'Address Fail':
        
        assert mock_check_address.call_count == 1
        assert mock_mark_address_error.call_count == 1

    elif sitrep == 'Entity':
        
        assert mock_check_address.call_count == 1
        assert mock_check_entity.call_count == 1
        assert mock_check_county.call_count == 1
        assert mock_gen_dlc2.call_count == 1
        assert mock_ignite.call_count == 1
        assert mock_drive.call_count == 1

    elif sitrep == 'Multi Standard' or sitrep == 'Single Standard':
        
        assert mock_check_address.call_count == 1
        assert mock_check_entity.call_count == 1
        assert mock_check_abbreviations.call_count == 1
        assert mock_re_search.call_count == 1

        if sitrep == 'Multi Standard':
            assert mock_check_multi.call_count == 1
            
        else:
            assert mock_check_single.call_count == 1
            
        assert mock_check_county.call_count == 1
        assert mock_gen_dlc2.call_count == 1
        assert mock_ignite.call_count == 1
        assert mock_drive.call_count == 1

    elif sitrep == 'Multi Fail' or sitrep == 'Single Fail':
        
        assert mock_check_address.call_count == 1
        assert mock_check_entity.call_count == 1
        assert mock_check_abbreviations.call_count == 1
        assert mock_re_search.call_count == 1

        if sitrep == 'Multi Fail':
            assert mock_check_multi.call_count == 1
 
        else:
            assert mock_check_single.call_count == 1

    elif sitrep == 'Inactive County':
        
        assert mock_check_address.call_count == 1
        assert mock_check_entity.call_count == 1
        assert mock_check_abbreviations.call_count == 1
        assert mock_re_search.call_count == 1
        assert mock_check_multi.call_count == 1   
        assert mock_check_county.call_count == 1


def test_log(initial_d_fixture):
    """
    Ensure log creation and content
    """

    dlc1 = initial_d_fixture
    
    start = start = datetime.now()
    end = datetime.now()

    delta = end - start
    seconds = int(round(delta.total_seconds()))
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)

    initial_d.log(start, end, dlc1)
    
    parent_dir = os.path.abspath('..')
    
    logpath =  parent_dir + '\\logs\\'+ dlc1['log_date'] + '.txt'
    log = open(logpath, 'r')

    assert log.readlines(1) == [str('Started at ' + str(start) + '\n')]
    assert log.readlines(2) == [str('Ended at ' + str(end) + '\n')]
    assert log.readlines(3) == [str('Evolution duration {:d} hour(s) and {:02d} minute(s) and {:02d} second(s)'.format(hours, minutes, seconds))]

    log.close()

    os.remove(logpath)


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
