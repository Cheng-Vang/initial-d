# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import pytest
import sys
sys.path.append(r'..\Initial_D')

import rev

from unittest.mock import patch

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------Tests/Test Suite------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


def test_select(rev_fixture):
    """
    Ensure initialization of dataset and dataframe
    """

    dlc1 = rev_fixture

    assert dlc1['dataframe'].shape[0] == 6
    assert dlc1['dataframe'].shape[1] == 4
    assert dlc1['dataset']['holder']['0'] == 'JOHN S & JANE DOE'
    assert dlc1['dataset']['address']['0'] == '100 Main St Newberry, FL'
    assert dlc1['dataset']['county']['0'] == 'Alachua'
    assert dlc1['dataset']['principal']['0'] == '250000'

    assert dlc1['dataset']['first_name'] == {}
    assert dlc1['dataset']['first_mid'] == {}
    assert dlc1['dataset']['second_name'] == {}
    assert dlc1['dataset']['second_mid'] == {}
    assert dlc1['dataset']['last_name'] == {}

    assert dlc1['from_date'] == '06/30/2017'
    assert dlc1['to_date'] == '12/31/2017'

    assert dlc1['path'] == r'C:/Mock/FL 12-31-2017.xlsx'

    assert dlc1['sole_holder_flag'] == False
    assert dlc1['skip'] == False
    assert dlc1['address_flag'] == False
    assert dlc1['entity'] == False
    assert dlc1['county_flag'] == 1


@pytest.mark.parametrize('data,expected_address_flag', [
    ('0', False),
    ('1', True)
])
def test_check_address(rev_fixture, data, expected_address_flag):
    """
    Validate address test and proper adjustments
    """

    dlc1 = rev_fixture
    dlc1 = rev.check_address(dlc1, data)

    assert dlc1['address_flag'] == expected_address_flag


@pytest.mark.parametrize('data,expected_entity,expected_sole_holder_flag', [
    ('0', False, False),
    ('2', True, True),
])
def test_check_entity(rev_fixture, data, expected_entity, expected_sole_holder_flag):
    """
    Validate entity test and proper adjustments
    """

    dlc1 = rev_fixture
    dlc1 = rev.check_entity(dlc1, data)

    assert dlc1['entity'] == expected_entity


@pytest.mark.parametrize('data,expected_holder', [
    ('0', 'JOHN S & JANE DOE'),
    ('3', 'RANDALL B BELL')
])
def test_check_abbreviations(rev_fixture, data, expected_holder):
    """
    Validate abbreviations test and proper adjustments
    """

    dlc1 = rev_fixture
    dlc1 = rev.check_abbreviations(dlc1, data)

    assert dlc1['dataset']['holder'][data] == expected_holder


@pytest.mark.parametrize(
    'data,expected_first_name,expected_first_mid,expected_second_name,expected_second_mid,expected_last_name,expected_skip',
    [
        ('0', 'JOHN', 'S', 'JANE', '', 'DOE', False),
        ('4', '', '', '', '', '', True,),
    ])
def test_check_multi(rev_fixture, data, expected_first_name, expected_first_mid, expected_second_name, expected_second_mid,
                     expected_last_name, expected_skip):
    """
    Validate multi test and proper adjustments
    """

    dlc1 = rev_fixture
    with patch('rev.mark_holder_not_found', return_value=dlc1):
        dlc1 = rev.check_multi(dlc1, data)

    assert dlc1['dataset']['first_name'][data] == expected_first_name
    assert dlc1['dataset']['first_mid'][data] == expected_first_mid
    assert dlc1['dataset']['second_name'][data] == expected_second_name
    assert dlc1['dataset']['second_mid'][data] == expected_second_mid
    assert dlc1['dataset']['last_name'][data] == expected_last_name
    assert dlc1['skip'] == expected_skip


@pytest.mark.parametrize(
    'data,expected_first_name,expected_first_mid,expected_last_name,expected_skip,expected_sole_holder_flag', [
        ('3', 'RANDALL', 'B', 'BELL', False, True),
        ('5', '', '', '', True, True)
    ])
def test_check_single(rev_fixture, data, expected_first_name, expected_first_mid, expected_last_name, expected_skip,
                      expected_sole_holder_flag):
    """
    Validate single test and set data
    """

    dlc1 = rev_fixture

    with patch('rev.mark_holder_not_found', return_value=dlc1):
        dlc1 = rev.check_single(dlc1, data)

    assert dlc1['dataset']['first_name'][data] == expected_first_name
    assert dlc1['dataset']['first_mid'][data] == expected_first_mid
    assert dlc1['dataset']['last_name'][data] == expected_last_name
    assert dlc1['skip'] == expected_skip
    assert dlc1['sole_holder_flag'] == expected_sole_holder_flag


@pytest.mark.parametrize('data,expected_county_flag', [
    ('0', 1),
    ('2', 2),
    ('5', 0)
])
def test_check_county(rev_fixture, data, expected_county_flag):
    """
    Validate county test and set course
    """

    dlc1 = rev_fixture
    dlc1 = rev.check_county(dlc1, data)

    assert dlc1['county_flag'] == expected_county_flag


def test_save_dataframe(rev_fixture):
    """
    Validate save called
    """

    dlc1 = rev_fixture

    with patch('rev.save_dataframe') as mock_save_dataframe:
        rev.save_dataframe(dlc1)

    mock_save_dataframe.assert_called_with(dlc1)


def test_mark_address_error(rev_fixture):
    """
    Ensure unaccounted address cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc1 = rev_fixture
    data = '1'

    with patch('rev.save_dataframe') as mock_save_dataframe:
        dlc1 = rev.mark_address_error(dlc1, data)

    mock_save_dataframe.assert_called_with(dlc1)

    assert dlc1['dataframe'].loc[1, 'Address Error'] == 1


def test_mark_holder_not_found(rev_fixture):
    """
    Ensure unaccounted holder cases are marked in the dataframe and reloaded to reflect changes
    """

    dlc1 = rev_fixture
    data = '4'

    with patch('rev.save_dataframe') as mock_save_dataframe:
        dlc1 = rev.mark_holder_not_found(dlc1, data)

    mock_save_dataframe.assert_called_with(dlc1)

    assert dlc1['dataframe'].loc[4, 'Holder Not Found'] == 1


def test_gen_dlc2(rev_fixture): 
    """
    Ensure proper county specific arguments are set and loaded
    """

    data = '3'
    dlc1 = rev_fixture
    dlc2 = rev.gen_dlc2(dlc1, data)

    assert dlc2['website'] == 'http://ori.indian-river.org/'
    assert dlc2['name_field_1'] == '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[2]/td[2]/input'
    assert dlc2['name_field_2'] == ''
    assert dlc2['from_date_field'] == '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[6]/td[2]/input'
    assert dlc2['to_date_field'] == '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[7]/td[2]/input'
    assert dlc2['search_button'] == '/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[9]/td[2]/a[1]'
    assert dlc2['mid_initial_flag'] == False
    assert dlc2['comma_flag'] == False
    assert dlc2['county'] == 'IndianRiver'


# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------Runtime-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    pass
