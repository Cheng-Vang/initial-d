# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------Libraries/Imports-------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

import pytest
import pandas

from selenium import webdriver
from unittest.mock import create_autospec

# ----------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------Fixtures-----------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------


@pytest.fixture(scope='function')
def initial_d_fixture():
    """
    Stub initial_d dataframe and dlc1
    """

    dataframe = pandas.DataFrame({
                                  'Address Error':[''],
                                  'County':['Alachua'],
                                  '_3':['100 Main St Newberry, FL'],
                                  'Holder Not Found':['']
                                })

    dlc1 = {
             'dataframe': dataframe,
             'dataset':
              {
               'holder':
                {'0':'JOHN S & JANE DOE'},
               'address':
                {'0':'100 Main St Newberry, FL'},
               'county':
                {'0':'Alachua'},
               'principal':
                {'0':'250000'},
               'first_name':
                {},
               'first_mid':
                {},
               'second_name':
                {},
               'second_mid':
                {},
               'last_name':
                {}
              },
             'county':'Alachua',
             'from_date':'06/30/2017',
             'to_date':'12/31/2017',
             'path':r'C:/Mock/FL 12-31-2017.xlsx',
             'sole_holder_flag':False,
             'skip':False,
             'address_flag':False,
             'entity':False,
             'county_flag':1,
             'log_date':'11-10-2018'
            }

    return dlc1

    
@pytest.fixture(scope='module')
def rev_fixture():
    """
    Stub rev dataframe and dlc1
    """

    dataframe = pandas.DataFrame({
                                  'Address Error':['','','','','',''],
                                  'County':['Alachua','Miami-dade','Bay','Indian River','Okeechobee','Orange'],
                                  '_3':['100 Main St Newberry, FL','FPO Miami, AA 34090','123 Water St Pensacola Beach, FL',\
                                        '700 Pine Rd Port Orange, FL','PO Box 1234 Harbor Islands, FL','4321 Water St Apopka, FL'],
                                  'Holder Not Found':['','','','','','']
                                })

    # 0 - Base (All .), 1 - Address (F), 2 - Entity (.), 3 - Abbreviation/Single (..), 4 - Multi (F), 5 - Single (F)
    dlc1 = {
             'dataframe': dataframe,
             'dataset':
              {
               'holder':
                {'0':'JOHN S & JANE DOE','1':'MASON FREEMAN','2':'GIT LLC','3':'RANDALL B BELL JR','4':'BRAD T SR III & ELLY F SMITH',\
                 '5':'RICHARD FRANKLIN THOMAS JONES'},
               'address':
                {'0':'100 Main St Newberry, FL','1':'FPO Miami, AA 34090','2':'123 Water St Pensacola Beach, FL','3':'700 Pine Rd Port Orange, FL',\
                 '4':'PO Box 1234 Harbor Islands, FL','5':'4321 Water St Apopka, FL'},
               'county':
                {'0':'Alachua','1':'Miami-dade','2':'Bay','3':'Indian River','4':'Okeechobee','5':'Orange'},
               'principal':
                {'0':'250000','1':'500000','2':'750000','3':'1000000','4':'250000','5':'500000'},
               'first_name':
                {},
               'first_mid':
                {},
               'second_name':
                {},
               'second_mid':
                {},
               'last_name':
                {}
              },
             'county':'MiamiDade',
             'from_date':'06/30/2017',
             'to_date':'12/31/2017',
             'path':r'C:/Mock/FL 12-31-2017.xlsx',
             'sole_holder_flag':False,
             'skip':False,
             'address_flag':False,
             'entity':False,
             'county_flag':1,
             'log_date':'11-10-2018'
            }

    return dlc1


@pytest.fixture(scope='function')
def drive_fixture():
    """
    Stub drive dataframe & dlc and mock WebDriver
    """

    dataframe = pandas.DataFrame({
                                  'Address Error':[''],
                                  'County':['Indian River'],
                                  '_3':['700 Pine Rd Port Orange, FL'],
                                  'Holder Not Found':[''],
                                  'Doc Not Found':[''],
                                  'Driver Crashed':[''],
                                  'Crash Cause':[''],
                                  'Owner':[''],
                                  'Matures':['']
                                })

    dlc = {
             'dataframe': dataframe,
             'dataset':
              {
               'holder':
                {'0':'RANDALL B BELL JR'},
               'address':
                {'0':'700 Pine Rd Port Orange, FL'},
               'county':
                {'0':'IndianRiver'},
               'principal':
                {'0':'1000000'},
               'first_name':
                {'0':'RANDALL'},
               'first_mid':
                {'0':'B'},
               'second_name':
                {},
               'second_mid':
                {},
               'last_name':
                {'0':'BELL'}
              },
             'set_name':'RANDALL B BELL',
             'from_date':'06/30/2017',
             'to_date':'12/31/2017',
             'path':r'C:/Mock/FL 12-31-2017.xlsx',
             'sole_holder_flag':True,
             'skip':False,
             'address_flag':False,
             'entity':False,
             'county_flag':2,
             'data':'0',
             'website':'http://ori.indian-river.org/',
             'name_field_1':'/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[2]/td[2]/input',
             'name_field_2':'',
             'from_date_field':'/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[6]/td[2]/input',
             'to_date_field':'/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[7]/td[2]/input',
             'search_button':'/html/body/div[6]/div/div/div/div[1]/div/div/div[1]/div/div/div[1]/form/table/tbody/tr[9]/td[2]/a[1]',
             'mid_initial_flag':False,
             'comma_flag':False,
             'crash_cause': '1',
             'crash_flag': False,
             'name_field_crash': 0,
             'set_holder_marker': 1
            }

    driver = create_autospec(webdriver)

    return dlc, driver

   
@pytest.fixture(scope='function')
def shift_fixture():
    """
    Stub comb and mock Webdriver
    """

    comb = {}
    comb['search_instance'] = 0
    comb['print_search_instance'] = 1
    comb['doc_counter'] = 1
    
    driver = create_autospec(webdriver)
    
    return driver, comb
